#version 410 core
//#extension GL_ARB_shading_language_packing : enable

in vec3 textureCoordinates;

// Floating point pack/unpack functions are part of the GLSL ES 300 specification used by web and mobile.
uint float2half(uint f) {
    uint e = f & uint(0x7f800000);
    if (e <= uint(0x38000000)) {
        return uint(0);
    } else {
        return ((f >> uint(16)) & uint(0x8000)) |
                (((e - uint(0x38000000)) >> uint(13)) & uint(0x7c00)) |
                ((f >> uint(13)) & uint(0x03ff));
    }
}

uint packHalf2x16(vec2 v) {
    return float2half(floatBitsToUint(v.x)) | float2half(floatBitsToUint(v.y)) << uint(16);
}

// 3 16 bits for position.
// 3 16 bits for normal.
// 1 32 bits for padding.
layout( location = 0 ) out uvec4 outViewPositionNormal;
// 3 32 bits for color.
// 1 32 bits for specular.
layout( location = 1 ) out vec4 outColor;

uniform samplerCube skybox;

void main()
{
	// Output data.
	// Compress position and normal.
	uvec4 viewPositionNormalCompressed;
	viewPositionNormalCompressed.x = packHalf2x16( vec2(0.) );
	viewPositionNormalCompressed.y = packHalf2x16( vec2(0.) );
	viewPositionNormalCompressed.z = packHalf2x16( vec2(0.) );
	viewPositionNormalCompressed.w = 0;

	// Output data.
	outViewPositionNormal = viewPositionNormalCompressed;
	outColor			  = texture(skybox, textureCoordinates);
}
