#version 410 core
#extension GL_ARB_shading_language_packing : enable

layout (location = 0) in vec3 aPos;

out vec3 textureCoordinates;

layout(std140) uniform CameraUniforms 
{
	mat4 uView;
	mat4 uProjection;
};

void main()
{
    textureCoordinates = aPos;
    gl_Position = (uProjection * mat4(mat3(uView)) * vec4(aPos, 1.0)).xyww;
}  
