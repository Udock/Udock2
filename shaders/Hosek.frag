#version 410 core

// Implementation of Lukas Hosek, & Alexander Wilkie (2012). An analytic model for full spectral sky-dome radiance. ACM
// Transactions on Graphics, 31(4), 1?9. Article and authors' implementation:
// https://cgg.mff.cuni.cz/projects/SkylightModelling/HosekWilkie_SkylightModel_SIGGRAPH2012_Preprint_lowres.pdf
// Shadertoy implementation by pajunen: https://www.shadertoy.com/view/wslfD7
// XYZ to RGB: https://www.itp.uni-hannover.de/fileadmin/itp/emeritus/zawischa/static_html/render.html

layout( location = 0 ) out vec4 fragColor;

layout(std140) uniform HosekUniforms
{
    vec2  uViewportSize;
    mat4  uView;
    vec4  uSolarPoint;
    vec4  uConfigs[9];
    vec4  uExpectedSpectralRadiance;
    float uTurbidity;
};

vec3 getRadiance( float theta, float gamma )
{
    vec3 a = uConfigs[0].rgb;
    vec3 b = uConfigs[1].rgb;
    vec3 c = uConfigs[2].rgb;
    vec3 d = uConfigs[3].rgb;
    vec3 e = uConfigs[4].rgb;
    vec3 f = uConfigs[5].rgb;
    vec3 g = uConfigs[6].rgb;
    vec3 h = uConfigs[7].rgb;
    vec3 i = uConfigs[8].rgb;

    float cosGamma = abs(cos(gamma));
    float cosTheta = abs(cos(theta));
    float squareCosGamma = cosGamma * cosGamma;

    // H and J seem to be inversed in the implementation and the article
    vec3 chi = (1.0 + squareCosGamma) / pow( 1. + i*i - 2.*i*cosGamma, vec3(1.5));
    return (1.0 + a * exp(b / (cosTheta + 0.01))) * (c + d * exp(e * gamma) + f * squareCosGamma + g * chi + h * sqrt(cosTheta));
}

vec3 xyzToRgb(vec3 xyz) {
    const vec3 v1 = vec3(  3.2404542 , -0.9692660,  0.0556434 );
    const vec3 v2 = vec3( -1.5371385,   1.8760108, -0.2040259 );
    const vec3 v3 = vec3( -0.4985314,   0.0415560,  1.0572252 );
	return mat3( v1, v2, v3 ) * xyz;
}

void main()
{
    vec2 uv = ((gl_FragCoord.xy / uViewportSize) - .5) * 2.;
    uv.x = -uv.x;

    vec3 rd = normalize((uView * vec4(normalize(vec3(uv, 1.)), 1.)).xyz);
    
    // angle formed by viewing ray and zenith
    float theta = acos(clamp(rd.y, 0., 1.));
    // angle between viewing ray and the solar point.
    float gamma = acos(clamp(dot(rd, uSolarPoint.xyz), 0., 1.));

    vec3 xyzRadiance = getRadiance(theta, gamma) * uExpectedSpectralRadiance.xyz * 1e-2;
    vec3 radiance    = xyzToRgb(xyzRadiance);

    fragColor = vec4( smoothstep(vec3(0.), vec3(1.), pow( (radiance + .055) / ( 1. + .055 ), vec3(1. / 2.2) ) ), 1.);
}
