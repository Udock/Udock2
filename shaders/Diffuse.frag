#version 410 core
//#extension GL_ARB_shading_language_packing : enable

uniform usampler2D gbViewPositionNormal;
uniform sampler2D gbColor;
uniform samplerCube skybox;
layout( location = 0 ) out vec4 fragColor;

// Floating point pack/unpack functions are part of the GLSL ES 300 specification used by web and mobile.
uint float2half(uint f) {
    uint e = f & uint(0x7f800000);
    if (e <= uint(0x38000000)) {
        return uint(0);
    } else {
        return ((f >> uint(16)) & uint(0x8000)) |
                (((e - uint(0x38000000)) >> uint(13)) & uint(0x7c00)) |
                ((f >> uint(13)) & uint(0x03ff));
    }
}

uint half2float(uint h) {
	uint h_e = h & uint(0x7c00);
	return ((h & uint(0x8000)) << uint(16)) | uint((h_e >> uint(10)) != uint(0)) * (((h_e + uint(0x1c000)) << uint(13)) | ((h & uint(0x03ff)) << uint(13)));
}

uint packHalf2x16(vec2 v) {
    return float2half(floatBitsToUint(v.x)) | float2half(floatBitsToUint(v.y)) << uint(16);
}

vec2 unpackHalf2x16(uint v) {
	return vec2(uintBitsToFloat(half2float(v & uint(0xffff))),
			uintBitsToFloat(half2float(v >> uint(16))));
}

layout(std140) uniform DiffuseUniforms 
{
	vec4 uBackgroundColor;
	vec4 uLightPosition;
	vec4 uLightColor;
	bool uWithEnvironment;
	bool uIsSkybox;
};

struct UnpackedData
{
	vec3 viewPosition;
	vec3 normal;
	bool shade;
	bool isBackground;
	bool isOther;
};

void unpackGBuffers( ivec2 px, out UnpackedData data )
{
	uvec4 viewPositionNormal = texelFetch( gbViewPositionNormal, px, 0 );

	vec2 tmp		  = unpackHalf2x16( viewPositionNormal.y );
	data.viewPosition = vec3( unpackHalf2x16( viewPositionNormal.x ), tmp.x );
	data.normal		  = vec3( tmp.y, unpackHalf2x16( viewPositionNormal.z ) );
	data.shade		  = viewPositionNormal.w != 0.f;
	data.isBackground = data.viewPosition.z == 0.f && viewPositionNormal.w == 0.f;
	data.isOther      = viewPositionNormal.w == 3.f;
}

void main()
{
	ivec2 texCoord = ivec2( gl_FragCoord.xy );

	UnpackedData data;
	unpackGBuffers( texCoord, data );

	vec3 color = texelFetch( gbColor, texCoord, 0 ).rgb;
	if( data.isOther && !data.isBackground ) 
	{
		fragColor = vec4(color, 1.);
		return;
	}
	else if( data.isBackground )
	{
		if(uIsSkybox) 
			fragColor = vec4(color, 1.);
		else 
			fragColor = uBackgroundColor;
		return;
	}

	vec3  lightDir    = normalize( uLightPosition.xyz - data.viewPosition );
	float cosTheta    = max( dot( data.normal, lightDir ), 0.f );
	color *= cosTheta;

	if(uWithEnvironment)
		color *= 1.5 * texture(skybox, reflect(data.viewPosition, data.normal)).rgb;

	fragColor = vec4( color, 1.f );
}
