#version 410 core
//#extension GL_ARB_shading_language_packing : enable

// Floating point pack/unpack functions are part of the GLSL ES 300 specification used by web and mobile.
uint float2half(uint f) {
    uint e = f & uint(0x7f800000);
    if (e <= uint(0x38000000)) {
        return uint(0);
    } else {
        return ((f >> uint(16)) & uint(0x8000)) |
                (((e - uint(0x38000000)) >> uint(13)) & uint(0x7c00)) |
                ((f >> uint(13)) & uint(0x03ff));
    }
}

uint half2float(uint h) {
    uint h_e = h & uint(0x7c00);
    return ((h & uint(0x8000)) << uint(16)) | uint((h_e >> uint(10)) != uint(0)) * (((h_e + uint(0x1c000)) << uint(13)) | ((h & uint(0x03ff)) << uint(13)));
}

uint packHalf2x16(vec2 v) {
    return float2half(floatBitsToUint(v.x)) | float2half(floatBitsToUint(v.y)) << uint(16);
}

vec2 unpackHalf2x16(uint v) {
    return vec2(uintBitsToFloat(half2float(v & uint(0xffff))),
            uintBitsToFloat(half2float(v >> uint(16))));
}

uniform usampler2D gbViewPositionNormal;
uniform sampler2D trailsTexture;
layout(std140) uniform TrailsUniforms 
{
	ivec2 uShift;
	float uAttenuation;
	float uInitialMix;
};

layout( location = 0 ) out float trailsData;


struct UnpackedData
{
	vec3 viewPosition;
	vec3 normal;
	bool trails;
};

void unpackGBuffers( ivec2 px, out UnpackedData data )
{
	uvec4 viewPositionNormal = texelFetch( gbViewPositionNormal, px, 0 );

	vec2 tmp		  = unpackHalf2x16( viewPositionNormal.y );
	data.viewPosition = vec3( unpackHalf2x16( viewPositionNormal.x ), tmp.x );
	data.normal		  = vec3( tmp.y, unpackHalf2x16( viewPositionNormal.z ) );
	data.trails		  = viewPositionNormal.w == 2.f;
}

void main()
{ 
	ivec2 textureCoordinates = ivec2( gl_FragCoord.xy );

	UnpackedData data;
	unpackGBuffers( textureCoordinates, data );
	trailsData = uAttenuation * texelFetch( trailsTexture, textureCoordinates + uShift, 0 ).r;
	
	if( data.trails )
		trailsData = uInitialMix;
} 
