#version 410 core

uniform sampler2D trailsTexture;
uniform sampler2D colorTexture;

layout(std140) uniform TrailsUniforms 
{
	vec4 uTrailsColor;
};

layout( location = 0 ) out vec4 fragColor;

void main()
{ 
	ivec2 textureCoordinates = ivec2( gl_FragCoord.xy );
	fragColor = texelFetch( colorTexture, textureCoordinates, 0 );

	float trailValue = texelFetch( trailsTexture, textureCoordinates, 0 ).r;
	fragColor = mix(fragColor, uTrailsColor, trailValue);
} 
