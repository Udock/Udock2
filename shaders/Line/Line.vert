#version 410 core
#extension GL_ARB_shading_language_packing : enable

layout(std140) uniform CameraUniforms 
{
	mat4 uView;
	mat4 uProjection;
};

layout(std140) uniform LinesUniforms 
{
	vec4 color;
};

in vec4 aPoint;

void main()
{
	gl_Position = uProjection * uView * aPoint;
}
