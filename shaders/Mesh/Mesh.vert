#version 410 core
#extension GL_ARB_shading_language_packing : enable

layout (location = 0) in vec3 aPosition;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec3 aColor;

out vec3 viewPosition;
out vec3 normal;
out vec3 color;

layout(std140) uniform CameraUniforms 
{
	mat4 uView;
	mat4 uProjection;
};

layout(std140) uniform ModelUniforms
{
	mat4 uModel;
};

void main()
{
	mat4 modelView = uView * uModel;
	vec4 vertexViewPosition = modelView * vec4(aPosition, 1.0);

	gl_Position  = uProjection * vertexViewPosition;
	viewPosition = vertexViewPosition.xyz;
	normal = normalize(mat3(transpose(inverse(modelView))) * aNormal);
	color = aColor;
}
