#version 410 core
//#extension GL_ARB_shading_language_packing : enable
//#extension GL_ARB_conservative_depth : enable

//#define SHOW_IMPOSTORS

//layout (depth_greater) out float gl_FragDepth;

// Floating point pack/unpack functions are part of the GLSL ES 300 specification used by web and mobile.
uint float2half(uint f) {
    uint e = f & uint(0x7f800000);
    if (e <= uint(0x38000000)) {
        return uint(0);
    } else {
        return ((f >> uint(16)) & uint(0x8000)) |
                (((e - uint(0x38000000)) >> uint(13)) & uint(0x7c00)) |
                ((f >> uint(13)) & uint(0x03ff));
    }
}

uint packHalf2x16(vec2 v) {
    return float2half(floatBitsToUint(v.x)) | float2half(floatBitsToUint(v.y)) << uint(16);
}

layout(std140) uniform CameraUniforms 
{
	mat4 uView;
	mat4 uProjection;
};

smooth in vec3 viewImpPos;
flat in vec3   viewSpherePos;
flat in vec3   sphereColor;
flat in float  sphereRad;
flat in float  dotViewSpherePos;

// 3 16 bits for position.
// 3 16 bits for normal.
// 1 32 bits for padding.
layout( location = 0 ) out uvec4 outViewPositionNormal;
// 3 32 bits for color.
// 1 32 bits for specular.
layout( location = 1 ) out vec4 outColor;

float computeDepth( const vec3 v )
{
	// Computes 'v' NDC depth ([-1,1])
	float ndcDepth = ( v.z * uProjection[ 2 ].z + uProjection[ 3 ].z ) / -v.z;
	// Return depth according to depth range
	return ( gl_DepthRange.diff * ndcDepth + gl_DepthRange.near + gl_DepthRange.far ) * 0.5f;
}

void main()
{
	float a = dot( viewImpPos, viewImpPos );
	// b = -dot(viewImpPos, viewSpherePos);
	// But '-' is useless since 'b' is squared for 'delta'.
	// So for 't', '-' is also removed.
	float b	  = dot( viewImpPos, viewSpherePos );
	float c	  = dotViewSpherePos - sphereRad * sphereRad;
	float delta = b * b - a * c;

	if ( delta < 0.f )
	{
#ifdef SHOW_IMPOSTORS
		// Show impostors for debugging purpose.
		uvec4 colorNormal = uvec4( 0 );
		// fill G-buffers.
		uvec4 viewPositionNormalCompressed;
		viewPositionNormalCompressed.x = packHalf2x16( viewImpPos.xy );
		viewPositionNormalCompressed.y = packHalf2x16( vec2( viewImpPos.z, -viewSpherePos.x ) );
		viewPositionNormalCompressed.z = packHalf2x16( -viewSpherePos.yz );
		viewPositionNormalCompressed.w = 0; // Padding.

		// Output data.
		outViewPositionNormal = viewPositionNormalCompressed;
		outColor			  = vec4( 1.f, 0.f, 0.f, 32.f ); // w = specular shininess.

		gl_FragDepth = computeDepth( viewImpPos );
#else
		discard;
#endif
	}
	else
	{
		// Solve equation (only first intersection).
		// '-' is removed (see 'b').
		float t = ( b - sqrt( delta ) ) / a;

		// Compute hit point and normal (always in view space).
		vec3 hit	  = viewImpPos * t;
		vec3 normal = normalize( hit - viewSpherePos );

		// Fill depth buffer.
		gl_FragDepth = computeDepth( hit );

		// Output data.
		// Compress position and normal.
		uvec4 viewPositionNormalCompressed;
		viewPositionNormalCompressed.x = packHalf2x16( hit.xy );
		viewPositionNormalCompressed.y = packHalf2x16( vec2( hit.z, normal.x ) );
		viewPositionNormalCompressed.z = packHalf2x16( normal.yz );
		viewPositionNormalCompressed.w = 1;

		// Output data.
		outViewPositionNormal = viewPositionNormalCompressed;
		outColor			  = vec4( sphereColor, 32.f ); // w = specular shininess.
	}
}
