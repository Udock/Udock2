#version 410 core

layout(std140) uniform CameraUniforms 
{
	mat4 uView;
	mat4 uProjection;
};

in vec4 aSphere;
in vec4 aColor;

flat out vec3  vViewSpherePos; // Sphere position in view space.
flat out vec3  vSphereColor;
flat out float vSphereRad;
flat out vec3  vImpU; // Impostor vectors.
flat out vec3  vImpV;
flat out float vDotViewSpherePos;

void main()
{
	vViewSpherePos = vec3( uView * vec4( aSphere.xyz, 1.f ) );
	vSphereRad	   = aSphere.w;
	vSphereColor   = aColor.rgb;

	// Compute normalized view vector.
	vDotViewSpherePos		  = dot( vViewSpherePos, vViewSpherePos );
	float dSphereCenter = sqrt( vDotViewSpherePos );
	vec3	view		  = vViewSpherePos / dSphereCenter;

	// Impostor in front of the sphere.
	vec3 viewImpPos = vViewSpherePos - vSphereRad * view;

	// Compute impostor size.
	float sinAngle = vSphereRad / dSphereCenter;
	float tanAngle = tan( asin( sinAngle ) );
	float impSize	 = tanAngle * length( viewImpPos );

	// Compute impostor vectors.
	// TODO: simplify normalize ? (vImpU.x == 0) but normalize should be hard optimized on GPU...
	// But for cross always better doing no calculation.
	// vImpU = normalize( cross( dir, vec3( 1.f, 0.f, 0.f ) ) ); becomes:
	vImpU = normalize( vec3( 0.f, view.z, -view.y ) );
	// TODO: simplify cross ? (vImpU.x == 0) but cross should be hard optimized on GPU...
	vImpV = cross( vImpU, view ) * impSize; // No need to normalize.
	vImpU *= impSize;

	gl_Position = vec4( viewImpPos, 1.f );
}
