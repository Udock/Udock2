#version 410 core
//#extension GL_ARB_shading_language_packing : enable

uniform sampler2D colorTexture;
uniform usampler2D gbViewPositionNormal;
layout( location = 0 ) out vec4 fragColor;

// Floating point pack/unpack functions are part of the GLSL ES 300 specification used by web and mobile.
uint float2half(uint f) {
    uint e = f & uint(0x7f800000);
    if (e <= uint(0x38000000)) {
        return uint(0);
    } else {
        return ((f >> uint(16)) & uint(0x8000)) |
                (((e - uint(0x38000000)) >> uint(13)) & uint(0x7c00)) |
                ((f >> uint(13)) & uint(0x03ff));
    }
}

uint half2float(uint h) {
    uint h_e = h & uint(0x7c00);
    return ((h & uint(0x8000)) << uint(16)) | uint((h_e >> uint(10)) != uint(0)) * (((h_e + uint(0x1c000)) << uint(13)) | ((h & uint(0x03ff)) << uint(13)));
}

uint packHalf2x16(vec2 v) {
    return float2half(floatBitsToUint(v.x)) | float2half(floatBitsToUint(v.y)) << uint(16);
}

vec2 unpackHalf2x16(uint v) {
    return vec2(uintBitsToFloat(half2float(v & uint(0xffff))),
            uintBitsToFloat(half2float(v >> uint(16))));
}

struct UnpackedData
{
	vec3 viewPosition;
	vec3 normal;
	bool shade;
};

void unpackGBuffers( ivec2 px, out UnpackedData data )
{
	uvec4 viewPositionNormal = texelFetch( gbViewPositionNormal, px, 0 );

	vec2 tmp		  = unpackHalf2x16( viewPositionNormal.y );
	data.viewPosition = vec3( unpackHalf2x16( viewPositionNormal.x ), tmp.x );
	data.normal		  = vec3( tmp.y, unpackHalf2x16( viewPositionNormal.z ) );
	data.shade = viewPositionNormal.w != 0.f;
}

void main()
{
	ivec2 texCoord = ivec2( gl_FragCoord.xy );
	vec4 color = texelFetch( colorTexture, texCoord, 0 );

	UnpackedData data;
	unpackGBuffers( texCoord, data );

	if( !data.shade )
	{
		fragColor = color;
		return;
	}

	fragColor = vec4( pow(color.rgb, vec3(1./2.2)), 1.f );
}
