#version 410 core

uniform sampler2D depthTexture;
layout(std140) uniform LinearizeDepthUniforms 
{
	vec4 uClipInfo;
};

layout( location = 0 ) out float linearDepth;

float linearizeDepth( const vec4 clipInfo, const float depth )
{
	return clipInfo[ 0 ] / ( clipInfo[ 1 ] - depth * clipInfo[ 2 ] );
}

void main() 
{ 
	linearDepth = linearizeDepth( uClipInfo, texelFetch( depthTexture, ivec2( gl_FragCoord.xy ), 0 ).x ); 
}
