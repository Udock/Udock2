#version 410 core
//#extension GL_ARB_shading_language_packing : enable

uniform usampler2D gbViewPositionNormal;
uniform sampler2D colorTexture;
uniform sampler2D linearDepthTexture;
layout (location = 0) out vec4 fragColor;

layout (std140) uniform ContoursUniforms
{
    float uNear;
    float uFar;
    float uStartDecay;
};

// Floating point pack/unpack functions are part of the GLSL ES 300 specification used by web and mobile.
uint float2half(uint f) {
    uint e = f & uint(0x7f800000);
    if (e <= uint(0x38000000)) {
        return uint(0);
    } else {
        return ((f >> uint(16)) & uint(0x8000)) |
        (((e - uint(0x38000000)) >> uint(13)) & uint(0x7c00)) |
        ((f >> uint(13)) & uint(0x03ff));
    }
}

uint half2float(uint h) {
    uint h_e = h & uint(0x7c00);
    return ((h & uint(0x8000)) << uint(16)) | uint((h_e >> uint(10)) != uint(0)) * (((h_e + uint(0x1c000)) << uint(13)) | ((h & uint(0x03ff)) << uint(13)));
}

uint packHalf2x16(vec2 v) {
    return float2half(floatBitsToUint(v.x)) | float2half(floatBitsToUint(v.y)) << uint(16);
}

vec2 unpackHalf2x16(uint v) {
    return vec2(uintBitsToFloat(half2float(v & uint(0xffff))),
                uintBitsToFloat(half2float(v >> uint(16))));
}

const int offset = 1;

const ivec2 offsets[9] = ivec2[](
    ivec2(-offset, offset),
    ivec2(0, offset),
    ivec2(offset, offset),
    ivec2(-offset, 0),
    ivec2(0, 0),
    ivec2(offset, 0),
    ivec2(-offset, -offset),
    ivec2(0.0f, -offset),
    ivec2(offset, -offset)
);

// sharpen
const float KernelSize = 9.;
const float kernel[9] = float[](
    -1., -1., -1.,
    -1., 8., -1.,
    -1., -1., -1.
);

struct UnpackedData
{
    vec3 viewPosition;
    vec3 normal;
    bool shade;
};

void unpackGBuffers(ivec2 px, out UnpackedData data)
{
    uvec4 viewPositionNormal = texelFetch(gbViewPositionNormal, px, 0);

    vec2 tmp = unpackHalf2x16(viewPositionNormal.y);
    data.viewPosition = vec3(unpackHalf2x16(viewPositionNormal.x), tmp.x);
    data.normal = vec3(tmp.y, unpackHalf2x16(viewPositionNormal.z));
    data.shade = viewPositionNormal.w == 1.f;
}

void main()
{
    ivec2 textureCoordinates = ivec2(gl_FragCoord.xy);

    UnpackedData data;
    unpackGBuffers(textureCoordinates, data);
    fragColor = texelFetch(colorTexture, textureCoordinates, 0);

    if (!data.shade) return;

    float minDepth = 1e4;
    float edge = 0;
    for (int i = 0; i < 9; i++)
    {
        float currentValue = texelFetch(linearDepthTexture, textureCoordinates + offsets[i], 0).r;
        edge += currentValue * kernel[i];

        minDepth = min(minDepth, currentValue);
    }
    edge /= KernelSize;

    float weight = clamp((1. - ((minDepth - uNear) / (uFar * uStartDecay))), 0., 1.);
    fragColor = mix(fragColor, vec4(0.), abs(edge) * weight);
} 
