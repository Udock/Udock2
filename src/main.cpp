#include "Udock/Udock.hpp"
#include <fmt/format.h>
#include <iostream>

int main( int /* argc */, char ** /* argv */ )
{
    try
    {
        udk::Udock app {};
        app.run();
    }
    catch ( std::exception & e )
    {
        std::cerr << fmt::format( "{}\n", e.what() );

        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
