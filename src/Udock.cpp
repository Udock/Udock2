#include "Udock/Udock.hpp"
#include "Udock/Renderer/Pass/BakedSkybox.hpp"
#include "Udock/Renderer/Pass/HosekSkybox.hpp"
#include "Udock/Ui/State/Docking.hpp"
#include "Udock/Ui/Window/EnergyWindow.hpp"
#include "Udock/Ui/Window/HelpWindow.hpp"
#include "Udock/Ui/Window/InformationWindow.hpp"
#include "Udock/Ui/Window/SettingsWindow.hpp"
#include "Udock/Utils/Io.hpp"
#include "portable-file-dialogs.h"
#include <fmt/format.h>

namespace udk
{
    Udock::Udock() :
        m_window( { 1280, 720 } ), m_physicalWorld( &m_system ), m_currentSkyboxType( m_settings.skyboxType )
    {
        Entity      cameraEntity    = m_system.create();
        Transform & cameraTransform = cameraEntity.emplace<Transform>();
        cameraTransform.setPosition( { 0.f, 0.f, -1.f } );
        cameraEntity.emplace<Target>( glm::vec3 {} );
        Camera & camera = cameraEntity.emplace<Camera>( &cameraTransform, glm::vec3 {} );
        m_camera        = &camera;

        m_renderer = std::make_unique<Renderer>( &m_window, &m_settings, &m_system, &m_physicalWorld, cameraEntity );

        UiManager & uiManager = m_renderer->getUiManager();
        m_state = std::make_unique<DockingState>( &uiManager, &m_settings, &m_system, &m_physicalWorld, cameraEntity );

        m_moleculeListener = Listener<Molecule>( &m_system, [ this ]( Entity, Event ) { m_state->resetCamera(); } );
        m_moleculeListener.subscribe( Event::Construct );

        createUi();

        switch ( m_currentSkyboxType )
        {
        case SkyboxType::Realistic:
            m_renderer->setSkybox( std::make_unique<HosekSkybox>( m_settings.turbidity, m_settings.groundAlbedo ) );
            break;
        case SkyboxType::Space:
            m_renderer->setSkybox( std::make_unique<BakedSkybox>( "./resources/Skybox/MilkyWay" ) );
            break;
        default: break;
        }
    }

    Udock::~Udock() { SDL_Quit(); }

    void Udock::run()
    {
        while ( m_window.update() )
        {
            const UiInput & lastInput = m_window.getLastUiInput();

            if ( m_currentSkyboxType != m_settings.skyboxType )
            {
                m_currentSkyboxType = m_settings.skyboxType;
                switch ( m_currentSkyboxType )
                {
                case SkyboxType::Realistic:
                    m_renderer->setSkybox(
                        std::make_unique<HosekSkybox>( m_settings.turbidity, m_settings.groundAlbedo ) );
                    break;
                case SkyboxType::Space:
                    m_renderer->setSkybox( std::make_unique<BakedSkybox>( "./resources/Skybox/MilkyWay" ) );
                    break;
                default: m_renderer->setSkybox( {} );
                }
            }

            if ( m_settings.skyboxType == SkyboxType::Realistic )
            {
                HosekSkybox * skybox = reinterpret_cast<HosekSkybox *>( m_renderer->getSkybox() );
                skybox->setGroundAlbedo( m_settings.groundAlbedo );
                skybox->setTurbidity( m_settings.turbidity );
            }

            if ( lastInput.windowResized )
            {
                const glm::vec2 floatWindowSize = lastInput.windowSize;
                m_camera->setAspectRatio( floatWindowSize.x / floatWindowSize.y );
                m_renderer->resize( lastInput.windowSize );
            }

            if ( lastInput.droppedFile && !m_physicalWorld.getFreezeStatus() )
            {
                m_system.create( udk::readMolecule( *lastInput.droppedFile ) );
            }

            // Software logic
            m_state->update( lastInput );

            m_physicalWorld.update();
            m_renderer->render();

            if ( m_state->done() )
                m_state = m_state->next();
        }
    }

    void Udock::createUi()
    {
        // Main Menu
        UiManager & uiManager = m_renderer->getUiManager();
        MenuBar     menuBar {};
        menuBar.addField( //
            Button( "Open",
                    [ this ]
                    {
                        if ( !m_physicalWorld.getFreezeStatus() )
                        {
                            pfd::open_file fileDialog { "Choose files to read",
                                                        "./samples/",
                                                        { "Mol2 files (.mol2)", "*.mol2" },
                                                        pfd::opt::multiselect };

                            const std::vector<std::string> result = fileDialog.result();
                            for ( auto const & name : result )
                            {
                                m_system.create( udk::readMolecule( name ) );
                            }
                        }
                    } ) );

        menuBar.addField( Button( "Settings", [ & ] { toggleWindow<SettingsWindow>( "Settings", &m_settings ); } ) );
        menuBar.addField( Button( "Reset molecules position",
                                  [ this ]
                                  {
                                      if ( !m_physicalWorld.getFreezeStatus() )
                                      {
                                          m_system.resetMoleculePosition();
                                          m_state->resetCamera();
                                      }
                                  } ) );
        menuBar.addField( Button( "Reset camera", [ this ] { m_state->resetCamera(); } ) );
        menuBar.addField( Button( "Help", [ & ] { toggleWindow<HelpWindow>( "Help" ); } ) );
        uiManager.setMenuBar( std::move( menuBar ) );

        // Static windows
        uiManager.addWindow( //
            "Information",
            std::make_unique<InformationWindow>(
                &m_settings,
                &m_system,
                &m_physicalWorld,
                [ this ]( entt::entity toRemove )
                {
                    if ( !m_physicalWorld.getFreezeStatus() )
                        m_system.erase( toRemove );
                },
                [ this ]()
                {
                    if ( !m_physicalWorld.getFreezeStatus() )
                        m_system.clear<Molecule>();
                } ) );
        uiManager.addWindow( "Energy", std::make_unique<EnergyWindow>( &m_settings, &m_system, &m_physicalWorld ) );
    }
} // namespace udk
