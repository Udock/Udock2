#include "Udock/Utils/Logger.hpp"
#include "Udock/Utils/Chrono.hpp"

namespace udk
{
    void Logger::log( const std::string_view level, const std::string_view message ) const
    {
        std::cout << fmt::format( "[{}][{}] {}", udk::getNowString(), level, message ) << std::endl;
    }

} // namespace udk
