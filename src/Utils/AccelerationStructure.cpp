#include "Udock/Utils/AccelerationStructure.hpp"
#include "Udock/Data/Molecule.hpp"
#include "Udock/Utils/Logger.hpp"
#include <glm/gtx/component_wise.hpp>
#include <set>

namespace udk
{
    AccelerationStructure::AccelerationStructure( const Molecule * const molecule )
    {
        const Aabb & aabb = molecule->getDimensions();

        m_origin                    = aabb.minimum - 2.1f;
        const glm::vec3   worldSize = glm::abs( aabb.maximum + 2.1f - m_origin );
        const std::size_t size      = 16;
        m_gridSize                  = glm::ivec3( size );
        m_grid.resize( size * size * size );
        m_cellSize = worldSize / static_cast<float>( size );

        build( molecule );
    }

    // Based on particle simulation samples
    // Ref: https://github.com/zchee/cuda-sample/blob/master/5_Simulations/particles/particles_kernel_impl.cuh#L109
    glm::ivec3 AccelerationStructure::getGridPosition( const glm::vec3 & position ) const
    {
        glm::ivec3 gridPos;
        gridPos.x = static_cast<int>( std::floor( ( position.x - m_origin.x ) / m_cellSize.x ) );
        gridPos.y = static_cast<int>( std::floor( ( position.y - m_origin.y ) / m_cellSize.y ) );
        gridPos.z = static_cast<int>( std::floor( ( position.z - m_origin.z ) / m_cellSize.z ) );
        return gridPos;
    }
    std::size_t AccelerationStructure::getGridHash( glm::ivec3 gridPosition ) const
    {
        gridPosition.x = gridPosition.x & ( m_gridSize.x - 1 ); // wrap grid, assumes size is power of 2
        gridPosition.y = gridPosition.y & ( m_gridSize.y - 1 );
        gridPosition.z = gridPosition.z & ( m_gridSize.z - 1 );
        return ( ( gridPosition.z * m_gridSize.y ) * m_gridSize.x ) + ( gridPosition.y * m_gridSize.x )
               + gridPosition.x;
    }

    void AccelerationStructure::getNearElements( const glm::vec3 &                     worldPos,
                                                 float                                 radius,
                                                 std::function<void( std::size_t id )> callback ) const
    {
        const int range = glm::compMax( glm::ivec3( glm::ceil( glm::vec3( radius ) / m_cellSize ) ) );

        std::vector<std::size_t> result {};

        const glm::ivec3 gridPosition = getGridPosition( worldPos );
        const glm::ivec3 minGrid      = glm::max( gridPosition - range, glm::ivec3 {} );
        const glm::ivec3 maxGrid      = glm::min( gridPosition + range, m_gridSize - 1 );
        for ( int x = minGrid.x; x <= maxGrid.x; x++ )
        {
            for ( int y = minGrid.y; y <= maxGrid.y; y++ )
            {
                for ( int z = minGrid.z; z <= maxGrid.z; z++ )
                {
                    glm::ivec3  current { x, y, z };
                    std::size_t hash = getGridHash( current );

                    const std::pair<std::size_t, std::size_t> & cubeContent = m_grid[ hash ];
                    for ( std::size_t i = cubeContent.first; i < cubeContent.second; i++ )
                    {
                        callback( m_hashes[ i ].first );
                    }
                }
            }
        }
    }

    void AccelerationStructure::build( const Molecule * const molecule )
    {
        const std::vector<Atom> & atoms = molecule->getAtoms();
        m_hashes.resize( atoms.size() );

        for ( std::size_t i = 0; i < atoms.size(); i++ )
        {
            const glm::ivec3 gridPosition = getGridPosition( atoms[ i ].pos );
            m_hashes[ i ]                 = { i, getGridHash( gridPosition ) };
        }

        std::sort( m_hashes.begin(),
                   m_hashes.end(),
                   []( const std::pair<std::size_t, std::size_t> & a, const std::pair<std::size_t, std::size_t> & b )
                   { return a.second < b.second; } );

        m_grid[ m_hashes[ 0 ].second ].first = 0;
        for ( std::size_t i = 1; i < m_hashes.size(); i++ )
        {
            const std::pair<std::size_t, std::size_t> & pred    = m_hashes[ i - 1 ];
            const std::pair<std::size_t, std::size_t> & current = m_hashes[ i ];
            if ( pred.second != current.second )
            {
                m_grid[ pred.second ].second   = i;
                m_grid[ current.second ].first = i;
            }
        }
        m_grid[ m_hashes.back().second ].second = m_hashes.size();
    }

} // namespace udk
