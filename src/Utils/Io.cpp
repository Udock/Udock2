#include "Udock/Utils/Io.hpp"
#include "Udock/Utils/Logger.hpp"
#include <chemfiles.hpp>
#include <optional>

#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h>

namespace udk
{
    static void prepareChemfiles()
    {
#ifndef NDEBUG
        chemfiles::warning_callback_t callback = []( const std::string & p_log ) { UDK_WARNING( p_log ); };
#else
        chemfiles::warning_callback_t callback = []( const std::string & p_log ) { /* UDK_WARNING( p_log ); */ };
#endif
        chemfiles::set_warning_callback( callback );
    }

    // Strongly based on TinyObjLoader viewer example
    // Reference: https://github.com/tinyobjloader/tinyobjloader/blob/master/examples/viewer/viewer.cc#L458
    Mesh readObj( const Path & path )
    {
        tinyobj::ObjReader       reader;
        tinyobj::ObjReaderConfig config;
        config.mtl_search_path = path.parent_path().string();
        reader.ParseFromFile( path.string() );

        const tinyobj::attrib_t &             attributes = reader.GetAttrib();
        const std::vector<tinyobj::shape_t> & shapes     = reader.GetShapes();
        std::vector<tinyobj::material_t>      materials  = reader.GetMaterials();

        if ( !reader.Valid() )
        {
            const std::string warnings = reader.Warning();
            const std::string errors   = reader.Error();

            UDK_WARNING( warnings );
            UDK_ERROR( errors );
            UDK_ERROR( "Failed to load {}", path.string() );

            return {};
        }

        // Append 'default' material
        materials.push_back( tinyobj::material_t() );
        std::vector<glm::vec3> vertices { attributes.vertices.size() };
        std::vector            normals { attributes.vertices.size(), glm::vec3 { 1.f } };
        for ( std::size_t i = 0; i < attributes.vertices.size(); i += 3 )
        {
            vertices[ i / 3 ] = glm::vec3 { attributes.vertices[ i + 0 ],
                                            attributes.vertices[ i + 1 ],
                                            attributes.vertices[ i + 2 ] };
        }

        std::vector<uint32_t> indices {};
        indices.reserve( attributes.vertices.size() );
        std::vector<glm::vec3> colors { attributes.vertices.size() };
        {
            for ( const tinyobj::shape_t & shape : shapes )
            {
                std::vector<float> buffer; // pos(3float), normal(3float), color(3float)
                for ( std::size_t f = 0; f < shape.mesh.indices.size() / 3; f++ )
                {
                    tinyobj::index_t idx0 = shape.mesh.indices[ 3 * f + 0 ];
                    indices.emplace_back( idx0.vertex_index );
                    tinyobj::index_t idx1 = shape.mesh.indices[ 3 * f + 1 ];
                    indices.emplace_back( idx1.vertex_index );
                    tinyobj::index_t idx2 = shape.mesh.indices[ 3 * f + 2 ];
                    indices.emplace_back( idx2.vertex_index );

                    int current_material_id = shape.mesh.material_ids[ f ];
                    if ( ( current_material_id < 0 )
                         || ( current_material_id >= static_cast<int>( materials.size() ) ) )
                    {
                        // Invalid material ID. Use default material.
                        current_material_id = static_cast<int>(
                            materials.size() - 1 ); // Default material is added to the last item in `materials`.
                    }

                    glm::vec3 diffuse;
                    for ( int i = 0; i < 3; i++ )
                    {
                        diffuse[ i ] = materials[ current_material_id ].diffuse[ i ];
                    }

                    colors[ idx0.vertex_index ] = diffuse;
                    colors[ idx1.vertex_index ] = diffuse;
                    colors[ idx2.vertex_index ] = diffuse;

                    normals[ idx0.vertex_index ]
                        = glm::normalize( glm::vec3 { attributes.normals[ idx0.normal_index * 3 + 0 ],
                                                      attributes.normals[ idx0.normal_index * 3 + 1 ],
                                                      attributes.normals[ idx0.normal_index * 3 + 2 ] } );
                    normals[ idx1.vertex_index ]
                        = glm::normalize( glm::vec3 { attributes.normals[ idx1.normal_index * 3 + 0 ],
                                                      attributes.normals[ idx1.normal_index * 3 + 1 ],
                                                      attributes.normals[ idx1.normal_index * 3 + 2 ] } );
                    normals[ idx2.vertex_index ]
                        = glm::normalize( glm::vec3 { attributes.normals[ idx2.normal_index * 3 + 0 ],
                                                      attributes.normals[ idx2.normal_index * 3 + 1 ],
                                                      attributes.normals[ idx2.normal_index * 3 + 2 ] } );
                }
            }
        }

        return { std::move( vertices ), std::move( normals ), std::move( indices ), std::move( colors ) };
    }

    Molecule readMolecule( const Path & path )
    {
        prepareChemfiles();

        chemfiles::Trajectory trajectory { path.string() };
        UDK_DEBUG( "{} frames found", trajectory.nsteps() );

        if ( trajectory.nsteps() == 0 )
        {
            throw std::runtime_error( "Trajectory is empty" );
        }

        chemfiles::Frame                        frame    = trajectory.read();
        const chemfiles::Topology &             topology = frame.topology();
        const std::vector<chemfiles::Residue> & residues = topology.residues();
        const std::vector<chemfiles::Bond> &    bonds    = topology.bonds();

        if ( frame.size() != topology.size() )
        {
            throw std::runtime_error( "Data count missmatch" );
        }

        // Set molecule properties.
        std::string moleculeName;
        if ( frame.get( "name" ) )
        {
            moleculeName = frame.get( "name" )->as_string();
        }

        // Check properties, same for all atoms/residues?
#ifndef NDEBUG
        if ( frame.size() > 0 && frame[ 0 ].properties() )
        {
            std::string propAtom = std::to_string( frame[ 0 ].properties()->size() ) + " properties in atoms:";
            for ( chemfiles::property_map::const_iterator it = frame[ 0 ].properties()->begin();
                  it != frame[ 0 ].properties()->end();
                  ++it )
            {
                propAtom += " " + it->first;
            }
            UDK_DEBUG( propAtom );
        }

        if ( residues.size() > 0 )
        {
            std::string propResidue = std::to_string( residues[ 0 ].properties().size() ) + " properties in residues:";
            for ( chemfiles::property_map::const_iterator it = residues[ 0 ].properties().begin();
                  it != residues[ 0 ].properties().end();
                  ++it )
            {
                propResidue += " " + it->first;
            }
            UDK_DEBUG( propResidue );
        }
#endif // !NDEBUG

        // If no residue, create a fake one.
        // TODO: check file format instead of residue count?
        if ( residues.size() == 0 )
        {
            UDK_INFO( "No residues found" );
            chemfiles::Residue residue = chemfiles::Residue( "" );
            for ( std::size_t i = 0; i < frame.size(); ++i )
            {
                residue.add_atom( i );
            }
            frame.add_residue( residue );
        }

        std::vector<Chain>   molChains {};
        std::vector<Residue> molResidues {};
        std::vector<Atom>    molAtoms {};

        std::unordered_map<std::size_t, std::vector<const chemfiles::Bond *>> mapResidueBonds {};

        for ( std::size_t residueIdx = 0; residueIdx < residues.size(); ++residueIdx )
        {
            const chemfiles::Residue & residue = residues[ residueIdx ];

            // Check if chain name changed.
            std::string chainName = residue.properties().get( "chainname" ).value_or( "" ).as_string();
            std::string chainId   = residue.properties().get( "chainid" ).value_or( "" ).as_string();

            if ( molChains.empty() || chainName != molChains.back().name )
            {
                molChains.emplace_back( Chain { std::move( chainId ), std::move( chainName ), residueIdx, 0 } );
            }

            Chain & currentChain = molChains.back();
            currentChain.residueNb++;

            // Create residue.
            molResidues.emplace_back(
                Residue { residue.id().value_or( 0 ), residue.name(), *residue.begin(), residue.size(), 0, 0 } );

            mapResidueBonds.emplace( residueIdx, std::vector<const chemfiles::Bond *>() );
            if ( residue.size() == 0 )
            {
                UDK_WARNING( "Empty residue found" );
            }

            for ( std::size_t atomId : residue )
            {
                const chemfiles::Atom & atom = topology[ atomId ];

                const chemfiles::span<chemfiles::Vector3D> & positions = frame.positions();
                const chemfiles::Vector3D &                  position  = positions[ atomId ];
                glm::vec3 atomPosition { position[ 0 ], position[ 1 ], position[ 2 ] };

                Sybyl sybylType = fromString( atom.get( "sybyl" ).value_or( "" ).as_string() );
                molAtoms.emplace_back( Atom { atom.name(),
                                              sybylType,
                                              std::move( atomPosition ),
                                              getRadius( sybylType ),
                                              getEpsilon( sybylType ),
                                              static_cast<float>( atom.charge() ),
                                              residueIdx } );
            }
        }

        // Bonds.
        // Sort by residus.
        // Map with residue index to keep the order.
        std::vector<const chemfiles::Bond *> bondsExtraResidues = std::vector<const chemfiles::Bond *>();
        for ( const auto & bond : bonds )
        {
            const std::size_t bondStart = bond[ 0 ];
            const std::size_t bondEnd   = bond[ 1 ];

            const std::size_t residueStart = molAtoms[ bondStart ].residueId;
            const std::size_t residueEnd   = molAtoms[ bondEnd ].residueId;

            if ( residueStart == residueEnd )
            {
                // Create vector if needed.
                mapResidueBonds[ residueStart ].emplace_back( &bond );
            }
            else
            {
                bondsExtraResidues.emplace_back( &bond );
            }
        }

        std::vector<Bond> molBonds;
        // Create models.
        std::size_t counter = 0;
        for ( const auto & pair : mapResidueBonds )
        {
            Residue & residue = molResidues[ pair.first ];

            const std::vector<const chemfiles::Bond *> & vectorBonds = pair.second;

            residue.startBondId = counter;
            residue.bondNb      = vectorBonds.size();

            for ( std::size_t i = 0; i < vectorBonds.size(); ++i, ++counter )
            {
                const chemfiles::Bond & bond = *vectorBonds[ i ];
                molBonds.emplace_back( Bond { bond[ 0 ], bond[ 1 ] } );
            }
        }

        // Bonds between residues.
        for ( std::size_t i = 0; i < bondsExtraResidues.size(); ++i, ++counter )
        {
            const chemfiles::Bond & bond         = *bondsExtraResidues[ i ];
            std::size_t             residueStart = molAtoms[ bond[ 0 ] ].residueId;
            std::size_t             residueEnd   = molAtoms[ bond[ 1 ] ].residueId;

            molBonds.emplace_back( Bond { bond[ 0 ], bond[ 1 ] } );

            molResidues[ residueStart ].extraBounds.emplace_back( counter );
            molResidues[ residueEnd ].extraBounds.emplace_back( counter );
        }

        return { std::move( moleculeName ),
                 std::move( molAtoms ),
                 std::move( molChains ),
                 std::move( molResidues ),
                 std::move( molBonds ) };
    }

    void writeMolecule( const Path & path, const Molecule & molecule, const glm::mat4 & transform )
    {
        prepareChemfiles();

        Aabb aabb    = molecule.getDimensions();
        aabb.minimum = transform * glm::vec4( aabb.minimum, 1.f );
        aabb.maximum = transform * glm::vec4( aabb.maximum, 1.f );

        const glm::vec3 range = glm::abs( aabb.maximum - aabb.minimum );

        chemfiles::Frame    frame = chemfiles::Frame { chemfiles::UnitCell(
            { static_cast<double>( range.x ), static_cast<double>( range.y ), static_cast<double>( range.z ) } ) };
        chemfiles::Topology topology {};

        const std::vector<Atom> &  atoms    = molecule.getAtoms();
        const std::vector<Chain>   chains   = molecule.getChains();
        const std::vector<Residue> residues = molecule.getResidues();
        const std::vector<Bond>    bonds    = molecule.getBonds();

        for ( const Chain & chain : chains )
        {
            const std::size_t endResidueId = chain.startResidueId + chain.residueNb;
            for ( std::size_t i = chain.startResidueId; i < endResidueId; i++ )
            {
                const Residue & residue = residues[ i ];

                chemfiles::Residue finalResidue { residue.name, residue.id };
                finalResidue.set( "chainname", chain.name );
                finalResidue.set( "chainid", chain.id );
                const std::size_t endAtomId = residue.startAtomId + residue.atomNb;
                for ( std::size_t j = residue.startAtomId; j < endAtomId; j++ )
                {
                    const Atom & atom = atoms[ j ];

                    chemfiles::Atom finalAtom { atom.name, "" };
                    finalAtom.set( "sybyl", std::string( toString( atom.sybylType ) ) );
                    finalAtom.set_charge( static_cast<double>( atom.charge ) );

                    const glm::vec3 position = transform * glm::vec4( atom.pos, 1.f );
                    frame.add_atom( finalAtom, { position.x, position.y, position.z } );
                    topology.add_atom( finalAtom );

                    finalResidue.add_atom( j );
                }

                topology.add_residue( finalResidue );
                frame.add_residue( std::move( finalResidue ) );
            }
        }

        for ( const Bond & bond : bonds )
        {
            frame.add_bond( bond.first, bond.second );
            topology.add_bond( bond.first, bond.second );
        }
        frame.set_topology( std::move( topology ) );
        frame.set( "name", std::string( molecule.getName() ) );

        auto trajectory = chemfiles::Trajectory( path.string(), 'w' );
        trajectory.write( frame );
    }
} // namespace udk
