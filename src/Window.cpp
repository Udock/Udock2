#include "Udock/Window.hpp"
#include "Udock/Utils/Logger.hpp"
#include <backends/imgui_impl_sdl.h>

#ifdef UDOCK_RENDERER_OPENGL
#include <SDL_opengl.h>
#endif // UDOCK_RENDERER_OPENGL

namespace udk
{
    const std::string Window::WindowTitle = "Udock";

    Window::Window( glm::ivec2 size ) : m_size( std::move( size ) )
    {
        SDL_Init( SDL_INIT_VIDEO | SDL_INIT_JOYSTICK | SDL_INIT_GAMECONTROLLER );

#ifdef UDOCK_RENDERER_OPENGL
#ifdef __APPLE__
        SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );
#endif // __APPLE__

        SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 4 );
        SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 1 );
        SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
        SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 24 );
        SDL_GL_SetAttribute( SDL_GL_STENCIL_SIZE, 8 );

        // control vsync
        SDL_GL_SetSwapInterval( 1 );
#endif

        SDL_Window * handle = SDL_CreateWindow( WindowTitle.c_str(),
                                                SDL_WINDOWPOS_CENTERED,
                                                SDL_WINDOWPOS_CENTERED,
                                                static_cast<int>( m_size.x ),
                                                static_cast<int>( m_size.y ),
#ifdef UDOCK_RENDERER_OPENGL
                                                SDL_WINDOW_OPENGL |
#endif
                                                    SDL_WINDOW_RESIZABLE );

        m_window       = WindowHandle { handle, closeWindow };
        m_lastTimeStep = SDL_GetPerformanceCounter();
    }

    bool Window::update()
    {
        m_lastInput.reset();

        // Based on ImGui
        // https://github.com/PlathC/VTX-Light/blob/3e61d46ef302ac3b48a344dd4953481b77e26cfe/extern/imgui/imgui_impl_sdl.cpp#L466
        static const uint64_t sdlFrequency = SDL_GetPerformanceFrequency();
        const uint64_t        now          = SDL_GetPerformanceCounter();
        m_lastInput.deltaTime = static_cast<float>( static_cast<double>( now - m_lastTimeStep ) / sdlFrequency );
        m_lastTimeStep        = now;

        auto & io = ImGui::GetIO();

        bool      running = true;
        SDL_Event windowEvent;
        while ( SDL_PollEvent( &windowEvent ) )
        {
            ImGui_ImplSDL2_ProcessEvent( &windowEvent );
            switch ( windowEvent.type )
            {
            case SDL_QUIT: running = false; break;
            case SDL_MOUSEWHEEL:
            {
                m_lastInput.deltaMouseWheel = windowEvent.wheel.y;
                break;
            }
            case SDL_MOUSEMOTION:
            {
                m_lastInput.deltaMousePosition.x = windowEvent.motion.xrel;
                m_lastInput.deltaMousePosition.y = windowEvent.motion.yrel;
                m_lastInput.mousePosition.x      = windowEvent.motion.x;
                m_lastInput.mousePosition.y      = windowEvent.motion.y;
                break;
            }
            case SDL_KEYDOWN:
            {
                // Do not handle key if the mouse is on ImGui's window
                if ( io.WantCaptureMouse )
                    break;

                m_lastInput.keysPressed.emplace( windowEvent.key.keysym.scancode );
                m_lastInput.keysDown.emplace( windowEvent.key.keysym.scancode );
                break;
            }
            case SDL_KEYUP:
            {
                m_lastInput.keysPressed.erase( windowEvent.key.keysym.scancode );
                m_lastInput.keysUp.emplace( windowEvent.key.keysym.scancode );
                break;
            }
            case SDL_MOUSEBUTTONDOWN:
            {
                // Do not handle key if the mouse is on ImGui's window
                if ( io.WantCaptureMouse )
                    break;

                switch ( windowEvent.button.button )
                {
                case SDL_BUTTON_LEFT:
                {
                    m_lastInput.mouseLeftPressed = true;
                    m_lastInput.mouseLeftClicked = true;
                    m_lastInput.doubleLeftClick  = windowEvent.button.clicks == 2;
                    break;
                }
                case SDL_BUTTON_RIGHT:
                    m_lastInput.mouseRightPressed = true;
                    m_lastInput.mouseRightClicked = true;
                    break;
                case SDL_BUTTON_MIDDLE:
                    m_lastInput.mouseMiddlePressed = true;
                    m_lastInput.mouseMiddleClicked = true;
                    break;
                }
                break;
            }
            case SDL_MOUSEBUTTONUP:
            {
                switch ( windowEvent.button.button )
                {
                case SDL_BUTTON_LEFT: m_lastInput.mouseLeftPressed = false; break;
                case SDL_BUTTON_RIGHT: m_lastInput.mouseRightPressed = false; break;
                case SDL_BUTTON_MIDDLE: m_lastInput.mouseMiddlePressed = false; break;
                }
                break;
            }
            case SDL_WINDOWEVENT:
            {
                switch ( windowEvent.window.event )
                {
                case SDL_WINDOWEVENT_SIZE_CHANGED:
                {
                    m_lastInput.windowResized = true;
                    m_size                    = { windowEvent.window.data1, windowEvent.window.data2 };
                    break;
                }
                }
                break;
            }
            case SDL_DROPFILE:
            {
                m_lastInput.droppedFile = std::string( windowEvent.drop.file );
                SDL_free( windowEvent.drop.file );
                break;
            }
            case SDL_CONTROLLERDEVICEADDED:
            {
                updateControllers();
                break;
            }
            case SDL_CONTROLLERDEVICEREMOVED:
            {
                m_controller = {};
                m_lastInput.controller.reset();
                break;
            }
            case SDL_CONTROLLERAXISMOTION:
            {
                if ( !m_lastInput.controller )
                    continue;

                float normalized = windowEvent.caxis.value / static_cast<float>( std::numeric_limits<int16_t>::max() );
                normalized       = std::clamp( normalized, -1.f, 1.f );

                if ( std::abs( normalized ) < 1e-1f )
                    normalized = 0.f;

                switch ( windowEvent.caxis.axis )
                {
                case 0: m_lastInput.controller->left.x = normalized; break;
                case 1: m_lastInput.controller->left.y = normalized; break;
                case 2: m_lastInput.controller->right.x = normalized; break;
                case 3: m_lastInput.controller->right.y = normalized; break;
                case 4: m_lastInput.controller->leftShoulder = normalized; break;
                case 5: m_lastInput.controller->rightShoulder = normalized; break;
                }
                break;
            }
            case SDL_CONTROLLERBUTTONDOWN:
            {
                const SDL_GameControllerButton button
                    = static_cast<SDL_GameControllerButton>( windowEvent.cbutton.button );
                m_lastInput.controller->keysPressed.emplace( button );
                m_lastInput.controller->keysDown.emplace( button );
                break;
            }
            case SDL_CONTROLLERBUTTONUP:
            {
                const SDL_GameControllerButton button
                    = static_cast<SDL_GameControllerButton>( windowEvent.cbutton.button );
                m_lastInput.controller->keysPressed.erase( button );
                m_lastInput.controller->keysUp.emplace( button );
                break;
            }
            }
        }

        m_lastInput.windowSize = m_size;

        return running;
    }

    void Window::updateControllers()
    {
        m_lastInput.controller.reset();
        m_controller         = GameControllerHandle {};
        const int joyStickNb = SDL_NumJoysticks();
        for ( int i = 0; i < joyStickNb; i++ )
        {
            if ( SDL_IsGameController( i ) )
            {
                SDL_GameController * handle = SDL_GameControllerOpen( i );
                if ( !handle )
                {
                    UDK_WARNING( "Failed to open game controller {}", SDL_GetError() );
                    continue;
                }

                const char * name = SDL_GameControllerName( handle );
                UDK_INFO( "Using  Gamepad {}!", name );

                // The system select only the first controller.
                m_controller           = GameControllerHandle { handle };
                m_lastInput.controller = GameController {};
                break;
            }
        }
    }

    void Window::closeWindow( SDL_Window * handle ) { SDL_DestroyWindow( handle ); }

    Window::GameControllerHandle::GameControllerHandle( SDL_GameController * controller ) : handle( controller )
    {
        haptic = SDL_HapticOpenFromJoystick( SDL_GameControllerGetJoystick( controller ) );
        if ( SDL_HapticRumbleSupported( haptic ) )
            hasHaptic = !SDL_HapticRumbleInit( haptic );
        if ( !hasHaptic )
            SDL_HapticClose( haptic );
    }

    Window::GameControllerHandle::GameControllerHandle( GameControllerHandle && other )
    {
        std::swap( handle, other.handle );
        std::swap( haptic, other.haptic );
        std::swap( hasHaptic, other.hasHaptic );
    }
    Window::GameControllerHandle & Window::GameControllerHandle::operator=( GameControllerHandle && other )
    {
        std::swap( handle, other.handle );
        std::swap( haptic, other.haptic );
        std::swap( hasHaptic, other.hasHaptic );
        return *this;
    }

    Window::GameControllerHandle::~GameControllerHandle()
    {
        if ( handle && SDL_GameControllerGetAttached( handle ) )
        {
            SDL_HapticClose( haptic );
            SDL_GameControllerClose( handle );
            handle = nullptr;
        }
    }

} // namespace udk
