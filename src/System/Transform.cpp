#include "Udock/System/Transform.hpp"
#include <glm/gtc/quaternion.hpp>

namespace udk
{
    Transform::Transform( glm::vec3 position, glm::dquat rotation ) :
        m_position( std::move( position ) ), m_rotation( std::move( rotation ) )
    {
    }
    Transform::~Transform()
    {
        if ( m_parent )
            m_parent->removeChild( this );

        for ( Transform * child : m_children )
            child->setParent( nullptr );
    }

    void Transform::setParent( Transform * parent )
    {
        if ( m_parent )
            set( get() );

        m_parent = parent;
        if ( m_parent )
        {
            m_parent->addChild( this );
            set( get() * glm::inverse( m_parent->get() ) );
        }
    }

    void Transform::addChild( Transform * child ) { m_children.emplace( child ); }
    void Transform::removeChild( Transform * child ) { m_children.erase( child ); }

    void Transform::set( const glm::mat4 & transform )
    {
        m_rotation = glm::quat_cast( transform );
        m_position = transform[ 3 ];
    }

    glm::mat4 Transform::get() const
    {
        glm::mat4 transform = glm::mat4_cast( m_rotation );
        transform[ 3 ]      = glm::vec4( m_position, 1.f );

        if ( m_parent )
            transform = m_parent->get() * transform;
        return transform;
    }

    void Transform::lookAt( glm::vec3 target )
    {
        if ( m_parent )
            target = glm::inverse( m_parent->get() ) * glm::vec4( target, 1.f );

        // Based on https://stackoverflow.com/a/49824672
        glm::vec3   targetDirection = target - m_position;
        const float distance        = glm::length( targetDirection );
        if ( distance < 1e-6f )
        {
            m_rotation = glm::dquat { 1., 0., 0., 0. };
        }
        else
        {
            targetDirection /= distance;

            const glm::vec3 up       = getUp();
            float           cosTheta = glm::dot( targetDirection, up );
            if ( glm::abs( glm::abs( cosTheta ) - 1.f ) < 1e-6f )
                m_rotation = glm::quatLookAt( targetDirection, getDirection() );
            else
                m_rotation = glm::quatLookAt( targetDirection, up );
        }
    }

    glm::vec3 Transform::getPosition() const { return get()[ 3 ]; }
    glm::vec3 Transform::getRelativePosition() const { return m_position; }

    glm::dquat Transform::getRotation() const
    {
        glm::dquat rotation = m_rotation;
        if ( m_parent )
            rotation = glm::quat_cast( get() * glm::mat4( glm::mat4_cast( rotation ) ) );

        return rotation;
    }
    glm::dquat Transform::getRelativeRotation() const { return m_rotation; }

    void Transform::setRotation( const glm::dquat & newRotation ) { m_rotation = newRotation; }
    void Transform::setPosition( const glm::vec3 & newPosition ) { m_position = newPosition; }

    void Transform::move( const glm::vec3 & delta )
    {
        m_position += getLeft() * delta.x;
        m_position += getUp() * delta.y;
        m_position += getDirection() * delta.z;
    }

    void Transform::rotate( const glm::vec3 & angles ) { m_rotation = m_rotation * glm::dquat( angles ); }

    void Transform::rotatePitch( const double pitch )
    {
        m_rotation = m_rotation * glm::dquat( glm::dvec3( -pitch, 0.0, 0.0 ) );
    }

    void Transform::rotateYaw( const double yaw )
    {
        m_rotation = m_rotation * glm::dquat( glm::dvec3( 0.0, -yaw, 0.0 ) );
    }

    void Transform::rotateRoll( const double roll )
    {
        m_rotation = m_rotation * glm::dquat( glm::dvec3( 0.0, 0.0, roll ) );
    }

    void Transform::rotateAround( const glm::dquat & rotation, const glm::dvec3 & target, const double distance )
    {
        m_rotation = m_rotation * rotation;
        m_position = m_rotation * glm::dvec3( 0.0, 0.0, distance ) + target;
    }

    glm::vec3 Transform::getDirection() const { return glm::mat3_cast( getRotation() ) * -glm::vec3( 0.f, 0.f, 1.f ); }

    glm::vec3 Transform::getLeft() const { return glm::mat3_cast( getRotation() ) * -glm::vec3( 1.f, 0.f, 0.f ); }

    glm::vec3 Transform::getUp() const { return glm::mat3_cast( getRotation() ) * glm::vec3( 0.f, 1.f, 0.f ); }

    void Transform::reset()
    {
        m_position = {};
        m_rotation = glm::dquat { 1., 0., 0., 0. };
    }

} // namespace udk
