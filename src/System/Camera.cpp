#include "Udock/System/Camera.hpp"
#include "Udock/Physics/RigidBody.hpp"
#include "Udock/Utils/Logger.hpp"

namespace udk
{
    Camera::Camera( Transform * transform, const glm::vec3 & target, const float aspectRatio ) :
        m_transform( transform ), m_aspectRatio( aspectRatio )
    {
        m_transform->lookAt( target );
    }

    void Camera::lookAt( const Aabb & aabb )
    {
        const glm::vec3 target   = ( aabb.minimum + aabb.maximum ) * .5f;
        const float     bbRadius = glm::compMax( glm::abs( aabb.maximum - target ) );
        const float     distance = bbRadius / std::tan( glm::radians( m_fov ) * .5f );

        const glm::vec3 direction = m_transform->getDirection();
        m_transform->setPosition( target - direction * 2.f * distance );
        m_transform->lookAt( target );
    }

    glm::mat4 Camera::getViewMatrix() const { return glm::inverse( m_transform->get() ); }
    glm::mat4 Camera::getProjectionMatrix() const
    {
        return glm::perspective( glm::radians( m_fov ), m_aspectRatio, m_near, m_far );
    }

    glm::vec3 Camera::screenToWorld( const glm::ivec2 & screenCoordinates,
                                     const glm::ivec2 & screenDimension,
                                     bool               invY ) const
    {
        const glm::vec4 viewport = { 0.0f, 0.0f, screenDimension.x, screenDimension.y };

        glm::vec3 mousePosition = { screenCoordinates, 1.f };
        if ( invY )
            mousePosition.y = viewport[ 3 ] - mousePosition.y;
        return glm::unProject( mousePosition, getViewMatrix(), getProjectionMatrix(), viewport );
    }

} // namespace udk
