#include "Udock/System/System.hpp"
#include "Udock/Core/Math.hpp"
#include "Udock/Data/Mesh.hpp"
#include "Udock/Data/MolecularSurface.hpp"
#include "Udock/Physics/RigidBody.hpp"
#include "entt/entity/handle.hpp"
#include <glm/gtc/type_ptr.hpp>

namespace udk
{
    System::System() = default;

    Entity System::create()
    {
        const entt::entity currentEntity = m_registry.create();
        return { m_registry, currentEntity };
    }

    Entity System::create( Molecule molecule )
    {
#ifdef NDEBUG
        constexpr static float MeshDetails = 1e-2f;
#else // NDEBUG
        constexpr static float MeshDetails = 2e-2f;
#endif

        const Aabb &    aabb = molecule.getDimensions();
        const glm::vec3 size = glm::abs( aabb.maximum - aabb.minimum );

        const float meshCubeSize = ( size.x + size.y + size.z ) / 3.f * MeshDetails;
        Mesh        ses          = getSolventExcludedSurface( molecule, meshCubeSize );

        const std::size_t stepIdx = ( m_positionCounter++ ) % 4;
        glm::vec3         step {};
        bool              updateStepping = false;
        switch ( stepIdx )
        {
        case 0: step = glm::vec3 { -size.x, 0.f, 0.f }; break;
        case 1: step = glm::vec3 { size.x, 0.f, 0.f }; break;
        case 2: step = glm::vec3 { 0.f, -size.y, 0.f }; break;
        case 3:
        {
            step           = glm::vec3 { 0.f, size.y, 0.f };
            updateStepping = true;
            break;
        }
        }

        const glm::vec3 currentPosition = m_lastPosition + step;

        m_aabb.minimum = glm::min( m_aabb.minimum, currentPosition + aabb.minimum );
        m_aabb.maximum = glm::max( m_aabb.maximum, currentPosition + aabb.maximum );

        Entity currentEntity = create();

        std::unique_ptr<MeshCollider> meshCollider = std::make_unique<MeshCollider>( ses );

        Transform & newTransform = currentEntity.emplace<Transform>( currentPosition );
        RigidBody & newBody      = currentEntity.emplace<RigidBody>( std::move( meshCollider ), newTransform );

        currentEntity.emplace<Molecule>( std::move( molecule ) );
        currentEntity.emplace<Mesh>( std::move( ses ) );

        if ( updateStepping )
            m_lastPosition = glm::vec3( m_aabb.maximum.x + size.x * 2.f, 0.f, 0.f );

        return currentEntity;
    }

    void System::erase( const entt::entity toRemove )
    {
        if ( m_registry.all_of<RigidBody>( toRemove ) )
        {
            m_aabb           = {};
            auto rigidBodies = m_registry.view<RigidBody>();
            for ( const entt::entity currentEntity : rigidBodies )
            {
                if ( currentEntity == toRemove )
                    continue;

                const RigidBody & rigidbody = m_registry.get<RigidBody>( currentEntity );
                const Aabb        aabb      = rigidbody.getBoundingBox();

                m_aabb.minimum = glm::min( m_aabb.minimum, aabb.minimum );
                m_aabb.maximum = glm::max( m_aabb.maximum, aabb.maximum );
            }
        }

        m_registry.destroy( toRemove );

        std::size_t moleculeCount = m_registry.view<Molecule>().size();
        if ( moleculeCount == 0 )
        {
            m_positionCounter = 0;
            m_aabb            = { glm::vec3 {}, glm::vec3 {} };
            m_lastPosition    = {};
        }
    }

    void System::resetMoleculePosition()
    {
        m_positionCounter    = 0;
        const auto molecules = m_registry.view<Molecule>();
        for ( const entt::entity entity : molecules )
        {
            Molecule & molecule   = m_registry.get<Molecule>( entity );
            Transform & transform = m_registry.get<Transform>( entity );
            const Aabb &    aabb  = molecule.getDimensions();
            const glm::vec3 size  = glm::abs( aabb.maximum - aabb.minimum );

            const std::size_t stepIdx = ( m_positionCounter++ ) % 4;
            glm::vec3         step {};
            bool              updateStepping = false;
            switch ( stepIdx )
            {
            case 0: step = glm::vec3 { -size.x, 0.f, 0.f }; break;
            case 1: step = glm::vec3 { size.x, 0.f, 0.f }; break;
            case 2: step = glm::vec3 { 0.f, -size.y, 0.f }; break;
            case 3:
            {
                step           = glm::vec3 { 0.f, size.y, 0.f };
                updateStepping = true;
                break;
            }
            }

            const glm::vec3 currentPosition = m_lastPosition + step;

            transform.setPosition( currentPosition );

            m_aabb.minimum = glm::min( m_aabb.minimum, currentPosition + aabb.minimum );
            m_aabb.maximum = glm::max( m_aabb.maximum, currentPosition + aabb.maximum );

            if ( updateStepping )
                m_lastPosition = glm::vec3( m_aabb.maximum.x + size.x * 2.f, 0.f, 0.f );
        }
    }
} // namespace udk
