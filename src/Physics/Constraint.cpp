#include "Udock/Physics/Constraint.hpp"

namespace udk
{
    Anchor::Anchor( Entity selectedParent, glm::vec3 worldPosition ) :
        parent( selectedParent ),
        relativePosition( glm::inverse( parent.get<Transform>().get() ) * glm::vec4( worldPosition, 1.f ) )
    {
    }

    glm::vec3 Anchor::getWorldPosition() const
    {
        return parent.get<Transform>().get() * glm::vec4( relativePosition, 1.f );
    }

    void LineConstraint::apply( const glm::vec3 & worldCenter, float strength )
    {
        Anchor & firstConstraint  = firstChild.get<Anchor>();
        RigidBody & firstBody     = firstConstraint.parent.get<RigidBody>();
        Transform& firstTransform = firstConstraint.parent.get<Transform>();
        firstBody.setLinearVelocity( {} );
        firstBody.setAngularVelocity( {} );

        Anchor & secondConstraint   = secondChild.get<Anchor>();
        RigidBody & secondBody      = secondConstraint.parent.get<RigidBody>();
        Transform & secondTransform = secondConstraint.parent.get<Transform>();
        secondBody.setLinearVelocity( {} );
        secondBody.setAngularVelocity( {} );

        const glm::vec3 p1    = firstConstraint.getWorldPosition();
        const glm::vec3 p2    = secondConstraint.getWorldPosition();
        glm::vec3       force = ( p2 - p1 ) * strength;

        const glm::vec3 m1 = firstTransform.getPosition();
        const glm::vec3 d1 = p1 - m1;

        const glm::vec3 m2 = secondTransform.getPosition();
        const glm::vec3 d2 = p2 - m2;

        if ( glm::dot( d1, d2 ) > 0.f )
        {
            const glm::vec3 torque1 = glm::cross( d1, force );
            const glm::vec3 torque2 = glm::cross( d2, -force );

            firstBody.applyTorque( torque1 );
            secondBody.applyTorque( torque2 );
        }
        else
        {
            firstBody.applyForce( force, d1 );
            secondBody.applyForce( -force, d2 );

            // force to center
            const glm::vec3 f2c1 = worldCenter - m1;
            const glm::vec3 f2c2 = worldCenter - m2;

            firstBody.applyForce( f2c1, d1 );
            secondBody.applyForce( -f2c2, d2 );
        }
    }

} // namespace udk
