#include "Udock/Physics/RigidBody.hpp"
#include "Udock/Utils/Logger.hpp"
#include <BulletCollision/CollisionShapes/btConvexHullShape.h>
#include <BulletCollision/CollisionShapes/btSphereShape.h>
#include <glm/gtc/type_ptr.hpp>

namespace udk
{
    MeshCollider::MeshCollider( const Mesh & surface ) : m_vertexArray( std::make_unique<btTriangleIndexVertexArray>() )
    {
        const int vertexNb   = static_cast<int>( surface.vertices.size() );
        const int triangleNb = static_cast<int>( surface.indices.size() / 3 );

        btIndexedMesh indexedMesh;
        indexedMesh.m_numTriangles        = triangleNb;
        indexedMesh.m_numVertices         = vertexNb;
        indexedMesh.m_triangleIndexStride = sizeof( uint32_t ) * 3;
        indexedMesh.m_vertexStride        = sizeof( glm::vec3 );
        indexedMesh.m_triangleIndexBase   = reinterpret_cast<const unsigned char *>( surface.indices.data() );
        indexedMesh.m_vertexBase          = reinterpret_cast<const unsigned char *>( surface.vertices.data() );

        m_vertexArray->addIndexedMesh( indexedMesh );

        std::unique_ptr<btGImpactMeshShape> impactMesh = std::make_unique<btGImpactMeshShape>( m_vertexArray.get() );
        impactMesh->setMargin( -0.1f );
        impactMesh->updateBound();

        m_collisionShape = std::move( impactMesh );
    }

    ConvexCollider::ConvexCollider( const std::vector<glm::vec3> & surface )
    {
        m_collisionShape = std::make_unique<btConvexHullShape>( reinterpret_cast<const btScalar *>( surface.data() ),
                                                                static_cast<int>( surface.size() ),
                                                                static_cast<int>( sizeof( glm::vec3 ) ) );
    }

    SphereCollider::SphereCollider( float radius ) : Collider( std::make_unique<btSphereShape>( radius ) ) {}

    RigidBody::RigidBody( std::unique_ptr<Collider> && collider, const Transform & initialTransform ) :
        m_collider( std::move( collider ) )
    {
        btTransform startTransform {};
        startTransform.setFromOpenGLMatrix( glm::value_ptr( initialTransform.get() ) );
        m_motionState = std::make_unique<btDefaultMotionState>( startTransform );

        constexpr btScalar DefaultMass = 1.f;
        btVector3          localInertia( 0, 0, 0 );

        m_collider->getHandle()->calculateLocalInertia( DefaultMass, localInertia );
        btRigidBody::btRigidBodyConstructionInfo rbInfo {
            DefaultMass, m_motionState.get(), m_collider->getHandle(), localInertia
        };
        m_handle = std::make_unique<btRigidBody>( rbInfo );
        m_handle->activate();
        m_handle->setUserPointer( this );
    }

    glm::vec3 RigidBody::getAngularVelocity() const { return toGlm( m_handle->getAngularVelocity() ); }
    glm::vec3 RigidBody::getLinearVelocity() const { return toGlm( m_handle->getLinearVelocity() ); }
    void      RigidBody::setAngularVelocity( const glm::vec3 & v ) { m_handle->setAngularVelocity( toBullet( v ) ); }
    void      RigidBody::setLinearVelocity( const glm::vec3 & v ) { m_handle->setLinearVelocity( toBullet( v ) ); }

    void RigidBody::applyForce( const glm::vec3 & force, const glm::vec3 & relativePosition )
    {
        m_handle->activate();
        m_handle->applyForce( toBullet( force ), toBullet( relativePosition ) );
    }

    void RigidBody::applyCentralForce( const glm::vec3 & force )
    {
        m_handle->activate();
        m_handle->applyCentralForce( toBullet( force ) );
    }

    void RigidBody::applyTorque( const glm::vec3 & torque )
    {
        m_handle->activate();
        m_handle->applyTorque( toBullet( torque ) );
    }

    void RigidBody::ignoreCollisionWith( const RigidBody & other )
    {
        m_handle->setIgnoreCollisionCheck( other.m_handle.get(), true );
    }

    void RigidBody::setIgnoreCollision( bool value )
    {
        if ( value )
            m_handle->setCollisionFlags( m_handle->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE );
        else
            m_handle->setCollisionFlags( m_handle->getCollisionFlags() & ~btCollisionObject::CF_NO_CONTACT_RESPONSE );
    }

    void RigidBody::setCollisionCallback( CollisionCallback callback ) { m_collisionCallback = std::move( callback ); }

    void RigidBody::setFreeze( bool freezeStatus )
    {
        if ( freezeStatus )
        {
            m_handle->setLinearFactor( toBullet( {} ) );
            m_handle->setAngularFactor( toBullet( {} ) );
            setAngularVelocity( {} );
            setLinearVelocity( {} );
        }
        else
        {
            m_handle->setLinearFactor( toBullet( glm::vec3 { 1.f } ) );
            m_handle->setAngularFactor( toBullet( glm::vec3 { 1.f } ) );
        }

        m_freezeStatus = freezeStatus;
    }

    Aabb RigidBody::getBoundingBox() const
    {
        btVector3 min, max;
        m_handle->getAabb( min, max );
        return { toGlm( min ), toGlm( max ) };
    }

    void RigidBody::update( float deltaTime, Transform & toUpdate )
    {
        if ( m_freezeStatus )
            return;

        toUpdate.set( getTransform() );

        const glm::vec3 originalLinearVelocity  = getLinearVelocity();
        const glm::vec3 originalAngularVelocity = getAngularVelocity();
        setLinearVelocity( originalLinearVelocity - originalLinearVelocity * deltaTime * 2.f );
        setAngularVelocity( originalAngularVelocity - originalAngularVelocity * deltaTime * 2.f );
    }

    glm::mat4 RigidBody::getTransform() const
    {
        glm::mat4 transformationMatrix;
        m_handle->getWorldTransform().getOpenGLMatrix( glm::value_ptr( transformationMatrix ) );
        return transformationMatrix;
    }

    void RigidBody::setTransform( const glm::mat4 & newTransform )
    {
        btTransform transform;
        transform.setFromOpenGLMatrix( glm::value_ptr( newTransform ) );
        m_motionState->setWorldTransform( transform );
        m_handle->setWorldTransform( transform );
    }

    void RigidBody::notifyCollisionWith( RigidBody & other, const glm::vec3 & contactPoint )
    {
        if ( m_collisionCallback )
            m_collisionCallback( *this, other, contactPoint );
    }

} // namespace udk
