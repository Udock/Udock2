#include "Udock/Physics/Projectile.hpp"
#include "Udock/Physics/PhysicalWorld.hpp"
#include "Udock/Physics/RigidBody.hpp"
#include "Udock/Utils/Logger.hpp"

namespace udk
{
    Projectile::Projectile( Entity                entity,
                            Entity                hitEntity,
                            glm::vec3             hitPoint,
                            const float           radius,
                            const float           lifeTimeInSeconds,
                            ProjectileHitCallback callback ) :
        m_entity( entity ),
        m_hitEntity( hitEntity ), m_hitPoint( hitPoint ), m_radius( radius ), m_lifeTimeInSeconds( lifeTimeInSeconds ),
        m_callback( std::move( callback ) )
    {
        m_entity.get<RigidBody>().setCollisionCallback(
            [ this ]( RigidBody & current, RigidBody & other, const glm::vec3 & contactPoint )
            {
                if ( m_callback && m_lifeTimeInSeconds > 0.f )
                    m_callback( m_hitEntity, m_hitPoint );

                m_lifeTimeInSeconds = 0.f;
            } );
    }

    void Projectile::setHitCallback( ProjectileHitCallback callback ) { m_callback = std::move( callback ); }

    void Projectile::fire( const glm::vec3 & direction, const float strength )
    {
        assert( m_entity.all_of<RigidBody>() && "Projectile's entity must contains a rigidbody" );

        RigidBody & body = m_entity.get<RigidBody>();
        m_lastForce      = direction * strength;
        body.applyForce( m_lastForce, glm::vec3 { 0.f, 0.f, -m_radius * 10.f } );
        m_chrono.start();
    }

    void Projectile::update()
    {
        if ( m_chrono.isStarted() )
        {
            // It seems that sometimes, the projectile loses its velocity.
            RigidBody & body = m_entity.get<RigidBody>();
            if ( body.getLinearVelocity() == glm::vec3 {} )
                body.applyForce( m_lastForce, glm::vec3 { 0., 0., -m_radius * 10.f } );

            if ( m_chrono.currentTime() >= m_lifeTimeInSeconds && !m_entity.all_of<Destroyable>() )
            {
                m_entity.emplace<Destroyable>();
                return;
            }
        }
    }

} // namespace udk
