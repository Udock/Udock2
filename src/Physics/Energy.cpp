#include "Udock/Physics/Energy.hpp"
#include "Udock/Utils/Logger.hpp"
#include <glm/gtc/random.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/quaternion.hpp>

namespace udk
{
    float lennardJonesCoulombic( const glm::vec3 & p1,
                                 float             r1,
                                 float             e1,
                                 float             c1,
                                 const glm::vec3 & p2,
                                 float             r2,
                                 float             e2,
                                 float             c2 )
    {
        const float d = glm::length( p2 - p1 );

        // Avoid 0 division
        if ( d < 1e-1f )
            return std::numeric_limits<float>::max();

        // 8-6 Lennard-Jones potential
        // http://www.sklogwiki.org/SklogWiki/index.php/8-6_Lennard-Jones_potential
        const float rmOutD  = ( r1 + r2 ) / d;
        float       rmOutD6 = rmOutD * rmOutD * rmOutD;
        rmOutD6             = rmOutD6 * rmOutD6;

        const float rmOutD8 = rmOutD6 * rmOutD * rmOutD;
        const float epsilon = std::sqrt( e1 * e2 );
        const float contact = 3.f * epsilon * rmOutD8 - 4.f * epsilon * rmOutD6;

        // Coulombic
        // "f is a conversion factor for converting the electrostatics term to kcal/mol. We used f = 332.0522
        // according to the AMBER12 documentation. We used a distance dependent dielectric constant of \mathcal{E}_0 =
        // 20 that resulted in balanced contributions of t different terms of the scoring function." Udock, the
        // interactive docking entertainment system, Levieux et al. 2014 Reference:
        // https://cedric.cnam.fr/fichiers/art_3149.pdf
        const float charge = ( 332.0522f / 20.f ) * ( c1 * c2 / d );

        return contact + charge;
    }

    float computeEnergyBetweenMolecules( const std::vector<Atom> &         firstAtoms,
                                         const glm::mat4 &                 firstTransform,
                                         const std::vector<Atom> &         secondAtoms,
                                         const AccelerationStructure &     secondAccelerationStructures,
                                         const glm::mat4 &                 secondTransform,
                                         float                             maxDistance,
                                         const EnergyComputationFunction & energyComputation )
    {
        float result = 0.f;

        const glm::mat4 secondInverseTransform = glm::inverse( secondTransform );
        for ( const Atom & first : firstAtoms )
        {
            const glm::vec4 firstPosition = secondInverseTransform * firstTransform * glm::vec4( first.pos, 1.f );
            secondAccelerationStructures.getNearElements( firstPosition,
                                                          maxDistance,
                                                          [ & ]( std::size_t secondId )
                                                          {
                                                              const Atom & second = secondAtoms[ secondId ];
                                                              result += energyComputation( firstPosition,
                                                                                           first.radius,
                                                                                           first.epsilon,
                                                                                           first.charge,
                                                                                           second.pos,
                                                                                           second.radius,
                                                                                           second.epsilon,
                                                                                           second.charge );
                                                          } );
        }

        return result;
    }

    float computeEnergy( const EnergyComputationData & data, const EnergyComputationFunction & energyComputation )
    {
        float result = 0.f;
        for ( std::size_t i = 0; i < data.entities.size(); i++ )
        {
            for ( std::size_t j = i + 1; j < data.entities.size(); j++ )
            {
                result += computeEnergyBetweenMolecules( data.entities[ i ].atoms,
                                                         data.entities[ i ].transform,
                                                         data.entities[ j ].atoms,
                                                         data.entities[ j ].accelerationStructures,
                                                         data.entities[ j ].transform,
                                                         data.maxDistance,
                                                         energyComputation );
            }
        }

        return result;
    }

    void optimizationStep( const std::size_t                 toOptimizeId,
                           const EnergyComputationFunction & energyComputation,
                           const float                       stepSize,
                           EnergyComputationData &           data,
                           float &                           lastDelta,
                           glm::mat4 &                       bestTransform,
                           float &                           bestEnergy )
    {
        glm::mat4 transform { 1.f };
        if ( lastDelta >= 0.f )
        {
            const glm::vec3 transformationVector
                = { udk::random( -1.f, 1.f ), udk::random( -1.f, 1.f ), udk::random( -1.f, 1.f ) };
            if ( udk::random( 0, 1 ) == 1 )
                transform[ 3 ] = glm::vec4( stepSize * transformationVector, 1.f );
            else
                transform = glm::rotate( transform, glm::pi<float>() * .25f * stepSize, transformationVector );
        }
        else
        {
            transform = bestTransform;
        }

        auto &          toOptimize = data.entities[ toOptimizeId ];
        const glm::mat4 save       = toOptimize.transform;
        toOptimize.transform       = transform * toOptimize.transform;

        const float result = computeEnergy( data, energyComputation );
        lastDelta          = result - bestEnergy;
        if ( lastDelta < 0.f )
        {
            bestTransform = transform;
            bestEnergy    = result;
        }
        else
        {
            toOptimize.transform = save;
        }
    }

} // namespace udk
