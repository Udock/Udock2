#include "Udock/Physics/PhysicalWorld.hpp"
#include "BulletCollision/NarrowPhaseCollision/btRaycastCallback.h"
#include "Udock/Core/Math.hpp"
#include "Udock/Physics/Constraint.hpp"
#include "Udock/Physics/Projectile.hpp"
#include "Udock/System/System.hpp"
#include "Udock/Utils/Logger.hpp"
#include <BulletCollision/Gimpact/btGImpactCollisionAlgorithm.h>
#include <glm/gtc/type_ptr.hpp>

namespace udk
{
    PhysicalWorld::PhysicalWorld( System * system ) :
        m_system( system ), m_dispatcher( &m_collisionConfiguration ),
        m_world( &m_dispatcher, &m_overlappingPairCache, &m_solver, &m_collisionConfiguration )
    {
        btGImpactCollisionAlgorithm::registerAlgorithm( &m_dispatcher );
        m_world.setGravity( btVector3( 0.f, 0.f, 0.f ) );

        m_rigidBodyListener = Listener<RigidBody>( //
            m_system,
            [ this ]( Entity entity, Event eventType )
            {
                RigidBody & body = entity.get<RigidBody>();
                if ( eventType == Event::Construct )
                {
                    addRigidBody( body );
                }
                else if ( eventType == Event::Destroy )
                {
                    removeRigidBody( entity, body );
                }
            } );
        m_rigidBodyListener.subscribe( Event::Construct );
        m_rigidBodyListener.subscribe( Event::Destroy );

        // We only need slightly accurate collision detection
        m_world.getSolverInfo().m_numIterations = 1;
        m_world.getSolverInfo().m_solverMode    = SOLVER_SIMD;
    }

    void PhysicalWorld::setFreeze( bool freezeStatus )
    {
        m_freezeStatus               = freezeStatus;
        entt::registry & registry    = m_system->getRegistry();
        auto             rigidBodies = registry.view<RigidBody, Molecule>();
        for ( entt::entity currentEntity : rigidBodies )
        {
            RigidBody & rigidbody = registry.get<RigidBody>( currentEntity );
            rigidbody.setFreeze( freezeStatus );
        }
    }

    bool PhysicalWorld::intersect( const Ray & ray, glm::vec3 & hit )
    {
        auto [ body, hitPoint ] = intersect( ray );
        if ( !body )
            return false;

        hit = hitPoint;
        return true;
    }

    RigidBody * PhysicalWorld::intersect( const Ray & ray, Entity & hitEntity )
    {
        auto [ btBody, _ ] = intersect( ray );
        if ( !btBody )
            return nullptr;

        entt::registry & registry = m_system->getRegistry();
        RigidBody *      body     = static_cast<RigidBody *>( btBody->getUserPointer() );

        for ( auto entity : registry.view<RigidBody>() )
        {
            auto & r = registry.get<RigidBody>( entity );
            if ( &r == body )
            {
                hitEntity = Entity( registry, entity );
                return body;
            }
        }

        return nullptr;
    }

    bool PhysicalWorld::intersect( const Ray & ray, glm::vec3 & hit, RigidBody *& hitRigidBody )
    {
        auto [ btBody, position ] = intersect( ray );
        if ( !btBody )
            return false;

        RigidBody * body = static_cast<RigidBody *>( btBody->getUserPointer() );
        hit              = position;
        hitRigidBody     = body;
        return true;
    }

    bool PhysicalWorld::intersect( const Ray & ray, glm::vec3 & hit, Entity & hitEntity )
    {
        auto [ btBody, position ] = intersect( ray );
        if ( !btBody )
            return false;

        hit = position;

        entt::registry & registry = m_system->getRegistry();
        RigidBody *      body     = static_cast<RigidBody *>( btBody->getUserPointer() );
        for ( const auto entity : registry.view<RigidBody>() )
        {
            auto & r = registry.get<RigidBody>( entity );
            if ( &r == body )
            {
                hitEntity = Entity( registry, entity );
                return body;
            }
        }

        return false;
    }

    void PhysicalWorld::clearConstraints()
    {
        m_system->clear<Anchor>();
        m_system->clear<LineConstraint>();
    }

    void PhysicalWorld::applyConstraints( float strength )
    {
        const Aabb &    aabb   = m_system->getBoundingBox();
        const glm::vec3 center = ( aabb.minimum + aabb.maximum ) * .5f;

        entt::registry & registry        = m_system->getRegistry();
        const auto       lineConstraints = registry.view<LineConstraint>();
        for ( const entt::entity entity : lineConstraints )
        {
            LineConstraint & constraint = registry.get<LineConstraint>( entity );
            constraint.apply( center, strength );
        }
    }

    void PhysicalWorld::update()
    {
        entt::registry & registry    = m_system->getRegistry();
        const auto       rigidbodies = registry.view<RigidBody, Transform>();
        for ( const entt::entity currentEntity : rigidbodies )
        {
            auto & body      = registry.get<RigidBody>( currentEntity );
            auto & transform = registry.get<Transform>( currentEntity );
            body.setTransform( transform.get() );

            if ( registry.any_of<Projectile>( currentEntity ) )
            {
                Projectile & projectile = registry.get<Projectile>( currentEntity );
                projectile.update();
            }
        }

        constexpr float TimeStep = 1.f / 60.f;
        m_world.stepSimulation( TimeStep, 1 );

        btDispatcher * dispatcher = m_world.getDispatcher();
        for ( const entt::entity currentEntity : rigidbodies )
        {
            auto & body      = registry.get<RigidBody>( currentEntity );
            auto & transform = registry.get<Transform>( currentEntity );
            body.update( TimeStep, transform );
        }

        for ( int i = 0; i < dispatcher->getNumManifolds(); i++ )
        {
            btPersistentManifold * const manifold = dispatcher->getManifoldByIndexInternal( i );
            if ( !manifold->getNumContacts() )
                continue;

            RigidBody * const firstObject  = static_cast<RigidBody *>( manifold->getBody0()->getUserPointer() );
            RigidBody * const secondObject = static_cast<RigidBody *>( manifold->getBody1()->getUserPointer() );

            if ( !firstObject || !secondObject )
                continue;

            const int               contactPointNb      = manifold->getNumContacts();
            const btManifoldPoint * closestContactPoint = nullptr;
            float                   minDistance         = std::numeric_limits<float>::max();
            for ( int j = 0; j < contactPointNb; j++ )
            {
                const btManifoldPoint & current         = manifold->getContactPoint( j );
                const float             currentDistance = current.getDistance();
                if ( currentDistance < minDistance )
                {
                    minDistance         = currentDistance;
                    closestContactPoint = &current;
                }
            }

            if ( minDistance > 1e-8f )
                continue;

            if ( closestContactPoint )
            {
                firstObject->notifyCollisionWith( *secondObject, toGlm( closestContactPoint->getPositionWorldOnA() ) );
                secondObject->notifyCollisionWith( *firstObject, toGlm( closestContactPoint->getPositionWorldOnB() ) );
            }
        }

        const auto destroyable = registry.view<Destroyable>();
        for ( const entt::entity currentEntity : destroyable )
            registry.destroy( currentEntity );
    }

    std::pair<const btRigidBody *, glm::vec3> PhysicalWorld::intersect( const Ray & ray, const float maxDistance )
    {
        m_world.updateAabbs();
        m_world.computeOverlappingPairs();

        const glm::vec3 origin = ray.origin;
        const btVector3 ro { origin.x, origin.y, origin.z };
        const glm::vec3 direction = ray.origin + ray.direction * maxDistance;
        const btVector3 rd { direction.x, direction.y, direction.z };

        btCollisionWorld::ClosestRayResultCallback rayCallback { ro, rd };
        rayCallback.m_flags |= btTriangleRaycastCallback::kF_FilterBackfaces;

        m_world.rayTest( ro, rd, rayCallback );
        if ( rayCallback.hasHit() )
        {
            return std::make_pair( btRigidBody::upcast( rayCallback.m_collisionObject ),
                                   glm::vec3 { rayCallback.m_hitPointWorld.x(),
                                               rayCallback.m_hitPointWorld.y(),
                                               rayCallback.m_hitPointWorld.z() } );
        }

        return {};
    }

    void PhysicalWorld::addRigidBody( RigidBody & body )
    {
        m_world.addRigidBody( body.m_handle.get() );
        m_world.updateAabbs();
        m_world.computeOverlappingPairs();
    }

    void PhysicalWorld::removeRigidBody( Entity removedEntity, RigidBody & body )
    {
        m_world.removeRigidBody( body.m_handle.get() );

        entt::registry & registry        = m_system->getRegistry();
        const auto       lineConstraints = registry.view<LineConstraint>();
        for ( const entt::entity entity : lineConstraints )
        {
            LineConstraint &  constraint = registry.get<LineConstraint>( entity );
            const RigidBody & firstBody  = constraint.firstChild.get<Anchor>().parent.get<RigidBody>();
            const RigidBody & secondBody = constraint.secondChild.get<Anchor>().parent.get<RigidBody>();
            if ( &firstBody == &body || &secondBody == &body )
            {
                constraint.firstChild.destroy();
                constraint.secondChild.destroy();
                registry.destroy( entity );
            }
        }
        m_world.updateAabbs();
        m_world.computeOverlappingPairs();
        m_world.clearForces();
    }
} // namespace udk
