#include "Udock/Data/Molecule.hpp"

namespace udk
{
    Molecule::Molecule( std::string          name,
                        std::vector<Atom>    atoms,
                        std::vector<Chain>   chains,
                        std::vector<Residue> residues,
                        std::vector<Bond>    bonds ) :
        m_name( std::move( name ) ),
        m_atoms( std::move( atoms ) ), m_chains( std::move( chains ) ), m_residues( std::move( residues ) ),
        m_bonds( std::move( bonds ) ), m_aabb( getBoundingGeometries() ), m_accelerationStructure( this )
    {
    }

    float Molecule::getCharge( const glm::vec3 & position ) const
    {
        float charge = 0.0f;
        for ( const Atom & atom : m_atoms )
        {
            charge += static_cast<float>( atom.charge ) / glm::length( atom.pos - position );
        }
        return charge;
    }

    void Molecule::updateBarycenter()
    {
        m_barycenter = {};
        for ( const Atom & atom : m_atoms )
        {
            m_barycenter += atom.pos;
        }

        m_barycenter /= static_cast<float>( m_atoms.size() );
    }

    Aabb Molecule::getBoundingGeometries()
    {
        moveCenterToOrigin();

        m_radius = 0.f;
        glm::vec3 min { std::numeric_limits<float>::max() };
        glm::vec3 max { std::numeric_limits<float>::lowest() };
        for ( const Atom & atom : m_atoms )
        {
            m_radius = std::max( m_radius, glm::length( atom.pos - m_barycenter ) );

            min = glm::min( min, atom.pos - atom.radius );
            max = glm::max( max, atom.pos + atom.radius );
        }

        return { min, max };
    }

    void Molecule::moveCenterToOrigin()
    {
        updateBarycenter();
        for ( Atom & atom : m_atoms )
        {
            atom.pos -= m_barycenter;
        }

        updateBarycenter();
    }

} // namespace udk
