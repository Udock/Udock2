#include "Udock/Data/MolecularSurface.hpp"
#include "Udock/Physics/Energy.hpp"
#include "Udock/Settings.hpp"
#include "Udock/Utils/Logger.hpp"
#include <glm/gtx/norm.hpp>

#define MC_IMPLEM_ENABLE
#include <MC.h>

namespace udk
{
    static std::size_t linearizeGridPosition( const glm::ivec3 & gridPosition, const glm::vec3 & gridSize )
    {
        return static_cast<std::size_t>( ( gridPosition.z * gridSize.y + gridPosition.y ) * gridSize.x
                                         + gridPosition.x );
    }

    Mesh getSolventAccessibleSurface( const Molecule & molecule, float cubeSize, float probeRadius )
    {
        const auto [ min, max ] = molecule.getDimensions();

        const glm::vec3  origin   = min - 2.f * probeRadius - 4.f;
        const glm::ivec3 gridSize = glm::ivec3( glm::ceil( ( max - min + 2.f * probeRadius ) / cubeSize ) ) + 4;

        std::vector<float> field = sdSas( molecule, origin, gridSize, cubeSize, probeRadius );

        MC::mcMesh mesh;
        MC::marching_cube( field.data(), gridSize.x, gridSize.y, gridSize.z, mesh );

        std::vector vertices = std::vector<glm::vec3>( mesh.vertices.size() );
        std::memcpy( vertices.data(), mesh.vertices.data(), mesh.vertices.size() * sizeof( glm::vec3 ) );
        for ( glm::vec3 & vertex : vertices )
        {
            vertex *= cubeSize;
            vertex += origin;
        }

        std::vector normals = std::vector<glm::vec3>( mesh.normals.size() );
        std::memcpy( normals.data(), mesh.normals.data(), mesh.normals.size() * sizeof( glm::vec3 ) );

        std::vector charges = std::vector( vertices.size(), glm::vec3( 1.f ) );
        return { std::move( vertices ), std::move( normals ), std::move( mesh.indices ), std::move( charges ) };
    }

    std::vector<float> sdSas( const Molecule &   molecule,
                              const glm::vec3 &  origin,
                              const glm::ivec3 & gridSize,
                              const float        cubeSize,
                              const float        probeRadius )
    {
        std::vector field = std::vector( gridSize.x * gridSize.y * gridSize.z, std::numeric_limits<float>::max() );
        const auto  gridToWorld
            = [ & ]( const glm::ivec3 & gridPosition ) { return origin + glm::vec3( gridPosition ) * cubeSize; };

        const std::vector<Atom> & atoms = molecule.getAtoms();
        for ( const Atom & atom : atoms )
        {
            const glm::vec3 & position = atom.pos;
            const float       radius   = atom.radius + probeRadius;

            const glm::vec3  relativePosition = position - origin;
            const glm::ivec3 gridStart
                = glm::max( glm::ivec3( glm::trunc( ( relativePosition - radius ) / cubeSize ) ) - 1, glm::ivec3( 0 ) );
            const glm::ivec3 gridEnd
                = glm::min( glm::ivec3( glm::ceil( ( relativePosition + radius ) / cubeSize ) ) + 1, gridSize - 1 );

            for ( int x = gridStart.x; x <= gridEnd.x; x++ )
            {
                for ( int y = gridStart.y; y <= gridEnd.y; y++ )
                {
                    for ( int z = gridStart.z; z <= gridEnd.z; z++ )
                    {
                        const glm::ivec3  gridPosition = glm::ivec3( x, y, z );
                        const std::size_t index        = linearizeGridPosition( gridPosition, gridSize );
                        float &           fieldValue   = field[ index ];
                        const float       atomSignedDistance
                            = glm::length( gridToWorld( gridPosition ) - relativePosition ) - radius;

                        fieldValue = glm::min( fieldValue, atomSignedDistance );
                    }
                }
            }
        }

        return field;
    }

    Mesh getSolventExcludedSurface( const Molecule & molecule, float cubeSize, float probeRadius )
    {
        const auto [ min, max ] = molecule.getDimensions();

        const glm::vec3  origin   = min - 2.f * probeRadius;
        const glm::ivec3 gridSize = glm::ivec3( glm::ceil( ( max - min + 2.f * probeRadius ) / cubeSize ) ) + 4;

        std::vector        fieldCharges = std::vector( gridSize.x * gridSize.y * gridSize.z, 0.f );
        std::vector<float> field        = sdSes( molecule, origin, gridSize, cubeSize, probeRadius, fieldCharges );

        MC::mcMesh mesh;
        MC::marching_cube( field.data(), gridSize.x, gridSize.y, gridSize.z, mesh );

        std::vector vertices = std::vector<glm::vec3>( mesh.vertices.size() );
        std::memcpy( vertices.data(), mesh.vertices.data(), mesh.vertices.size() * sizeof( glm::vec3 ) );

        std::vector normals = std::vector<glm::vec3>( mesh.normals.size() );
        std::memcpy( normals.data(), mesh.normals.data(), mesh.normals.size() * sizeof( glm::vec3 ) );

        const std::vector<Atom> & atoms           = molecule.getAtoms();
        std::vector               verticesCharges = std::vector( vertices.size(), 0.f );

        float lowerRange = std::numeric_limits<float>::max();
        float upperRange = std::numeric_limits<float>::lowest();
        for ( std::size_t i = 0; i < vertices.size(); i++ )
        {
            glm::vec3 & vertex = vertices[ i ];

            vertex *= cubeSize;
            vertex += origin;

            // For every point of the surface, we calculate the mean of all the atomic partial charges within a 5A
            // radius sphere. Each atom's partial charge is divided by the squared distance to a point located 1.4A
            // above the surface point we calculate.
            const glm::vec3 pointAboveSurface = vertex + normals[ i ] * probeRadius;
            float           vertexCharge      = 0.f;

            const AccelerationStructure & as = molecule.getAccelerationStructure();
            as.getNearElements( vertex,
                                5.f,
                                [ & ]( const std::size_t neighborId )
                                {
                                    const Atom & neighbor = atoms[ neighborId ];
                                    vertexCharge += neighbor.charge / glm::length2( pointAboveSurface - neighbor.pos );
                                } );

            verticesCharges[ i ] = vertexCharge;

            lowerRange = std::min( lowerRange, vertexCharge );
            upperRange = std::max( upperRange, vertexCharge );
        }

        for ( std::size_t i = 0; i < mesh.indices.size(); i += 3 )
        {
            std::array<std::size_t, 3> vertexIds;
            float                      triangleCharge = 0.f;
            for ( std::size_t j = 0; j < 3; j++ )
            {
                const std::size_t current = mesh.indices[ i + j ];
                vertexIds[ j ]            = current;
                triangleCharge += verticesCharges[ current ];
            }
            triangleCharge /= 3.f;
            for ( std::size_t j = 0; j < 3; j++ )
            {
                const std::size_t current       = vertexIds[ j ];
                const float       currentCharge = ( verticesCharges[ current ] + triangleCharge ) * .5f;
                verticesCharges[ current ]      = currentCharge;
            }
        }

        for ( std::size_t i = 0; i < vertices.size(); i++ )
        {
            float & charge = verticesCharges[ i ];
            charge         = ( glm::smoothstep( lowerRange, upperRange, charge ) - .5f ) * 2.f;
        }

        // Convert charges to colors
        std::vector colors = std::vector<glm::vec3>( verticesCharges.begin(), verticesCharges.end() );
        return { std::move( vertices ), std::move( normals ), std::move( mesh.indices ), std::move( colors ) };
    }

    static std::vector<float> sdSes( const Molecule &     molecule,
                                     const glm::vec3 &    origin,
                                     const glm::ivec3 &   gridSize,
                                     const float          cubeSize,
                                     const float          probeRadius,
                                     std::vector<float> & charges )
    {
        std::vector field = std::vector( gridSize.x * gridSize.y * gridSize.z, std::numeric_limits<float>::max() );
        std::vector<glm::ivec3> boundaryCubes {};
        boundaryCubes.reserve( gridSize.x * gridSize.y );

        const auto gridToWorld
            = [ & ]( const glm::ivec3 & gridPosition ) { return origin + glm::vec3( gridPosition ) * cubeSize; };

        const std::vector<Atom> & atoms = molecule.getAtoms();
        for ( const Atom & atom : atoms )
        {
            const glm::vec3 & position = atom.pos;
            const float       radius   = atom.radius;

            const glm::vec3  relativePosition = position - origin;
            const glm::ivec3 gridStart        = glm::max(
                glm::ivec3( glm::trunc( ( relativePosition - radius - probeRadius ) / cubeSize ) ), glm::ivec3( 0 ) );
            const glm::ivec3 gridEnd
                = glm::min( glm::ivec3( ( relativePosition + radius + probeRadius ) / cubeSize ), gridSize - 1 );

            for ( int x = gridStart.x; x <= gridEnd.x; x++ )
            {
                for ( int y = gridStart.y; y <= gridEnd.y; y++ )
                {
                    for ( int z = gridStart.z; z <= gridEnd.z; z++ )
                    {
                        const glm::ivec3  gridPosition  = glm::ivec3 { x, y, z };
                        const std::size_t index         = linearizeGridPosition( gridPosition, gridSize );
                        const float       previousValue = field[ index ];

                        const glm::vec3 cubeCenter       = gridToWorld( gridPosition );
                        const float     distanceFromAtom = glm::length( cubeCenter - position );
                        float           signedDistance;
                        if ( distanceFromAtom < radius - cubeSize ) // Inside SES
                        {
                            signedDistance = -cubeSize;
                        }
                        else if ( distanceFromAtom < radius + probeRadius ) // In boundary
                        {
                            signedDistance = 0.f;
                        }
                        else
                        {
                            signedDistance = probeRadius;
                        }

                        if ( signedDistance < previousValue )
                        {
                            field[ index ] = signedDistance;

                            if ( signedDistance == 0.f )
                                boundaryCubes.emplace_back( gridPosition );
                        }
                    }
                }
            }
        }

        const int offset = static_cast<int>( std::ceil( probeRadius / cubeSize ) );
        for ( const glm::ivec3 & boundaryIndex : boundaryCubes )
        {
            const std::size_t currentIndex = linearizeGridPosition( boundaryIndex, gridSize );
            const float       baseValue    = field[ currentIndex ];
            if ( baseValue != 0.f )
            {
                charges[ currentIndex ] = 0.f;
                continue;
            }

            const glm::vec3  cubeCenter = gridToWorld( boundaryIndex );
            float            minValue   = std::numeric_limits<float>::max();
            bool             isInside   = true;
            const glm::ivec3 gridStart  = glm::max( boundaryIndex - offset, glm::ivec3( 0 ) );
            const glm::ivec3 gridEnd    = glm::min( boundaryIndex + offset, gridSize - 1 );

            for ( int x = gridStart.x; x <= gridEnd.x; x++ )
            {
                for ( int y = gridStart.y; y <= gridEnd.y; y++ )
                {
                    for ( int z = gridStart.z; z <= gridEnd.z; z++ )
                    {
                        const glm::ivec3  gridPosition = glm::ivec3( x, y, z );
                        const std::size_t index        = linearizeGridPosition( gridPosition, gridSize );
                        const float       fieldValue   = field[ index ];
                        if ( fieldValue >= probeRadius )
                        {
                            minValue = std::min( minValue, glm::distance( cubeCenter, gridToWorld( gridPosition ) ) );
                            isInside = false;
                        }
                    }
                }
            }

            field[ currentIndex ] = isInside ? -cubeSize : probeRadius - minValue;
        }

        return field;
    }

} // namespace udk
