#include "Udock/Data/Mesh.hpp"

namespace udk
{
    MeshView::MeshView( const Mesh & mesh ) :
        vertices( mesh.vertices ), normals( mesh.normals ), indices( mesh.indices ), colors( mesh.colors )
    {
    }
} // namespace udk
