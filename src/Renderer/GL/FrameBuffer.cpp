#include "Udock/Renderer/FrameBuffer.hpp"
#include "Udock/Utils/Logger.hpp"

namespace udk
{
    FrameBuffer::FrameBuffer( glm::ivec2 viewportSize ) : m_viewportSize( std::move( viewportSize ) )
    {
        glGenFramebuffers( 1, &m_handle );
        glBindFramebuffer( GL_FRAMEBUFFER, m_handle );
        glBindFramebuffer( GL_FRAMEBUFFER, 0 );
    }
    FrameBuffer::FrameBuffer( FrameBuffer && other ) noexcept :
        m_viewportSize( std::move( other.m_viewportSize ) ),
        m_handle( std::exchange( other.m_handle, GL_INVALID_VALUE ) )
    {
    }

    FrameBuffer & FrameBuffer::operator=( FrameBuffer && other ) noexcept
    {
        std::swap( m_viewportSize, other.m_viewportSize );
        std::swap( m_handle, other.m_handle );
        std::swap( m_attachments, other.m_attachments );

        return *this;
    }

    FrameBuffer::~FrameBuffer()
    {
        if ( glIsFramebuffer( m_handle ) )
        {
            glDeleteFramebuffers( 1, &m_handle );
            m_handle = GL_INVALID_VALUE;
        }
    }

    void FrameBuffer::synchronize() const
    {
#ifndef __APPLE__
        glMemoryBarrier( GL_FRAMEBUFFER_BARRIER_BIT );
#endif // __APPLE__
    }

    void FrameBuffer::setDepthBuffer( Attachment && depthBuffer )
    {
        m_depthBuffer = std::move( depthBuffer );
        glBindFramebuffer( GL_FRAMEBUFFER, m_handle );
        glFramebufferTexture2D( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_depthBuffer->m_handle, 0 );
        glBindFramebuffer( GL_FRAMEBUFFER, 0 );
    }

    void FrameBuffer::addAttachment( Attachment && attachment )
    {
        m_attachments.emplace_back( std::move( attachment ) );

        glBindFramebuffer( GL_FRAMEBUFFER, m_handle );
        glFramebufferTexture2D( GL_FRAMEBUFFER,
                                static_cast<GLenum>( GL_COLOR_ATTACHMENT0 + m_attachments.size() - 1 ),
                                GL_TEXTURE_2D,
                                m_attachments.back().m_handle,
                                0 );
        glBindFramebuffer( GL_FRAMEBUFFER, 0 );
    }

    const Attachment & FrameBuffer::getAttachment( std::size_t index ) const { return m_attachments[ index ]; }

    void FrameBuffer::setDrawBuffers( const std::vector<std::size_t> & buffersId )
    {
        std::vector<GLenum> drawBuffers;
        drawBuffers.reserve( buffersId.size() );
        for ( const std::size_t id : buffersId )
        {
            drawBuffers.emplace_back( static_cast<GLenum>( GL_COLOR_ATTACHMENT0 + id ) );
        }

        glBindFramebuffer( GL_FRAMEBUFFER, m_handle );
        glDrawBuffers( static_cast<GLsizei>( drawBuffers.size() ), drawBuffers.data() );
        glBindFramebuffer( GL_FRAMEBUFFER, 0 );

        GLenum fboStatus = glCheckFramebufferStatus( GL_FRAMEBUFFER );
        if ( fboStatus != GL_FRAMEBUFFER_COMPLETE )
            UDK_WARNING( "Framebuffer not complete: {}", fboStatus );

        GLenum glstatus = glGetError();
        if ( glstatus != GL_NO_ERROR )
            UDK_WARNING( "Error in GL call: {}", glstatus );
    }

    void FrameBuffer::resize( glm::ivec2 newSize )
    {
        if ( newSize != m_viewportSize )
        {
            m_viewportSize = std::move( newSize );
            for ( Attachment & attachment : m_attachments )
                attachment.resize( m_viewportSize );

            if ( m_depthBuffer )
                m_depthBuffer->resize( m_viewportSize );
        }
    }

    void FrameBuffer::bind() const { glBindFramebuffer( GL_FRAMEBUFFER, m_handle ); }
    void FrameBuffer::unbind() const { glBindFramebuffer( GL_FRAMEBUFFER, 0 ); }

    GLint toGl( Format format )
    {
        switch ( format )
        {
        case Format::DepthComponent32F: return GL_DEPTH_COMPONENT32F;
        case Format::R16F: return GL_R16F;
        case Format::RGBA16F: return GL_RGBA16F;
        case Format::RGBA32UI: return GL_RGBA32UI;
        }
        return GL_RGBA16F;
    }

    GLint toGl( DataType dataType )
    {
        switch ( dataType )
        {
        case DataType::UnsignedInt: return GL_UNSIGNED_INT;
        case DataType::Int: return GL_INT;
        case DataType::Float: return GL_FLOAT;
        }

        return GL_FLOAT;
    }

    GLint internalFormatToGlFormat( Format format )
    {
        switch ( format )
        {
        case Format::DepthComponent32F: return GL_DEPTH_COMPONENT;
        case Format::R16F: return GL_RED;
        case Format::RGBA16F: return GL_RGBA;
        case Format::RGBA32UI: return GL_RGBA_INTEGER;
        }

        return GL_RGBA;
    }

    GLint toGl( Filter filter )
    {
        switch ( filter )
        {
        case Filter::Nearest: return GL_NEAREST;
        case Filter::Linear: return GL_LINEAR;
        }
        return GL_NEAREST;
    }

    GLint toGl( WrappingMode filter )
    {
        switch ( filter )
        {
        case WrappingMode::ClampToEdge: return GL_CLAMP_TO_EDGE;
        case WrappingMode::ClampToBorder: return GL_CLAMP_TO_BORDER;
        case WrappingMode::MirroredRepeat: return GL_MIRRORED_REPEAT;
        case WrappingMode::Repeat: return GL_REPEAT;
        }

        return GL_CLAMP_TO_EDGE;
    }

    uint8_t getChannelNb( Format format )
    {
        switch ( format )
        {
        case Format::DepthComponent32F: return 1;
        case Format::R16F: return 1;
        case Format::RGBA16F: return 4;
        case Format::RGBA32UI: return 4;
        }
        return 4;
    }

    Attachment::Attachment( const glm::ivec2 & size,
                            Format             format,
                            DataType           dataType,
                            Filter             min,
                            Filter             mag,
                            WrappingMode       wrapS,
                            WrappingMode       wrapT,
                            std::size_t        levelOfDetails,
                            bool               zeroInit ) :
        m_format( format ),
        m_dataType( dataType ), m_minFilter( min ), m_magFilter( mag ), m_wrapS( wrapS ), m_wrapT( wrapT ),
        m_levelOfDetails( levelOfDetails )
    {
        glGenTextures( 1, &m_handle );
        glBindTexture( GL_TEXTURE_2D, m_handle );

        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, toGl( min ) );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, toGl( mag ) );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, toGl( wrapS ) );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, toGl( wrapT ) );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0 );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0 );

        if ( zeroInit )
        {
            std::vector<GLint> data = std::vector<GLint>( getChannelNb( m_format ) * size.x * size.y, 0 );
            glTexImage2D( GL_TEXTURE_2D,
                          static_cast<int>( m_levelOfDetails ),
                          toGl( m_format ),
                          size.x,
                          size.y,
                          0,
                          internalFormatToGlFormat( m_format ),
                          toGl( m_dataType ),
                          data.data() );
        }
        else
        {
            glTexImage2D( GL_TEXTURE_2D,
                          static_cast<int>( m_levelOfDetails ),
                          toGl( m_format ),
                          size.x,
                          size.y,
                          0,
                          internalFormatToGlFormat( m_format ),
                          toGl( m_dataType ),
                          nullptr );
        }

        glBindTexture( GL_TEXTURE_2D, 0 );
    }

    Attachment::Attachment( Attachment && other ) noexcept :
        m_handle( std::exchange( other.m_handle, GL_INVALID_VALUE ) ), m_format( std::move( other.m_format ) ),
        m_dataType( std::move( other.m_dataType ) ), m_minFilter( std::move( other.m_minFilter ) ),
        m_magFilter( std::move( other.m_magFilter ) ), m_wrapS( std::move( other.m_wrapS ) ),
        m_wrapT( std::move( other.m_wrapT ) ), m_levelOfDetails( std::move( other.m_levelOfDetails ) )
    {
    }

    Attachment & Attachment::operator=( Attachment && other ) noexcept
    {
        std::swap( m_handle, other.m_handle );
        std::swap( m_format, other.m_format );
        std::swap( m_dataType, other.m_dataType );
        std::swap( m_minFilter, other.m_minFilter );
        std::swap( m_magFilter, other.m_magFilter );
        std::swap( m_wrapS, other.m_wrapS );
        std::swap( m_wrapT, other.m_wrapT );
        std::swap( m_levelOfDetails, other.m_levelOfDetails );
        return *this;
    }

    Attachment::~Attachment()
    {
        if ( glIsTexture( m_handle ) )
        {
            glDeleteTextures( 1, &m_handle );
            m_handle = GL_INVALID_VALUE;
        }
    }

    void Attachment::resize( const glm::ivec2 & size )
    {
        glActiveTexture( GL_TEXTURE0 );
        glBindTexture( GL_TEXTURE_2D, m_handle );
        glTexImage2D( GL_TEXTURE_2D,
                      static_cast<int>( m_levelOfDetails ),
                      toGl( m_format ),
                      size.x,
                      size.y,
                      0,
                      internalFormatToGlFormat( m_format ),
                      toGl( m_dataType ),
                      nullptr );
        glBindTexture( GL_TEXTURE_2D, 0 );
        glDisable( GL_TEXTURE_2D );
    }

    void Attachment::bindAsTexture( const std::string & name, const Program & program, std::size_t bindingIndex ) const
    {
        const GLint index = static_cast<GLint>( bindingIndex );

        glActiveTexture( GL_TEXTURE0 + index );
        glBindTexture( GL_TEXTURE_2D, m_handle );

        const GLint location = glGetUniformLocation( program.getId(), name.c_str() );
        glUniform1i( location, index );
    }

} // namespace udk
