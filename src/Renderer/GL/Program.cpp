#include "Udock/Renderer/Program.hpp"
#include "Udock/Utils/Filesystem.hpp"
#include "Udock/Utils/Logger.hpp"
#include <fmt/printf.h>

namespace udk
{
    static std::string getShaderErrors( const GLuint p_shader )
    {
        GLint length;
        glGetShaderiv( p_shader, GL_INFO_LOG_LENGTH, &length );
        if ( length <= 0 )
        {
            return "";
        }
        std::vector<GLchar> log( length );
        glGetShaderInfoLog( p_shader, length, &length, &log[ 0 ] );
        return std::string( log.begin(), log.end() );
    }

    Program::Program( std::string name, std::vector<Path> shaderPaths ) :
        m_name( std::move( name ) ), m_shaderPaths( std::move( shaderPaths ) )
    {
        create( m_name );
        link();
    }

    Program::~Program()
    {
        if ( m_id != GL_INVALID_VALUE )
        {
            detachShaders();
            for ( GLuint id : m_shaders )
                glDeleteShader( id );
            glDeleteProgram( m_id );
        }
    }

    Program::Program( Program && other ) :
        m_id( std::exchange( other.m_id, GL_INVALID_VALUE ) ), m_name( std::exchange( other.m_name, "" ) ),
        m_shaderPaths( std::exchange( other.m_shaderPaths, {} ) )
    {
    }

    Program & Program::operator=( Program && other )
    {
        std::swap( m_id, other.m_id );
        std::swap( m_name, other.m_name );
        std::swap( m_shaderPaths, other.m_shaderPaths );
        return *this;
    }

    void Program::create( const std::string & p_name )
    {
        if ( m_id == GL_INVALID_VALUE )
        {
            m_name = p_name;
            m_id   = glCreateProgram();

            for ( udk::Path & path : m_shaderPaths )
            {
                const std::string name = path.string();
                const std::string ext  = path.extension().string();

                GLuint shaderType = GL_INVALID_INDEX;
                if ( ext == ".vert" )
                    shaderType = GL_VERTEX_SHADER;
                else if ( ext == ".geom" )
                    shaderType = GL_GEOMETRY_SHADER;
                else if ( ext == ".frag" )
                    shaderType = GL_FRAGMENT_SHADER;
                else if ( ext == ".comp" )
                    shaderType = GL_COMPUTE_SHADER;
                else if ( ext == ".tesc" )
                    shaderType = GL_TESS_CONTROL_SHADER;
                else if ( ext == ".tese" )
                    shaderType = GL_TESS_EVALUATION_SHADER;

                GLuint      shaderId = glCreateShader( shaderType );
                std::string src      = udk::readPath( path );
                if ( src.empty() )
                {
                    glDeleteShader( shaderId );
                    throw std::runtime_error( fmt::format( "No shader code in {}.", src ) );
                }
                const GLchar * shaderCode = src.c_str();
                glShaderSource( shaderId, 1, &shaderCode, 0 );
                glCompileShader( shaderId );
                GLint compiled;
                glGetShaderiv( shaderId, GL_COMPILE_STATUS, &compiled );
                if ( compiled == GL_FALSE )
                {
                    std::string error = "Error compiling shader: ";
                    error += name;
                    error += "\n";
                    error += getShaderErrors( shaderId );
                    glDeleteShader( shaderId );

                    throw std::runtime_error( error );
                }

                attachShader( shaderId );
                m_shaders.emplace_back( shaderId );
            }
        }
        else
        {
            fmt::print( "Program already created" );
        }
    }

    void Program::attachShader( const GLuint p_shaderId ) const
    {
        if ( m_id == GL_INVALID_INDEX )
        {
            fmt::print( "Cannot attach shader: program is not created" );
            return;
        }

        glAttachShader( m_id, p_shaderId );
    }

    void Program::link()
    {
        if ( m_id == GL_INVALID_INDEX )
        {
            fmt::print( "Can not link program: program is not created" );
            return;
        }

        GLint linked;
        glLinkProgram( m_id );
        glGetProgramiv( m_id, GL_LINK_STATUS, &linked );
        if ( linked == GL_FALSE )
        {
            std::string error = "Error linking program: ";
            error += m_name;
            error += "\n";
            error += getShaderErrors( m_id );
            glDeleteProgram( m_id );
            UDK_ERROR( error );
            return;
        }
    }

    void Program::detachShaders()
    {
        GLint nbShaders = 0;
        glGetProgramiv( m_id, GL_ATTACHED_SHADERS, &nbShaders );
        std::vector<GLuint> shaders( nbShaders );
        glGetAttachedShaders( m_id, nbShaders, nullptr, shaders.data() );
        for ( GLuint shader : shaders )
        {
            glDetachShader( m_id, shader );
        }
    }
} // namespace udk
