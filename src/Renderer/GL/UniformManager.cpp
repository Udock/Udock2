#include "Udock/Renderer/UniformManager.hpp"

namespace udk
{
    UniformManager::UniformManager( std::string name, std::size_t bufferInitializationSize ) :
        m_name( std::move( name ) ), m_bufferCurrentMaxSize( bufferInitializationSize ),
        m_uniformBuffer( m_bufferCurrentMaxSize, BufferAuthorization::Write )
    {
    }

    void UniformManager::bind( const Program & program, std::size_t bindingPoint ) const
    {
        const GLuint index = glGetUniformBlockIndex( program.getId(), m_name.c_str() );
        if ( index != GL_INVALID_INDEX )
        {
            glUniformBlockBinding( program.getId(), index, static_cast<GLint>( bindingPoint ) );
            m_uniformBuffer.bind( bindingPoint, BufferType::Uniform );
        }
    }

} // namespace udk
