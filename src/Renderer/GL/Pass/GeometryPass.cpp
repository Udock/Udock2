#include "Udock/Renderer/Pass/GeometryPass.hpp"
#include "Udock/Settings.hpp"
#include "Udock/Utils/Logger.hpp"

namespace udk
{
    GeometryPass::GeometryPass( const glm::ivec2 & size ) : m_frameBuffer( size )
    {
        // Create G-buffers for deferred shading.
        // viewPositionsNormalsCompressedTexture
        m_frameBuffer.addAttachment(
            { size, Format::RGBA32UI, DataType::UnsignedInt, Filter::Nearest, Filter::Nearest } );
        // colorsTexture
        m_frameBuffer.addAttachment( { size, Format::RGBA16F, DataType::Float, Filter::Nearest, Filter::Nearest } );
        // depthTexture
        m_frameBuffer.setDepthBuffer(
            { size, Format::DepthComponent32F, DataType::Float, Filter::Nearest, Filter::Nearest } );

        m_frameBuffer.setDrawBuffers( { 0, 1 } );
    }

    GeometryPass::GeometryPass( GeometryPass && other ) noexcept : m_frameBuffer( std::move( other.m_frameBuffer ) )
    {
        std::swap( m_geometrySubpasses, other.m_geometrySubpasses );
    }

    GeometryPass & GeometryPass::operator=( GeometryPass && other ) noexcept
    {
        std::swap( m_frameBuffer, other.m_frameBuffer );
        std::swap( m_geometrySubpasses, other.m_geometrySubpasses );
        return *this;
    }

    GeometryPass::~GeometryPass() = default;

    const Attachment & GeometryPass::getViewPosition() const { return m_frameBuffer.getAttachment( 0 ); }
    const Attachment & GeometryPass::getColors() const { return m_frameBuffer.getAttachment( 1 ); }
    const Attachment & GeometryPass::getDepth() const { return *( m_frameBuffer.getDepthBuffer() ); }

    void GeometryPass::resize( const glm::ivec2 & newSize ) { m_frameBuffer.resize( newSize ); }

    void GeometryPass::addSubpass( GeometrySubpass && pass ) { m_geometrySubpasses.emplace_back( std::move( pass ) ); }
    void GeometryPass::clear() { m_geometrySubpasses.clear(); }

    void GeometryPass::render( const UniformManager & rendererLevelUniforms )
    {
        m_frameBuffer.bind();
        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

        for ( const GeometrySubpass & pass : m_geometrySubpasses )
        {
            pass.program.use();
            rendererLevelUniforms.bind( pass.program, UdockSettings::ShaderUniformsRendererIndexDefault );
            pass.renderCallback( pass.program );
        }
        m_frameBuffer.unbind();
    }

} // namespace udk
