#include "Udock/Renderer/Pass/Skybox.hpp"
#include "Udock/Utils/Logger.hpp"

namespace udk
{
    Skybox::Skybox()
    {
        glGenVertexArrays( 1, &m_vao );
        glBindVertexArray( m_vao );
        glGenBuffers( 1, &m_vbo );

        glBindBuffer( GL_ARRAY_BUFFER, m_vbo );
        glBufferData( GL_ARRAY_BUFFER, vertices.size() * sizeof( float ), vertices.data(), GL_STATIC_DRAW );

        glEnableVertexAttribArray( 0 );
        glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof( float ), nullptr );

        glGenTextures( 1, &m_texture );
        glBindTexture( GL_TEXTURE_CUBE_MAP, m_texture );
    }

    Skybox::Skybox( Skybox && other )
    {
        std::swap( m_vao, other.m_vao );
        std::swap( m_vbo, other.m_vbo );
        std::swap( m_texture, other.m_texture );
    }

    Skybox & Skybox::operator=( Skybox && other )
    {
        std::swap( m_vao, other.m_vao );
        std::swap( m_vbo, other.m_vbo );
        std::swap( m_texture, other.m_texture );
        return *this;
    }

    Skybox::~Skybox()
    {
        if ( glIsVertexArray( m_vao ) )
        {
            glDeleteVertexArrays( 1, &m_vao );
            m_vao = GL_INVALID_VALUE;
        }
        if ( glIsBuffer( m_vbo ) )
        {
            glDeleteBuffers( 1, &m_vbo );
            m_vbo = GL_INVALID_VALUE;
        }
        if ( glIsTexture( m_texture ) )
        {
            glDeleteTextures( 1, &m_texture );
            m_texture = GL_INVALID_VALUE;
        }
    }

    void Skybox::bind( const std::string & name, const Program & program, std::size_t bindingIndex )
    {
        const GLint index = static_cast<GLint>( bindingIndex );

        glActiveTexture( GL_TEXTURE0 + index );
        glBindTexture( GL_TEXTURE_CUBE_MAP, m_texture );

        const GLint location = glGetUniformLocation( program.getId(), name.c_str() );
        glUniform1i( location, index );
    }

    void Skybox::render()
    {
        glDepthFunc( GL_LEQUAL );
        glBindVertexArray( m_vao );

        glActiveTexture( GL_TEXTURE0 );
        glBindTexture( GL_TEXTURE_CUBE_MAP, m_texture );
        glDrawArrays( GL_TRIANGLES, 0, 36 );
        glBindTexture( GL_TEXTURE_CUBE_MAP, 0 );

        glBindVertexArray( 0 );
        glDepthFunc( GL_LESS );
    }
} // namespace udk
