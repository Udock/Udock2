#include "Udock/Renderer/Pass/BakedSkybox.hpp"
#include "Udock/Utils/Logger.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

namespace udk
{
    BakedSkybox::BakedSkybox( const Path & skyboxPath ) : Skybox()
    {
        load( { skyboxPath.string() + "/right.png",
                skyboxPath.string() + "/left.png",
                skyboxPath.string() + "/top.png",
                skyboxPath.string() + "/bottom.png",
                skyboxPath.string() + "/front.png",
                skyboxPath.string() + "/back.png" } );
    }

    BakedSkybox::BakedSkybox( BakedSkybox && other ) : Skybox( std::move( other ) ) {}

    BakedSkybox & BakedSkybox::operator=( BakedSkybox && other )
    {
        Skybox::operator=( std::move( other ) );

        return *this;
    }

    void BakedSkybox::load( const std::vector<Path> & files )
    {
        glBindTexture( GL_TEXTURE_CUBE_MAP, m_texture );

        int width, height, nrChannels;
        for ( unsigned int i = 0; i < files.size(); i++ )
        {
            unsigned char * data = stbi_load( files[ i ].string().c_str(), &width, &height, &nrChannels, 0 );
            if ( data )
            {
                glTexImage2D(
                    GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data );
                stbi_image_free( data );
            }
            else
            {
                UDK_ERROR( "Failed to load texture: {}", files[ i ].string() );
                stbi_image_free( data );
            }
        }

        glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
        glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
        glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE );
        glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_BASE_LEVEL, 0 );
        glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAX_LEVEL, 0 );

        glBindTexture( GL_TEXTURE_CUBE_MAP, 0 );
    }
} // namespace udk
