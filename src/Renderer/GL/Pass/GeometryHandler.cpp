#include "Udock/Renderer/Pass/GeometryHandler.hpp"
#include "Udock/Settings.hpp"

namespace udk
{
    MeshHandler::MeshHandler( const Mesh & surface ) :
        m_surface( surface ), m_uniforms( "ModelUniforms", sizeof( glm::mat4 ) )
    {
        const GLsizeiptr vertexNb = static_cast<GLsizeiptr>( m_surface.vertices.size() );

        glGenBuffers( 1, &m_ibo );
        glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_ibo );
        glBufferData( GL_ELEMENT_ARRAY_BUFFER,
                      m_surface.indices.size() * sizeof( uint32_t ),
                      m_surface.indices.data(),
                      GL_STATIC_DRAW );

        glGenBuffers( 1, &m_verticesVbo );
        glBindBuffer( GL_ARRAY_BUFFER, m_verticesVbo );
        glBufferData( GL_ARRAY_BUFFER, vertexNb * sizeof( glm::vec3 ), m_surface.vertices.data(), GL_STATIC_DRAW );
        glBindBuffer( GL_ARRAY_BUFFER, 0 );

        glGenBuffers( 1, &m_normalsVbo );
        glBindBuffer( GL_ARRAY_BUFFER, m_normalsVbo );
        glBufferData( GL_ARRAY_BUFFER, vertexNb * sizeof( glm::vec3 ), m_surface.normals.data(), GL_STATIC_DRAW );
        glBindBuffer( GL_ARRAY_BUFFER, 0 );

        glGenBuffers( 1, &m_colorsVbo );
        glBindBuffer( GL_ARRAY_BUFFER, m_colorsVbo );
        glBufferData( GL_ARRAY_BUFFER, vertexNb * sizeof( glm::vec3 ), m_surface.colors.data(), GL_STATIC_DRAW );
        glBindBuffer( GL_ARRAY_BUFFER, 0 );

        glGenVertexArrays( 1, &m_meshVao );
        glBindVertexArray( m_meshVao );
        glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_ibo );

        glBindBuffer( GL_ARRAY_BUFFER, m_verticesVbo );
        glEnableVertexAttribArray( VertexId );
        glVertexAttribPointer( VertexId, 3, GL_FLOAT, GL_FALSE, sizeof( glm::vec3 ), 0 );

        glBindBuffer( GL_ARRAY_BUFFER, m_normalsVbo );
        glEnableVertexAttribArray( NormalId );
        glVertexAttribPointer( NormalId, 3, GL_FLOAT, GL_FALSE, sizeof( glm::vec3 ), 0 );

        glBindBuffer( GL_ARRAY_BUFFER, m_colorsVbo );
        glEnableVertexAttribArray( ColorId );
        glVertexAttribPointer( ColorId, 3, GL_FLOAT, GL_FALSE, sizeof( glm::vec3 ), 0 );

        glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
        glBindVertexArray( 0 );

        m_uniforms.addValue<glm::mat4>( "uModel" );
    }

    MeshHandler::MeshHandler( MeshHandler && other ) noexcept : m_surface( other.m_surface )
    {
        std::swap( m_meshVao, other.m_meshVao );
        std::swap( m_verticesVbo, other.m_verticesVbo );
        std::swap( m_normalsVbo, other.m_normalsVbo );
        std::swap( m_colorsVbo, other.m_colorsVbo );
        std::swap( m_ibo, other.m_ibo );
        std::swap( m_uniforms, other.m_uniforms );
    }

    MeshHandler & MeshHandler::operator=( MeshHandler && other ) noexcept
    {
        std::swap( m_surface, other.m_surface );
        std::swap( m_meshVao, other.m_meshVao );
        std::swap( m_verticesVbo, other.m_verticesVbo );
        std::swap( m_normalsVbo, other.m_normalsVbo );
        std::swap( m_colorsVbo, other.m_colorsVbo );
        std::swap( m_ibo, other.m_ibo );
        std::swap( m_uniforms, other.m_uniforms );

        return *this;
    }

    MeshHandler::~MeshHandler()
    {
        constexpr auto deleteBuffer = []( GLuint buffer )
        {
            if ( glIsBuffer( buffer ) )
                glDeleteBuffers( 1, &buffer );
        };

        if ( m_meshVao != GL_INVALID_VALUE )
            glDeleteVertexArrays( 1, &m_meshVao );
        deleteBuffer( m_verticesVbo );
        deleteBuffer( m_normalsVbo );
        deleteBuffer( m_colorsVbo );
        deleteBuffer( m_ibo );
    }

    void MeshHandler::render( const Program & program, const glm::mat4 & transform )
    {
        m_uniforms.updateValue( "uModel", transform );
        m_uniforms.bind( program, UdockSettings::ShaderUniformsModelIndexDefault );

        const GLsizei indexNb = static_cast<GLsizei>( m_surface.indices.size() );
        glBindVertexArray( m_meshVao );
        glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_ibo );
        glDrawElements( GL_TRIANGLES, indexNb, GL_UNSIGNED_INT, 0 );
        glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
        glBindVertexArray( 0 );
    }

    MolecularSurfaceHandler::MolecularSurfaceHandler( const Mesh &                     surface,
                                                      const MolecularSurfaceSettings & settings ) :
        m_surface( surface ),
        m_uniforms( "ModelUniforms", 64 * sizeof( float ) )
    {
        const GLsizeiptr vertexNb = static_cast<GLsizeiptr>( m_surface.vertices.size() );

        glGenBuffers( 1, &m_ibo );
        glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_ibo );
        glBufferData( GL_ELEMENT_ARRAY_BUFFER,
                      m_surface.indices.size() * sizeof( uint32_t ),
                      m_surface.indices.data(),
                      GL_STATIC_DRAW );
        glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );

        glGenBuffers( 1, &m_verticesVbo );
        glBindBuffer( GL_ARRAY_BUFFER, m_verticesVbo );
        glBufferData( GL_ARRAY_BUFFER, vertexNb * sizeof( glm::vec3 ), m_surface.vertices.data(), GL_STATIC_DRAW );
        glBindBuffer( GL_ARRAY_BUFFER, 0 );

        glGenBuffers( 1, &m_normalsVbo );
        glBindBuffer( GL_ARRAY_BUFFER, m_normalsVbo );
        glBufferData( GL_ARRAY_BUFFER, vertexNb * sizeof( glm::vec3 ), m_surface.normals.data(), GL_STATIC_DRAW );
        glBindBuffer( GL_ARRAY_BUFFER, 0 );

        glGenBuffers( 1, &m_chargesVbo );
        glBindBuffer( GL_ARRAY_BUFFER, m_chargesVbo );
        glBufferData( GL_ARRAY_BUFFER, vertexNb * sizeof( glm::vec3 ), m_surface.colors.data(), GL_STATIC_DRAW );
        glBindBuffer( GL_ARRAY_BUFFER, 0 );

        glGenVertexArrays( 1, &m_meshVao );
        glBindVertexArray( m_meshVao );
        glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_ibo );

        glBindBuffer( GL_ARRAY_BUFFER, m_verticesVbo );
        glEnableVertexAttribArray( VertexId );
        glVertexAttribPointer( VertexId, 3, GL_FLOAT, GL_FALSE, sizeof( glm::vec3 ), 0 );

        glBindBuffer( GL_ARRAY_BUFFER, m_normalsVbo );
        glEnableVertexAttribArray( NormalId );
        glVertexAttribPointer( NormalId, 3, GL_FLOAT, GL_FALSE, sizeof( glm::vec3 ), 0 );

        glBindBuffer( GL_ARRAY_BUFFER, m_chargesVbo );
        glEnableVertexAttribArray( ColorId );
        glVertexAttribPointer( ColorId, 3, GL_FLOAT, GL_FALSE, sizeof( glm::vec3 ), 0 );

        glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
        glBindVertexArray( 0 );

        m_uniforms.addValue<glm::mat4>( "uModel" );
        m_uniforms.addValue( "uPositiveColor", settings.positiveCharge );
        m_uniforms.addValue( "uNeutralColor", settings.neutralCharge );
        m_uniforms.addValue( "uNegativeColor", settings.negativeCharge );
        m_uniforms.addValue( "uContrast", settings.contrast );
    }

    MolecularSurfaceHandler::MolecularSurfaceHandler( MolecularSurfaceHandler && other ) noexcept :
        m_surface( other.m_surface )
    {
        std::swap( m_meshVao, other.m_meshVao );
        std::swap( m_verticesVbo, other.m_verticesVbo );
        std::swap( m_normalsVbo, other.m_normalsVbo );
        std::swap( m_chargesVbo, other.m_chargesVbo );
        std::swap( m_ibo, other.m_ibo );
        std::swap( m_uniforms, other.m_uniforms );
    }

    MolecularSurfaceHandler & MolecularSurfaceHandler::operator=( MolecularSurfaceHandler && other ) noexcept
    {
        std::swap( m_surface, other.m_surface );
        std::swap( m_meshVao, other.m_meshVao );
        std::swap( m_verticesVbo, other.m_verticesVbo );
        std::swap( m_normalsVbo, other.m_normalsVbo );
        std::swap( m_chargesVbo, other.m_chargesVbo );
        std::swap( m_ibo, other.m_ibo );
        std::swap( m_uniforms, other.m_uniforms );

        return *this;
    }

    MolecularSurfaceHandler::~MolecularSurfaceHandler()
    {
        constexpr auto deleteBuffer = []( GLuint buffer )
        {
            if ( glIsBuffer( buffer ) )
                glDeleteBuffers( 1, &buffer );
        };

        if ( m_meshVao != GL_INVALID_VALUE )
            glDeleteVertexArrays( 1, &m_meshVao );
        deleteBuffer( m_verticesVbo );
        deleteBuffer( m_normalsVbo );
        deleteBuffer( m_chargesVbo );
        deleteBuffer( m_ibo );
    }

    void MolecularSurfaceHandler::render( const Program &                  program,
                                          const glm::mat4 &                transform,
                                          const MolecularSurfaceSettings & settings )
    {
        m_uniforms.updateValue( "uModel", transform );
        m_uniforms.updateValue( "uPositiveColor", settings.positiveCharge );
        m_uniforms.updateValue( "uNeutralColor", settings.neutralCharge );
        m_uniforms.updateValue( "uNegativeColor", settings.negativeCharge );
        m_uniforms.updateValue( "uContrast", settings.contrast );

        m_uniforms.bind( program, UdockSettings::ShaderUniformsModelIndexDefault );

        const GLsizei indexNb = static_cast<GLsizei>( m_surface.indices.size() );
        glBindVertexArray( m_meshVao );
        glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_ibo );
        glDrawElements( GL_TRIANGLES, indexNb, GL_UNSIGNED_INT, 0 );
        glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
        glBindVertexArray( 0 );
    }

    PointHandler::PointHandler( std::size_t allocationSize ) : m_currentSize( allocationSize )
    {
        glGenBuffers( 1, &m_spheresVbo );
        glBindBuffer( GL_ARRAY_BUFFER, m_spheresVbo );
        glBufferData( GL_ARRAY_BUFFER, allocationSize * sizeof( glm::vec4 ), nullptr, GL_DYNAMIC_DRAW );
        glBindBuffer( GL_ARRAY_BUFFER, 0 );

        glGenBuffers( 1, &m_colorsVbo );
        glBindBuffer( GL_ARRAY_BUFFER, m_colorsVbo );
        glBufferData( GL_ARRAY_BUFFER, allocationSize * sizeof( glm::vec4 ), nullptr, GL_DYNAMIC_DRAW );
        glBindBuffer( GL_ARRAY_BUFFER, 0 );

        glGenVertexArrays( 1, &m_pointsVao );
        glBindVertexArray( m_pointsVao );

        glBindBuffer( GL_ARRAY_BUFFER, m_spheresVbo );
        glEnableVertexAttribArray( SphereId );
        glVertexAttribPointer( SphereId, 4, GL_FLOAT, GL_FALSE, sizeof( glm::vec4 ), 0 );

        glBindBuffer( GL_ARRAY_BUFFER, m_colorsVbo );
        glEnableVertexAttribArray( ColorId );
        glVertexAttribPointer( ColorId, 4, GL_FLOAT, GL_FALSE, sizeof( glm::vec4 ), 0 );

        glBindVertexArray( 0 );
    }

    PointHandler::PointHandler( PointHandler && other ) noexcept
    {
        std::swap( m_currentSize, other.m_currentSize );
        std::swap( m_pointsVao, other.m_pointsVao );
        std::swap( m_spheresVbo, other.m_spheresVbo );
        std::swap( m_colorsVbo, other.m_colorsVbo );
    }

    PointHandler & PointHandler::operator=( PointHandler && other ) noexcept
    {
        std::swap( m_currentSize, other.m_currentSize );
        std::swap( m_pointsVao, other.m_pointsVao );
        std::swap( m_spheresVbo, other.m_spheresVbo );
        std::swap( m_colorsVbo, other.m_colorsVbo );

        return *this;
    }

    PointHandler::~PointHandler()
    {
        if ( m_pointsVao != GL_INVALID_VALUE )
        {
            glDeleteVertexArrays( 1, &m_pointsVao );
            glDeleteBuffers( 1, &m_spheresVbo );
            glDeleteBuffers( 1, &m_colorsVbo );
        }
    }

    void PointHandler::render( Span<glm::vec4> points, Span<glm::vec4> colors )
    {
        if ( m_currentSize < points.size() )
        {
            const std::size_t newSize = points.size();

            GLuint newSphereHandle;
            glGenBuffers( 1, &newSphereHandle );
            glBindBuffer( GL_ARRAY_BUFFER, newSphereHandle );
            glBufferData( GL_ARRAY_BUFFER, newSize * sizeof( glm::vec4 ), nullptr, GL_DYNAMIC_DRAW );
            glBindBuffer( GL_ARRAY_BUFFER, 0 );

            GLuint newColorHandle;
            glGenBuffers( 1, &newColorHandle );
            glBindBuffer( GL_ARRAY_BUFFER, newColorHandle );
            glBufferData( GL_ARRAY_BUFFER, newSize * sizeof( glm::vec4 ), nullptr, GL_DYNAMIC_DRAW );
            glBindBuffer( GL_ARRAY_BUFFER, 0 );

            m_currentSize = newSize;

            glDeleteBuffers( 1, &m_spheresVbo );
            glDeleteBuffers( 1, &m_colorsVbo );
            m_spheresVbo = newSphereHandle;
            m_colorsVbo  = newColorHandle;
        }

        glBindBuffer( GL_ARRAY_BUFFER, m_spheresVbo );
        glBufferSubData( GL_ARRAY_BUFFER, 0, points.size() * sizeof( glm::vec4 ), points.data() );
        glBindBuffer( GL_ARRAY_BUFFER, 0 );

        glBindBuffer( GL_ARRAY_BUFFER, m_colorsVbo );
        glBufferSubData( GL_ARRAY_BUFFER, 0, colors.size() * sizeof( glm::vec4 ), colors.data() );
        glBindBuffer( GL_ARRAY_BUFFER, 0 );

        glBindVertexArray( m_pointsVao );
        glDrawArrays( GL_POINTS, 0, static_cast<GLsizei>( points.size() ) );
        glBindVertexArray( 0 );
    }

    LineHandler::LineHandler( glm::vec4 lineColor, std::size_t allocationSize ) :
        m_currentSize( allocationSize ), m_uniforms( "LinesUniforms", 64 * sizeof( float ) )
    {
        glGenBuffers( 1, &m_pointsVbo );
        glBindBuffer( GL_ARRAY_BUFFER, m_pointsVbo );
        glBufferData( GL_ARRAY_BUFFER, allocationSize * 2 * sizeof( glm::vec4 ), nullptr, GL_DYNAMIC_DRAW );
        glBindBuffer( GL_ARRAY_BUFFER, 0 );

        glGenVertexArrays( 1, &m_linesVao );
        glBindVertexArray( m_linesVao );
        glBindBuffer( GL_ARRAY_BUFFER, m_pointsVbo );
        glEnableVertexAttribArray( PointId );
        glVertexAttribPointer( PointId, 4, GL_FLOAT, GL_FALSE, sizeof( glm::vec4 ), 0 );
        glBindBuffer( GL_ARRAY_BUFFER, 0 );
        glBindVertexArray( 0 );

        m_uniforms.addValue( "color", lineColor );
    }

    LineHandler::LineHandler( LineHandler && other ) noexcept
    {
        std::swap( m_uniforms, other.m_uniforms );
        std::swap( m_linesVao, other.m_linesVao );
        std::swap( m_pointsVbo, other.m_pointsVbo );
    }

    LineHandler & LineHandler::operator=( LineHandler && other ) noexcept
    {
        std::swap( m_uniforms, other.m_uniforms );
        std::swap( m_linesVao, other.m_linesVao );
        std::swap( m_pointsVbo, other.m_pointsVbo );

        return *this;
    }

    LineHandler::~LineHandler()
    {
        if ( m_linesVao != GL_INVALID_VALUE )
            glDeleteVertexArrays( 1, &m_linesVao );
    }

    void LineHandler::render( const Program & program, Span<glm::vec4> lines, glm::vec4 lineColor )
    {
        if ( lines.empty() )
            return;

        m_uniforms.updateValue( "color", lineColor );

        assert( lines.size() % 2 == 0 && "Lines must be composed of two points" );
        if ( m_currentSize < lines.size() )
        {
            const std::size_t newSize = lines.size();

            GLuint newPointHandle;
            glGenBuffers( 1, &newPointHandle );
            glBindBuffer( GL_ARRAY_BUFFER, newPointHandle );
            glBufferData( GL_ARRAY_BUFFER, newSize * sizeof( glm::vec4 ), nullptr, GL_DYNAMIC_DRAW );
            glBindBuffer( GL_ARRAY_BUFFER, 0 );

            m_currentSize = newSize;

            glDeleteBuffers( 1, &m_pointsVbo );
            m_pointsVbo = newPointHandle;
        }

        glBindBuffer( GL_ARRAY_BUFFER, m_pointsVbo );
        glBufferSubData( GL_ARRAY_BUFFER, 0, lines.size() * sizeof( glm::vec4 ), lines.data() );
        glBindBuffer( GL_ARRAY_BUFFER, 0 );

        glBindVertexArray( m_linesVao );
        glLineWidth( 1.f );
        m_uniforms.bind( program, 1 );
        glDrawArrays( GL_LINES, 0, static_cast<GLsizei>( lines.size() ) );
        glBindVertexArray( 0 );
    }

} // namespace udk
