#include "Udock/Renderer/Pass/HosekSkybox.hpp"
#include "Udock/Renderer/Program.hpp"
#include "Udock/Renderer/UniformManager.hpp"
#include "Udock/Utils/Logger.hpp"

namespace udk
{
    HosekSkybox::HosekSkybox( float turbidity, float groundAlbedo, glm::vec3 solarDirection ) :
        Skybox(), m_solarDirection( std::move( solarDirection ) ), m_turbidity( turbidity ),
        m_groundAlbedo( groundAlbedo )
    {
        bake();
    }

    HosekSkybox::HosekSkybox( HosekSkybox && other ) : Skybox( std::move( other ) ) {}

    HosekSkybox & HosekSkybox::operator=( HosekSkybox && other )
    {
        Skybox::operator=( std::move( other ) );
        return *this;
    }

    void HosekSkybox::setSolarDirection( glm::vec3 solarDirection )
    {
        if ( solarDirection == m_solarDirection )
            return;

        m_solarDirection = std::move( solarDirection );
        bake();
    }
    void HosekSkybox::setTurbidity( float turbidity )
    {
        if ( std::abs( turbidity - m_turbidity ) < 1e-8f )
            return;

        m_turbidity = turbidity;
        bake();
    }
    void HosekSkybox::setGroundAlbedo( float albedo )
    {
        if ( std::abs( albedo - m_groundAlbedo ) < 1e-8f )
            return;

        m_groundAlbedo = albedo;
        bake();
    }

    void HosekSkybox::bake()
    {
        GLuint captureFBO, captureRBO;
        glGenFramebuffers( 1, &captureFBO );
        glGenRenderbuffers( 1, &captureRBO );

        glBindFramebuffer( GL_FRAMEBUFFER, captureFBO );
        glBindRenderbuffer( GL_RENDERBUFFER, captureRBO );
        glRenderbufferStorage( GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, 512, 512 );
        glFramebufferRenderbuffer( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, captureRBO );

        glBindTexture( GL_TEXTURE_CUBE_MAP, m_texture );
        glTexImage2D( GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGBA8, 512, 512, 0, GL_BGRA, GL_UNSIGNED_BYTE, nullptr );
        glTexImage2D( GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGBA8, 512, 512, 0, GL_BGRA, GL_UNSIGNED_BYTE, nullptr );
        glTexImage2D( GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGBA8, 512, 512, 0, GL_BGRA, GL_UNSIGNED_BYTE, nullptr );
        glTexImage2D( GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGBA8, 512, 512, 0, GL_BGRA, GL_UNSIGNED_BYTE, nullptr );
        glTexImage2D( GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGBA8, 512, 512, 0, GL_BGRA, GL_UNSIGNED_BYTE, nullptr );
        glTexImage2D( GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGBA8, 512, 512, 0, GL_BGRA, GL_UNSIGNED_BYTE, nullptr );

        glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
        glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
        glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE );
        glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_BASE_LEVEL, 0 );
        glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAX_LEVEL, 0 );

        const std::array<glm::mat4, 6> captureViews = {
            glm::lookAt( glm::vec3( 0.0f, 0.0f, 0.0f ), glm::vec3( 1.0f, 0.0f, 0.0f ), glm::vec3( 0.0f, -1.0f, 0.0f ) ),
            glm::lookAt(
                glm::vec3( 0.0f, 0.0f, 0.0f ), glm::vec3( -1.0f, 0.0f, 0.0f ), glm::vec3( 0.0f, -1.0f, 0.0f ) ),
            glm::lookAt( glm::vec3( 0.0f, 0.0f, 0.0f ), glm::vec3( 0.0f, 1.0f, 0.0f ), glm::vec3( 0.0f, 0.0f, 1.0f ) ),
            glm::lookAt(
                glm::vec3( 0.0f, 0.0f, 0.0f ), glm::vec3( 0.0f, -1.0f, 0.0f ), glm::vec3( 0.0f, 0.0f, -1.0f ) ),
            glm::lookAt( glm::vec3( 0.0f, 0.0f, 0.0f ), glm::vec3( 0.0f, 0.0f, 1.0f ), glm::vec3( 0.0f, -1.0f, 0.0f ) ),
            glm::lookAt( glm::vec3( 0.0f, 0.0f, 0.0f ), glm::vec3( 0.0f, 0.0f, -1.0f ), glm::vec3( 0.0f, -1.0f, 0.0f ) )
        };

        Program hosekProgram { "HosekProgram", { "shaders/FullScreenQuad.vert", "shaders/Hosek.frag" } };
        hosekProgram.use();

        const float solarElevation = std::acos( std::clamp( m_solarDirection.y, -1.f, 1.f ) );

        std::array<glm::vec4, 9>       configs {};
        const std::array<glm::vec3, 9> current = getConfiguration( m_turbidity, m_groundAlbedo, solarElevation );
        for ( std::size_t j = 0; j < 9; j++ )
        {
            configs[ j ] = glm::vec4( current[ j ], 1.f );
        }

        const glm::vec4 expectedSpectralRadiance
            = glm::vec4( getExpectedRadiance( m_turbidity, m_groundAlbedo, solarElevation ), 1.f );

        UniformManager uniforms { "HosekUniforms", sizeof( glm::mat4 ) * 5 };
        uniforms.addValue<glm::vec2>( "uViewportSize", glm::vec2 { 512 } );
        uniforms.addValue<glm::mat4>( "uView" );
        uniforms.addValue<glm::vec4>( "uSolarPoint", glm::vec4 { m_solarDirection, 1.f } );
        uniforms.addValue<glm::vec4>( "uConfigs", configs );
        uniforms.addValue<glm::vec4>( "uExpectedSpectralRadiance", expectedSpectralRadiance );
        uniforms.addValue<float>( "uTurbidity", m_turbidity );

        glViewport( 0, 0, 512, 512 );
        glBindFramebuffer( GL_FRAMEBUFFER, captureFBO );
        for ( unsigned int i = 0; i < 6; ++i )
        {
            uniforms.updateValue<glm::mat4>( "uView", captureViews[ i ] );

            uniforms.bind( hosekProgram, 0 );
            glFramebufferTexture2D(
                GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, m_texture, 0 );
            glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

            glBindVertexArray( m_vao );
            glDrawArrays( GL_TRIANGLES, 0, 36 );
        }
        glBindFramebuffer( GL_FRAMEBUFFER, 0 );
    }

    std::array<glm::vec3, 9> HosekSkybox::getConfiguration( float turbidity, float albedo, float solarElevation )
    {
        std::array<glm::vec3, 9> config {};
        for ( unsigned int i = 0; i < 9; ++i )
            config[ i ] = getSample( DatasetXYZ, 9, i, turbidity, albedo, solarElevation );

        return config;
    }

    glm::vec3 HosekSkybox::getExpectedRadiance( float turbidity, float albedo, float solarElevation )
    {
        return getSample( RadiancesXYZ, 1, 0, turbidity, albedo, solarElevation );
    }

    glm::vec3 HosekSkybox::quinticBezierInterpolation( const float       t,
                                                       const glm::vec3 & A,
                                                       const glm::vec3 & B,
                                                       const glm::vec3 & C,
                                                       const glm::vec3 & D,
                                                       const glm::vec3 & E,
                                                       const glm::vec3 & F )
    {
        //(1-t).^3* A1 + 3*(1-t).^2.*t * A2 + 3*(1-t) .* t .^ 2 * A3 + t.^3 * A4;
        return std::pow( 1.f - t, 5.f ) * A + 5.f * t * std::pow( 1.f - t, 4.f ) * B
               + 10.f * std::pow( 1.f - t, 3.f ) * t * t * C + 10.f * std::pow( 1.f - t, 2.f ) * t * t * t * D
               + 5.f * ( 1.f - t ) * std::pow( t, 4.f ) * E + std::pow( t, 5.f ) * F;
    }

    glm::vec3 HosekSkybox::getSample( Span<const glm::vec3> dataset,
                                      std::size_t           stride,
                                      std::size_t           shift,
                                      float                 turbidity,
                                      float                 albedo,
                                      float                 solarElevation )
    {
        const std::size_t turbidityIndex = static_cast<std::size_t>( std::ceil( turbidity ) );
        const float       turbidityRem   = turbidity - static_cast<float>( turbidityIndex );

        solarElevation = std::pow( solarElevation / ( glm::pi<float>() / 2.f ), ( 1.f / 3.f ) );

        // alb 0 low turb
        std::size_t baseId = stride * 6 * ( turbidityIndex - 1 ) + shift;
        glm::vec3   result = ( 1.f - albedo ) * ( 1.f - turbidityRem )
                           * quinticBezierInterpolation( solarElevation,
                                                         dataset[ baseId + stride * 0 ],
                                                         dataset[ baseId + stride * 1 ],
                                                         dataset[ baseId + stride * 2 ],
                                                         dataset[ baseId + stride * 3 ],
                                                         dataset[ baseId + stride * 4 ],
                                                         dataset[ baseId + stride * 5 ] );

        // alb 1 low turb
        baseId = stride * 6 * 10 + stride * 6 * ( turbidityIndex - 1 ) + shift;
        result += albedo * ( 1.f - turbidityRem )
                  * quinticBezierInterpolation( solarElevation,
                                                dataset[ baseId + stride * 0 ],
                                                dataset[ baseId + stride * 1 ],
                                                dataset[ baseId + stride * 2 ],
                                                dataset[ baseId + stride * 3 ],
                                                dataset[ baseId + stride * 4 ],
                                                dataset[ baseId + stride * 5 ] );

        if ( turbidityIndex == 10 )
            return result;

        // alb 0 high turb
        baseId = stride * 6 * turbidityIndex + shift;
        result += ( 1.f - albedo ) * turbidityRem
                  * quinticBezierInterpolation( solarElevation,
                                                dataset[ baseId + stride * 0 ],
                                                dataset[ baseId + stride * 1 ],
                                                dataset[ baseId + stride * 2 ],
                                                dataset[ baseId + stride * 3 ],
                                                dataset[ baseId + stride * 4 ],
                                                dataset[ baseId + stride * 5 ] );

        // alb 1 high turb
        baseId = stride * 6 * 10 + stride * 6 * turbidityIndex + shift;
        result += albedo * turbidityRem
                  * quinticBezierInterpolation( solarElevation,
                                                dataset[ baseId + stride * 0 ],
                                                dataset[ baseId + stride * 1 ],
                                                dataset[ baseId + stride * 2 ],
                                                dataset[ baseId + stride * 3 ],
                                                dataset[ baseId + stride * 4 ],
                                                dataset[ baseId + stride * 5 ] );

        return result;
    }
} // namespace udk
