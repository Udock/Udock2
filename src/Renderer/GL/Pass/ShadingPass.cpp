#include "Udock/Renderer/Pass/ShadingPass.hpp"
#include "Udock/Renderer/Pass/GeometryPass.hpp"
#include "Udock/Settings.hpp"

namespace udk
{
    ShadingPass::ShadingPass( const glm::ivec2 & size ) : m_frameBuffer( size )
    {
        m_frameBuffer.addAttachment( { size, Format::RGBA16F, DataType::Float } );

        GLfloat quadVertices[] = {
            -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -1.0f,
        };

        // Setup plane VAO.
        glGenBuffers( 1, &m_quadVBO );
        glBindBuffer( GL_ARRAY_BUFFER, m_quadVBO );
        glBufferData( GL_ARRAY_BUFFER, sizeof( quadVertices ), quadVertices, GL_STATIC_DRAW );
        glGenVertexArrays( 1, &m_quadVAO );
        glBindVertexArray( m_quadVAO );
        glBindBuffer( GL_ARRAY_BUFFER, m_quadVBO );
        glEnableVertexAttribArray( 0 );
        glVertexAttribPointer( 0, 2, GL_FLOAT, GL_FALSE, 0, nullptr );
        glBindBuffer( GL_ARRAY_BUFFER, 0 );
        glBindVertexArray( 0 );
    }

    ShadingPass::ShadingPass( ShadingPass && other ) : m_frameBuffer( std::move( other.m_frameBuffer ) )
    {
        std::swap( m_quadVBO, other.m_quadVBO );
    }

    ShadingPass & ShadingPass::operator=( ShadingPass && other )
    {
        std::swap( m_frameBuffer, other.m_frameBuffer );
        std::swap( m_quadVBO, other.m_quadVBO );

        return *this;
    }

    ShadingPass::~ShadingPass() {}

    void ShadingPass::addSubpass( PostProcessSubpass && subpass )
    {
        m_postProcessSubpasses.emplace_back( std::move( subpass ) );
    }

    void ShadingPass::clear() { m_postProcessSubpasses.clear(); }

    void ShadingPass::resize( const glm::ivec2 size ) { m_frameBuffer.resize( size ); }

    void ShadingPass::render( const UniformManager & rendererLevelUniforms, const GeometryPass * const geometryPass )
    {
        const GBuffer currentGBuffer { &geometryPass->getViewPosition(),
                                       &geometryPass->getColors(),
                                       &geometryPass->getDepth() };
        for ( PostProcessSubpass & pass : m_postProcessSubpasses )
        {
            pass.program.use();
            rendererLevelUniforms.bind( pass.program, UdockSettings::ShaderUniformsRendererIndexDefault );
            pass.renderCallback( pass.program, m_frameBuffer, currentGBuffer, pass.uniforms );
            glBindVertexArray( m_quadVAO );
            glDrawArrays( GL_TRIANGLE_STRIP, 0, 4 );
            glBindVertexArray( 0 );
        }
        glBindFramebuffer( GL_FRAMEBUFFER, 0 );
    }

} // namespace udk
