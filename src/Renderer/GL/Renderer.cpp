#include "Udock/Renderer/Renderer.hpp"
#include "Udock/Physics/Constraint.hpp"
#include "Udock/Physics/Projectile.hpp"
#include "Udock/Settings.hpp"
#include "Udock/Utils/Logger.hpp"
#include "Udock/Window.hpp"
#include <GL/gl3w.h>
#include <backends/imgui_impl_opengl3.h>
#include <fmt/format.h>
#include <stdexcept>

namespace udk
{
    static void APIENTRY debugMessageCallback( const GLenum errorSource,
                                               const GLenum errorType,
                                               const GLuint /* errorId */,
                                               const GLenum errorSeverity,
                                               const GLsizei /* messageLength */,
                                               const GLchar * message,
                                               const void * /* data */ )
    {
        std::string source;
        std::string type;
        std::string severity;

        switch ( errorSource )
        {
        case GL_DEBUG_SOURCE_API: source = "API"; break;
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM: source = "WINDOW SYSTEM"; break;
        case GL_DEBUG_SOURCE_SHADER_COMPILER: source = "SHADER COMPILER"; break;
        case GL_DEBUG_SOURCE_THIRD_PARTY: source = "THIRD PARTY"; break;
        case GL_DEBUG_SOURCE_APPLICATION: source = "APPLICATION"; break;
        case GL_DEBUG_SOURCE_OTHER: source = "UNKNOWN"; break;
        default: source = "UNKNOWN"; break;
        }

        switch ( errorType )
        {
        case GL_DEBUG_TYPE_ERROR: type = "ERROR"; break;
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: type = "DEPRECATED BEHAVIOR"; break;
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR: type = "UDEFINED BEHAVIOR"; break;
        case GL_DEBUG_TYPE_PORTABILITY: type = "PORTABILITY"; break;
        case GL_DEBUG_TYPE_PERFORMANCE: type = "PERFORMANCE"; break;
        case GL_DEBUG_TYPE_OTHER: type = "OTHER"; break;
        case GL_DEBUG_TYPE_MARKER: type = "MARKER"; break;
        default: type = "UNKNOWN"; break;
        }

        switch ( errorSeverity )
        {
        case GL_DEBUG_SEVERITY_HIGH: severity = "HIGH"; break;
        case GL_DEBUG_SEVERITY_MEDIUM: severity = "MEDIUM"; break;
        case GL_DEBUG_SEVERITY_LOW: severity = "LOW"; break;
        case GL_DEBUG_SEVERITY_NOTIFICATION: severity = "NOTIFICATION"; break;
        default: severity = "UNKNOWN"; break;
        }

        const std::string finalMessage
            = fmt::format( "[OPENGL][{} {} {}]: {}", severity, source, type, std::string( message ) );

        switch ( errorSeverity )
        {
        case GL_DEBUG_SEVERITY_HIGH: UDK_ERROR( finalMessage ); break;
        case GL_DEBUG_SEVERITY_NOTIFICATION: break;
        default: UDK_DEBUG( finalMessage ); break;
        }
    }

    Renderer::Renderer( const Window *  window,
                        UdockSettings * settings,
                        System *        system,
                        PhysicalWorld * world,
                        Entity          camera ) :
        m_window( window ),
        m_viewportSize( window->getSize() ), m_context( SDL_GL_CreateContext( window->getHandle() ) ),
        m_settings( settings ), m_system( system ), m_world( world ), m_cameraEntity( camera )
    {
        if ( gl3wInit() )
            throw std::runtime_error( "Can't initialize OpenGL." );

        if ( !gl3wIsSupported( 4, 1 ) )
            throw std::runtime_error( "This renderer needs OpenGL 4.1 support." );

#ifdef _DEBUG
        glEnable( GL_DEBUG_OUTPUT );
        glDebugMessageCallback( debugMessageCallback, nullptr );
#endif

        // Delay those initializations to make sure gl3w has been initialized before.
        m_pointHandler = std::make_unique<PointHandler>();
        m_lineHandler  = std::make_unique<LineHandler>( m_settings->constraintColor );

        m_ui           = udk::Ui( m_window, this );
        m_geometryPass = std::make_unique<GeometryPass>( m_viewportSize );
        m_shadingPass  = std::make_unique<ShadingPass>( m_viewportSize );

        m_rendererLevelUniforms = UniformManager( "CameraUniforms", 64 * sizeof( float ) );
        m_rendererLevelUniforms.addValue<glm::mat4>( "uView" );
        m_rendererLevelUniforms.addValue<glm::mat4>( "uProjection" );

        m_meshListener = Listener<Mesh>(
            m_system, [ this ]( Entity entity, Event eventType ) { onNewMesh( entity, eventType ); } );
        m_meshListener.subscribe( Event::Construct );
        m_meshListener.subscribe( Event::Destroy );

        createRenderingPipeline();

        m_initalized = true;
    }

    Renderer::~Renderer() { SDL_GL_DeleteContext( m_context ); }

    void Renderer::setSkybox( std::unique_ptr<Skybox> && skybox ) { m_skybox = std::move( skybox ); }

    void Renderer::addGeometrySubpass( GeometrySubpass && subpass )
    {
        m_geometryPass->addSubpass( std::move( subpass ) );
    }

    void Renderer::addPostProcessSubpass( PostProcessSubpass && subpass )
    {
        m_shadingPass->addSubpass( std::move( subpass ) );
    }

    void Renderer::clear()
    {
        m_geometryPass->clear();
        m_shadingPass->clear();
    }

    void Renderer::resize( glm::ivec2 newSize )
    {
        if ( m_viewportSize == newSize )
            return;
        m_viewportSize = newSize;
        m_geometryPass->resize( newSize );
        m_shadingPass->resize( newSize );
        m_linearizedDepthFrameBuffer.resize( newSize );
        m_trailsFrameBuffer.resize( newSize );
    }

    void Renderer::render()
    {
        glViewport( 0, 0, m_viewportSize.x, m_viewportSize.y );

        Camera &        camera     = m_cameraEntity.get<Camera>();
        const glm::mat4 viewMatrix = camera.getViewMatrix();
        m_rendererLevelUniforms.updateValue<glm::mat4>( "uView", viewMatrix );
        m_rendererLevelUniforms.updateValue<glm::mat4>( "uProjection", camera.getProjectionMatrix() );

        glEnable( GL_DEPTH_TEST );
        m_geometryPass->render( m_rendererLevelUniforms );
        glDisable( GL_DEPTH_TEST );

        m_shadingPass->render( m_rendererLevelUniforms, m_geometryPass.get() );

        const FrameBuffer & renderedFbo = m_shadingPass->getFrameBuffer();
        renderedFbo.bind();
        m_ui.render();
        renderedFbo.unbind();

        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
        glBindFramebuffer( GL_READ_FRAMEBUFFER, renderedFbo.getHandle() );
        glBindFramebuffer( GL_DRAW_FRAMEBUFFER, 0 );
        glBlitFramebuffer( 0,
                           0,
                           m_viewportSize.x,
                           m_viewportSize.y,
                           0,
                           0,
                           m_viewportSize.x,
                           m_viewportSize.y,
                           GL_COLOR_BUFFER_BIT,
                           GL_NEAREST );
        ImGui_ImplOpenGL3_RenderDrawData( ImGui::GetDrawData() );
        SDL_GL_SwapWindow( m_window->getHandle() );
    }

    void Renderer::createRenderingPipeline()
    {
        addGeometrySubpass( GeometrySubpass {
            Program { "MolecularSurfaceProgram", { "shaders/SesMesh/SesMesh.vert", "shaders/SesMesh/SesMesh.frag" } },
            [ & ]( const Program & program )
            {
                const entt::registry & registry = m_system->getRegistry();
                for ( std::pair<const entt::entity, MolecularSurfaceHandler> & handler : m_surfaceHandlers )
                {
                    const Transform & transform = registry.get<Transform>( handler.first );
                    handler.second.render( program, transform.get(), m_settings->moleculeSurfaceColors );
                }
            } } );

        addGeometrySubpass(
            GeometrySubpass { Program { "MeshProgram", { "shaders/Mesh/Mesh.vert", "shaders/Mesh/Mesh.frag" } },
                              [ & ]( const Program & program )
                              {
                                  const entt::registry & registry = m_system->getRegistry();
                                  for ( std::pair<const entt::entity, MeshHandler> & handler : m_meshHandlers )
                                  {
                                      const Transform & transform = registry.get<Transform>( handler.first );
                                      handler.second.render( program, transform.get() );
                                  }
                              } } );

        addGeometrySubpass( GeometrySubpass {
            Program { "SphereProgram",
                      { "shaders/Sphere/Sphere.vert", "shaders/Sphere/Sphere.geom", "shaders/Sphere/Sphere.frag" } },
            [ & ]( const Program & /* program */ )
            {
                std::vector<glm::vec4> points;
                std::vector<glm::vec4> colors;
                if ( m_settings->displayTarget )
                {
                    Target & cameraTarget = m_cameraEntity.get<Target>();
                    points.emplace_back( glm::vec4 { cameraTarget.position, m_settings->targetSize } );
                    colors.emplace_back( m_settings->targetColor );
                }

                entt::registry & registry    = m_system->getRegistry();
                const auto       projectiles = registry.view<Projectile>();
                for ( const entt::entity entity : projectiles )
                {
                    const auto & [ transform, projectile ] = registry.get<Transform, Projectile>( entity );
                    points.emplace_back( glm::vec4 { transform.getPosition(), projectile.getRadius() } );
                    colors.emplace_back( glm::vec4 { 1.f } );
                }

                constexpr float ConstraintSize = 2.f;
                const auto      anchors        = registry.view<Anchor>();
                for ( const entt::entity entity : anchors )
                {
                    const Anchor & current = registry.get<Anchor>( entity );
                    points.emplace_back( glm::vec4 { current.getWorldPosition(), ConstraintSize } );
                    colors.emplace_back( glm::vec4 { m_settings->constraintColor } );
                }
                const auto tempAnchors = registry.view<Transform, TemporaryAnchor>();
                for ( const entt::entity entity : tempAnchors )
                {
                    const auto & [ transform, current ] = registry.get<Transform, TemporaryAnchor>( entity );
                    points.emplace_back( glm::vec4 {
                        glm::vec3( transform.get() * glm::vec4( current.relativePosition, 1.f ) ), ConstraintSize } );
                    colors.emplace_back( glm::vec4 { m_settings->constraintColor } );
                }

                if ( !points.empty() )
                    m_pointHandler->render( points, colors );
            } } );

        addGeometrySubpass(
            GeometrySubpass { Program { "LineProgram", { "shaders/Line/Line.vert", "shaders/Line/Line.frag" } },
                              [ & ]( const Program & program )
                              {
                                  entt::registry & registry = m_system->getRegistry();

                                  auto                   constraints = registry.view<LineConstraint>();
                                  std::vector<glm::vec4> lines       = {};
                                  lines.reserve( constraints.size() * 2 );
                                  for ( const entt::entity entity : constraints )
                                  {
                                      LineConstraint & current = registry.get<LineConstraint>( entity );
                                      Anchor &         first   = current.firstChild.get<Anchor>();
                                      Anchor &         second  = current.secondChild.get<Anchor>();
                                      lines.emplace_back( glm::vec4 { first.getWorldPosition(), 1.f } );
                                      lines.emplace_back( glm::vec4 { second.getWorldPosition(), 1.f } );
                                  }

                                  m_lineHandler->render( program, lines, m_settings->constraintColor );
                              } } );

        addGeometrySubpass( GeometrySubpass {
            Program { "SkyboxProgram", { "shaders/Skybox/Skybox.vert", "shaders/Skybox/Skybox.frag" } },
            [ & ]( const Program & program )
            {
                if ( m_skybox )
                    m_skybox->render();
            } } );

        addPostProcessSubpass( PostProcessSubpass {
            Program( "DiffuseProgram", { "shaders/FullScreenQuad.vert", "shaders/Diffuse.frag" } ),
            [ this ]( const Program &                 program,
                      const FrameBuffer &             finalFrameBuffer,
                      const GBuffer &                 gBuffer,
                      std::optional<UniformManager> & uniforms )
            {
                finalFrameBuffer.bind();
                gBuffer.viewPositionNormals->bindAsTexture( "gbViewPositionNormal", program, 0 );
                gBuffer.colors->bindAsTexture( "gbColor", program, 1 );

                if ( m_settings->enableShadingWithEnvironment || m_skybox )
                    m_skybox->bind( "skybox", program, 2 );

                uniforms->updateValue<glm::vec4>( "uBackgroundColor", m_settings->backgroundColor );

                const auto & [ camera, transform ] = m_cameraEntity.get<Camera, Transform>();
                const glm::vec4 lightPosition      = camera.getViewMatrix() * glm::vec4( transform.getPosition(), 1.f );

                uniforms->updateValue<glm::vec4>( "uLightPosition", lightPosition );
                uniforms->updateValue<glm::vec4>( "uLightColor", glm::vec4 { 1.f } );
                uniforms->updateValue<bool>( "uWithEnvironment", m_settings->enableShadingWithEnvironment );
                uniforms->updateValue<bool>( "uIsSkybox", bool( m_skybox ) );
                uniforms->bind( program, UdockSettings::ShaderUniformsRendererIndexDefault + 1 );
            },
            UniformManager( "DiffuseUniforms", 3 * sizeof( glm::vec4 ) ) } );

        m_linearizedDepthFrameBuffer = FrameBuffer( m_viewportSize );
        m_linearizedDepthFrameBuffer.addAttachment( { m_viewportSize,
                                                      Format::R16F,
                                                      DataType::Float,
                                                      Filter::Linear,
                                                      Filter::Linear,
                                                      WrappingMode::Repeat,
                                                      WrappingMode::Repeat } );
        addPostProcessSubpass( PostProcessSubpass {
            Program( "DepthLinearizationProgram", { "shaders/FullScreenQuad.vert", "shaders/LinearizeDepth.frag" } ),
            [ this ]( const Program &                 program,
                      const FrameBuffer &             finalFrameBuffer,
                      const GBuffer &                 gBuffer,
                      std::optional<UniformManager> & uniforms )
            {
                const Camera & camera  = m_cameraEntity.get<Camera>();
                const float    camNear = camera.getNear();
                const float    camFar  = camera.getFar();
                uniforms->updateValue( "uClipInfo", glm::vec4( camNear * camFar, camFar, camFar - camNear, 1.f ) );

                uniforms->bind( program, UdockSettings::ShaderUniformsRendererIndexDefault + 1 );
                m_linearizedDepthFrameBuffer.bind();
                gBuffer.depth->bindAsTexture( "depthTexture", program, 0 );
            },
            UniformManager( "LinearizeDepthUniforms", sizeof( glm::vec4 ) ) } );

        addPostProcessSubpass( PostProcessSubpass {
            Program( "ToneMappingProgram", { "shaders/FullScreenQuad.vert", "shaders/ToneMapping.frag" } ),
            [ this ]( const Program &     program,
                      const FrameBuffer & finalFrameBuffer,
                      const GBuffer &     gBuffer,
                      std::optional<UniformManager> & /* uniforms  */ )
            {
                finalFrameBuffer.synchronize();

                finalFrameBuffer.bind();
                finalFrameBuffer.getAttachment( 0 ).bindAsTexture( "colorTexture", program, 0 );
                gBuffer.viewPositionNormals->bindAsTexture( "gbViewPositionNormal", program, 1 );
            } } );

        addPostProcessSubpass( PostProcessSubpass {
            Program( "ContoursProgram", { "shaders/FullScreenQuad.vert", "shaders/Contours.frag" } ),
            [ this ]( const Program &                 program,
                      const FrameBuffer &             finalFrameBuffer,
                      const GBuffer &                 gBuffer,
                      std::optional<UniformManager> & uniforms )
            {
                finalFrameBuffer.synchronize();

                const Camera & camera = m_cameraEntity.get<Camera>();
                uniforms->updateValue( "uNear", camera.getNear() );
                uniforms->updateValue( "uFar", camera.getFar() );
                uniforms->updateValue( "uStartDecay", .5f );

                finalFrameBuffer.bind();
                uniforms->bind( program, UdockSettings::ShaderUniformsRendererIndexDefault + 1 );
                gBuffer.viewPositionNormals->bindAsTexture( "gbViewPositionNormal", program, 0 );
                finalFrameBuffer.getAttachment( 0 ).bindAsTexture( "colorTexture", program, 1 );
                m_linearizedDepthFrameBuffer.getAttachment( 0 ).bindAsTexture( "linearDepthTexture", program, 2 );
            },
            UniformManager( "ContoursUniforms", sizeof( float ) * 4 ) } );

        m_trailsFrameBuffer = FrameBuffer( m_viewportSize );
        m_trailsFrameBuffer.addAttachment( { m_viewportSize,
                                             Format::R16F,
                                             DataType::Float,
                                             Filter::Linear,
                                             Filter::Linear,
                                             WrappingMode::Repeat,
                                             WrappingMode::Repeat,
                                             0,
                                             true } );
        addPostProcessSubpass( PostProcessSubpass {
            Program( "TrailsProgram", { "shaders/FullScreenQuad.vert", "shaders/Trails.frag" } ),
            [ this ]( const Program &                 program,
                      const FrameBuffer &             finalFrameBuffer,
                      const GBuffer &                 gBuffer,
                      std::optional<UniformManager> & uniforms )
            {
                if ( !m_settings->enableTrails )
                    return;

                const auto & [ camera, transform ] = m_cameraEntity.get<Camera, Transform>();

                const glm::vec3 currentPosition = transform.getPosition();
                const glm::vec4 shift           = camera.getViewMatrix() * glm::vec4 { m_lastCameraPosition, 1.f };

                const glm::vec2 imageShift
                    = glm::vec2 { -shift.x, shift.y } * glm::length( currentPosition - m_lastCameraPosition );
                m_lastCameraPosition = currentPosition;

                uniforms->updateValue( "uShift", glm::ivec2( imageShift.x, imageShift.y ) );
                uniforms->updateValue( "uAttenuation", .90f );
                uniforms->updateValue( "uInitialMix", .5f );

                uniforms->bind( program, UdockSettings::ShaderUniformsRendererIndexDefault + 1 );

                m_trailsFrameBuffer.bind();
                gBuffer.viewPositionNormals->bindAsTexture( "gbViewPositionNormal", program, 0 );
                m_trailsFrameBuffer.getAttachment( 0 ).bindAsTexture( "trailsTexture", program, 1 );
            },
            UniformManager( "TrailsUniforms", sizeof( glm::vec4 ) ) } );

        addPostProcessSubpass( PostProcessSubpass {
            Program( "ApplyTrailsProgram", { "shaders/FullScreenQuad.vert", "shaders/ApplyTrails.frag" } ),
            [ this ]( const Program &     program,
                      const FrameBuffer & finalFrameBuffer,
                      const GBuffer & /* gBuffer */,
                      std::optional<UniformManager> & uniforms )
            {
                if ( !m_settings->enableTrails )
                    return;

                finalFrameBuffer.synchronize();
                m_trailsFrameBuffer.synchronize();

                uniforms->updateValue( "uTrailsColor", glm::vec4( 1.f ) );
                uniforms->bind( program, UdockSettings::ShaderUniformsRendererIndexDefault + 1 );

                finalFrameBuffer.bind();
                m_trailsFrameBuffer.getAttachment( 0 ).bindAsTexture( "trailsTexture", program, 0 );
                finalFrameBuffer.getAttachment( 0 ).bindAsTexture( "colorTexture", program, 1 );
            },
            UniformManager( "TrailsUniforms", sizeof( glm::vec4 ) ) } );
    }

    void Renderer::onNewMesh( Entity newEntity, Event eventType )
    {
        if ( eventType == Event::Construct )
        {
            if ( newEntity.any_of<Molecule>() )
            {
                m_surfaceHandlers.emplace(
                    newEntity, MolecularSurfaceHandler { newEntity.get<Mesh>(), m_settings->moleculeSurfaceColors } );
            }
            else
            {
                m_meshHandlers.emplace( newEntity, MeshHandler { newEntity.get<Mesh>() } );
            }
        }
        else if ( eventType == Event::Destroy )
        {
            if ( m_surfaceHandlers.find( newEntity ) != m_surfaceHandlers.end() )
                m_surfaceHandlers.erase( newEntity );
            else if ( m_meshHandlers.find( newEntity ) != m_meshHandlers.end() )
                m_meshHandlers.erase( newEntity );
        }
    }

} // namespace udk
