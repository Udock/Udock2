#include "Udock/Renderer/Ui.hpp"
#include "Udock/Renderer/Renderer.hpp"
#include "Udock/Window.hpp"
#include <GL/gl3w.h>
#include <backends/imgui_impl_opengl3.h>
#include <backends/imgui_impl_sdl.h>
#include <imgui.h>
#include <stdexcept>

namespace udk
{
    Ui::Ui( const Window * window, const Renderer * renderer ) : m_window( window ), m_renderer( renderer )
    {
        if ( !IMGUI_CHECKVERSION() )
        {
            throw std::runtime_error( "IMGUI_CHECKVERSION() failed" );
        }

        ImGui::CreateContext();

        // Setup controls.
        ImGuiIO & io = ImGui::GetIO();
        io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;

        // Style.
        ImGui::StyleColorsDark();
        ImGui::GetStyle().WindowRounding    = 0.f;
        ImGui::GetStyle().ChildRounding     = 0.f;
        ImGui::GetStyle().FrameRounding     = 0.f;
        ImGui::GetStyle().GrabRounding      = 0.f;
        ImGui::GetStyle().PopupRounding     = 0.f;
        ImGui::GetStyle().ScrollbarRounding = 0.f;
        ImGui::GetStyle().WindowBorderSize  = 0.f;
        ImGui::GetStyle().WindowPadding     = ImVec2( 0.f, 0.f );

        // Setup Platform/Renderer bindings.
        if ( ImGui_ImplSDL2_InitForOpenGL( m_window->getHandle(), m_renderer->getHandle() ) == false )
        {
            throw std::runtime_error( "ImGui_ImplSDL2_InitForOpenGL failed" );
        }

        if ( ImGui_ImplOpenGL3_Init( "#version 410" ) == false )
        {
            throw std::runtime_error( "ImGui_ImplOpenGL3_Init failed" );
        }

        m_isInitialized = true;
    }

    Ui::Ui( Ui && other ) noexcept :
        m_window( std::exchange( other.m_window, nullptr ) ), m_renderer( std::exchange( other.m_renderer, nullptr ) ),
        m_manager( std::move( other.m_manager ) ), m_isInitialized( std::exchange( other.m_isInitialized, false ) ),
        m_isVisible( std::exchange( other.m_isVisible, true ) )
    {
    }

    Ui & Ui::operator=( Ui && other ) noexcept
    {
        std::swap( m_window, other.m_window );
        std::swap( m_renderer, other.m_renderer );
        std::swap( m_manager, other.m_manager );
        std::swap( m_isInitialized, other.m_isInitialized );
        std::swap( m_isVisible, other.m_isVisible );

        return *this;
    }

    Ui::~Ui()
    {
        if ( m_isInitialized )
        {
            ImGui_ImplOpenGL3_Shutdown();
            ImGui_ImplSDL2_Shutdown();

            if ( ImGui::GetCurrentContext() != nullptr )
            {
                ImGui::DestroyContext();
            }
        }
    }

    void Ui::render()
    {
        if ( !m_isInitialized )
            return;

        ImGuiIO & io = ImGui::GetIO();

        // New frame.
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplSDL2_NewFrame( m_window->getHandle() );
        ImGui::NewFrame();

        // Configuration.
        const ImGuiViewport * viewport = ImGui::GetMainViewport();
        ImGui::SetNextWindowPos( viewport->Pos );
        ImGui::SetNextWindowSize( viewport->Size );
        ImGui::SetNextWindowBgAlpha( 0.0f );
        constexpr ImGuiWindowFlags windowFlags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoTitleBar
                                                 | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize
                                                 | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoBringToFrontOnFocus
                                                 | ImGuiWindowFlags_NoNavFocus | ImGuiWindowFlags_NoInputs;

        if ( ImGui::Begin( m_window->getTitle().data(), &m_isVisible, windowFlags ) == false )
        {
            ImGui::End();
            return;
        }

        // Viewport.
        SDL_GL_MakeCurrent( m_window->getHandle(), m_renderer->getHandle() );
        glViewport( 0, 0, static_cast<int>( io.DisplaySize.x ), static_cast<int>( io.DisplaySize.y ) );

        // Draw menu.
        ImGui::PushStyleVar( ImGuiStyleVar_WindowPadding, ImVec2( 0.f, 0.f ) );
        m_manager.draw();
        ImGui::PopStyleVar();

        ImGui::End();
        ImGui::Render();
    }
} // namespace udk
