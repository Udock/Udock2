#include "Udock/Renderer/Texture.hpp"
#include <fmt/format.h>
#include <stb_image.h>

namespace udk
{
    Texture::Texture( const Path & textureFile )
    {
        // Based on imgui's Image Loading and Displaying Examples
        // Reference: https://github.com/ocornut/imgui/wiki/Image-Loading-and-Displaying-Examples
        int               width          = 0;
        int               height         = 0;
        const std::string textureFileStr = textureFile.string();
        unsigned char *   image_data     = stbi_load( textureFileStr.c_str(), &width, &height, nullptr, 4 );
        if ( image_data == nullptr )
            throw std::runtime_error( fmt::format( "Can't load image {}", textureFileStr ) );

        glGenTextures( 1, &m_handle );
        glBindTexture( GL_TEXTURE_2D, m_handle );

        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_2D,
                         GL_TEXTURE_WRAP_S,
                         GL_CLAMP_TO_EDGE ); // This is required on WebGL for non power-of-two textures
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE ); // Same

#if defined( GL_UNPACK_ROW_LENGTH ) && !defined( __EMSCRIPTEN__ )
        glPixelStorei( GL_UNPACK_ROW_LENGTH, 0 );
#endif
        glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image_data );
        stbi_image_free( image_data );

        m_dimensions.x = width;
        m_dimensions.y = height;
    }

    Texture::Texture( Texture && other )
    {
        std::swap( m_handle, other.m_handle );
        std::swap( m_dimensions, other.m_dimensions );
    }
    Texture & Texture::operator=( Texture && other )
    {
        std::swap( m_handle, other.m_handle );
        std::swap( m_dimensions, other.m_dimensions );
        return *this;
    }
    Texture::~Texture()
    {
        if ( glIsTexture( m_handle ) )
        {
            glDeleteTextures( 1, &m_handle );
            m_handle = GL_INVALID_VALUE;
        }
    }

} // namespace udk
