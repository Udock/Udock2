#include "Udock/Renderer/Buffer.hpp"

#include <vector>

namespace udk
{
    Buffer::Buffer( const std::size_t allocationSize, const BufferAuthorization authorizations, const bool zeroInit ) :
        m_currentSize( allocationSize ), m_authorizations( authorizations )
    {
        glGenBuffers( 1, &m_handle );
        glBindBuffer( GL_UNIFORM_BUFFER, m_handle );
        if ( zeroInit )
        {
            const auto zeroData = std::vector<uint8_t>( allocationSize, 0u );
            glBufferData( GL_UNIFORM_BUFFER, zeroData.size(), nullptr, toGL( m_authorizations ) );
        }
        else
        {
            glBufferData( GL_UNIFORM_BUFFER, allocationSize, nullptr, toGL( m_authorizations ) );
        }

        glBindBuffer( GL_UNIFORM_BUFFER, 0 );
    }

    Buffer::Buffer( const uint8_t * const data, const std::size_t size, const BufferAuthorization authorizations ) :
        m_currentSize( size ), m_authorizations( authorizations )
    {
        glGenBuffers( 1, &m_handle );
        glBindBuffer( GL_UNIFORM_BUFFER, m_handle );
        glBufferStorage( m_handle, m_currentSize, data, toGL( m_authorizations ) );
        glBindBuffer( GL_UNIFORM_BUFFER, 0 );
    }

    Buffer::~Buffer()
    {
        if ( glIsBuffer( m_handle ) )
        {
            glDeleteBuffers( 1, &m_handle );
            m_handle = GL_INVALID_VALUE;
        }
    }

    Buffer::Buffer( const Buffer & other ) :
        m_currentSize( other.m_currentSize ), m_authorizations( other.m_authorizations )
    {
        glGenBuffers( 1, &m_handle );
        glBindBuffer( GL_UNIFORM_BUFFER, m_handle );
        glBufferData( GL_UNIFORM_BUFFER, m_currentSize, nullptr, toGL( m_authorizations ) );
        glBindBuffer( GL_UNIFORM_BUFFER, 0 );

        GLint size = static_cast<GLint>( other.m_currentSize );

        glBindBuffer( GL_COPY_READ_BUFFER, other.m_handle );
        glGetBufferParameteriv( GL_COPY_READ_BUFFER, GL_BUFFER_SIZE, &size );

        glBindBuffer( GL_COPY_WRITE_BUFFER, m_handle );
        glCopyBufferSubData( GL_COPY_READ_BUFFER, GL_COPY_WRITE_BUFFER, 0, 0, m_currentSize );
    }

    Buffer & Buffer::operator=( const Buffer & other )
    {
        glDeleteBuffers( 1, &m_handle );

        m_currentSize    = other.m_currentSize;
        m_authorizations = other.m_authorizations;

        glGenBuffers( 1, &m_handle );
        glBindBuffer( GL_UNIFORM_BUFFER, m_handle );
        glBufferData( GL_UNIFORM_BUFFER, m_currentSize, nullptr, toGL( m_authorizations ) );
        glBindBuffer( GL_UNIFORM_BUFFER, 0 );

        GLint size = static_cast<GLint>( other.m_currentSize );

        glBindBuffer( GL_COPY_READ_BUFFER, other.m_handle );
        glGetBufferParameteriv( GL_COPY_READ_BUFFER, GL_BUFFER_SIZE, &size );

        glBindBuffer( GL_COPY_WRITE_BUFFER, m_handle );
        glGetBufferParameteriv( GL_COPY_WRITE_BUFFER, GL_BUFFER_SIZE, &size );

        glCopyBufferSubData( GL_COPY_READ_BUFFER, GL_COPY_WRITE_BUFFER, 0, 0, m_currentSize );

        return *this;
    }

    Buffer::Buffer( Buffer && other ) noexcept
    {
        std::swap( m_handle, other.m_handle );
        std::swap( m_authorizations, other.m_authorizations );
        std::swap( m_currentSize, other.m_currentSize );
    }

    Buffer & Buffer::operator=( Buffer && other ) noexcept
    {
        std::swap( m_handle, other.m_handle );
        std::swap( m_authorizations, other.m_authorizations );
        std::swap( m_currentSize, other.m_currentSize );

        return *this;
    }

    void Buffer::bind( const std::size_t index, const BufferType bufferType ) const
    {
        glBindBufferBase( toGL( bufferType ), static_cast<GLuint>( index ), m_handle );
    }

    void Buffer::bind( const std::size_t index,
                       const std::size_t offset,
                       const std::size_t range,
                       const BufferType  bufferType ) const
    {
        glBindBufferRange( toUnderlying( bufferType ),
                           static_cast<GLuint>( index ),
                           m_handle,
                           static_cast<GLuint>( offset ),
                           static_cast<GLsizeiptr>( range ) );
    }

    void Buffer::resize( const std::size_t newSize, const bool zeroInit )
    {
        GLuint newHandle;
        glGenBuffers( 1, &newHandle );
        glBindBuffer( GL_UNIFORM_BUFFER, newHandle );
        if ( zeroInit )
        {
            const auto zeroData = std::vector<uint8_t>( newSize, 0u );
            glBufferData( GL_UNIFORM_BUFFER, zeroData.size(), zeroData.data(), toGL( m_authorizations ) );
        }
        else
        {
            glBufferData( GL_UNIFORM_BUFFER, newSize, nullptr, toGL( m_authorizations ) );
        }
        glBindBuffer( GL_UNIFORM_BUFFER, 0 );

        GLint oldSizeGl = static_cast<GLint>( m_currentSize );
        glBindBuffer( GL_COPY_READ_BUFFER, m_handle );
        glGetBufferParameteriv( GL_COPY_READ_BUFFER, GL_BUFFER_SIZE, &oldSizeGl );

        GLint newSizeGl = static_cast<GLint>( newSize );
        glBindBuffer( GL_COPY_WRITE_BUFFER, newHandle );

        glCopyBufferSubData( GL_COPY_READ_BUFFER, GL_COPY_WRITE_BUFFER, 0, 0, m_currentSize );
        m_currentSize = newSize;

        glDeleteBuffers( 1, &m_handle );
        m_handle = newHandle;
    }

    void Buffer::unmap() const { glUnmapBuffer( GL_UNIFORM_BUFFER ); }

} // namespace udk
