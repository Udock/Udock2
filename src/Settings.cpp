#include "Udock/Settings.hpp"

namespace udk
{
    const bool UdockSettings::DisplayTargetDefault = false;

    const uint8_t UdockSettings::ShaderUniformsRendererIndexDefault  = 0;
    const uint8_t UdockSettings::ShaderUniformsModelIndexDefault     = 1;
    const bool    UdockSettings::EnableTrailsDefault                 = false;
    const bool    UdockSettings::EnableShadingWithEnvironmentDefault = false;

    const float     UdockSettings::TargetSizeDefault  = .5f;
    const glm::vec4 UdockSettings::TargetColorDefault = { .9f, 0.f, 0.f, 1.f };

    const glm::vec4 UdockSettings::PositiveChargeColorDefault = glm::vec4 { 0.f, 0.f, 1.f, 1.f };
    const glm::vec4 UdockSettings::NeutralChargeColorDefault  = glm::vec4 { 1.f, 1.f, 1.f, 1.f };
    const glm::vec4 UdockSettings::NegativeChargeColorDefault = glm::vec4 { 1.f, 0.f, 0.f, 1.f };
    const float     UdockSettings::ChargeContrastDefault      = 1.f;

    const glm::vec4 UdockSettings::ConstraintColorDefault = glm::vec4 { 1.f, 1.f, 0.f, 1.f };
    const float     UdockSettings::DockingSpeedDefault    = 50.f;

    const float UdockSettings::EnergyMaxRadiusDefault      = 12.f;
    const float UdockSettings::OptimizationStepSizeDefault = 1.f;

    const SkyboxType UdockSettings::SkyboxTypeDefault      = SkyboxType::Realistic;
    const glm::vec4  UdockSettings::BackgroundColorDefault = glm::vec4 { 0.7f, 0.81f, 1.f, 1.0f };
    const float      UdockSettings::TurbidityDefault       = 4.f;
    const float      UdockSettings::GroundAlbedoDefault    = 0.4f;
} // namespace udk
