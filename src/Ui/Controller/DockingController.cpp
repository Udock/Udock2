#include "Udock/Ui/Controller/DockingController.hpp"
#include "Udock/Physics/Constraint.hpp"
#include "Udock/Settings.hpp"
#include "Udock/System/Camera.hpp"
#include <glm/gtx/norm.hpp>

namespace udk
{
    const float DockingControllerSettings::EntityAngularVelocityDefault      = 1e3f;
    const float DockingControllerSettings::UserInputAngularDecaySpeedDefault = 50.f;

    DockingController::DockingController( const DockingControllerSettings * settings,
                                          System *                          system,
                                          PhysicalWorld *                   world,
                                          Entity                            cameraEntity ) :
        m_settings( settings ),
        m_system( system ), m_world( world ), m_cameraEntity( cameraEntity )
    {
        m_moleculeListener = Listener<Molecule>(
            m_system,
            [ this ]( Entity entity, Event eventType )
            {
                const bool anchorEntityDestroyed = m_firstAnchor && m_firstAnchor->get<Anchor>().parent == entity;
                if ( eventType == Event::Destroy && anchorEntityDestroyed )
                {
                    m_firstAnchor->destroy();
                    m_firstAnchor.reset();
                }
            } );
        m_moleculeListener.subscribe( Event::Destroy );
    }

    DockingController::~DockingController()
    {
        if ( m_firstAnchor && m_firstAnchor->valid() )
            m_firstAnchor->destroy();
    }

    void DockingController::update( const UiInput & uiInput )
    {
        const auto & [ transform, camera ] = m_cameraEntity.get<Transform, Camera>();

        if ( uiInput.doubleLeftClick )
        {
            const glm::vec3 worldPos = camera.screenToWorld( uiInput.mousePosition, uiInput.windowSize );
            const glm::vec3 position = transform.getPosition();
            const Ray       ray { position, glm::normalize( worldPos - position ) };

            Entity      hitEntity;
            RigidBody * hitBody = m_world->intersect( ray, hitEntity );
            if ( hitBody && hitEntity.all_of<Molecule>() )
            {
                hitBody->setAngularVelocity( glm::vec3 {} );
                hitBody->setLinearVelocity( glm::vec3 {} );
                const Aabb bb = hitBody->getBoundingBox();
                camera.lookAt( bb );
            }
        }

        if ( uiInput.mouseLeftPressed && !uiInput.isKeyPressed( SDL_SCANCODE_LCTRL ) )
        {
            const glm::vec3   worldPos = camera.screenToWorld( uiInput.mousePosition, uiInput.windowSize );
            const glm::vec3 & position = transform.getPosition();
            const Ray         ray { position, glm::normalize( worldPos - position ) };

            Entity      hitEntity;
            RigidBody * hitBody = m_world->intersect( ray, hitEntity );
            if ( hitBody && hitEntity.all_of<Molecule>() )
            {
                const glm::vec2 deltaAngles = glm::vec2 { uiInput.deltaMousePosition };
                const glm::vec3 force       = -deltaAngles.y * transform.getLeft() + deltaAngles.x * transform.getUp();
                if ( glm::any( glm::isnan( force ) ) )
                    return;

                hitBody->applyTorque( force * m_settings->entityAngularVelocity );
            }
        }

        if ( uiInput.mouseLeftClicked && uiInput.isKeyPressed( SDL_SCANCODE_LCTRL ) )
        {
            const glm::vec3   worldPos = camera.screenToWorld( uiInput.mousePosition, uiInput.windowSize );
            const glm::vec3 & position = transform.getPosition();
            const Ray         ray { position, glm::normalize( worldPos - position ) };

            glm::vec3 hitPosition;
            Entity    hitEntity;
            if ( m_world->intersect( ray, hitPosition, hitEntity ) && hitEntity.all_of<Molecule>() )
            {
                RigidBody & body = hitEntity.get<RigidBody>();
                if ( m_firstAnchor && m_firstAnchor->valid()
                     && &body != &m_firstAnchor->get<Anchor>().parent.get<RigidBody>() )
                {
                    Entity secondAnchor = m_system->create();
                    secondAnchor.emplace<Anchor>( hitEntity, hitPosition );

                    Entity newConstraint = m_system->create();
                    newConstraint.emplace<LineConstraint>( *m_firstAnchor, secondAnchor );
                    m_firstAnchor.reset();
                }
                else
                {
                    if ( m_firstAnchor && m_firstAnchor->valid() )
                        m_firstAnchor->destroy();

                    m_firstAnchor = m_system->create();
                    m_firstAnchor->emplace<Anchor>( hitEntity, hitPosition );
                }
            }
        }

        if ( uiInput.isKeyPressed( SDL_SCANCODE_ESCAPE ) && m_firstAnchor )
        {
            m_firstAnchor->destroy();
            m_firstAnchor.reset();
        }
    }
} // namespace udk
