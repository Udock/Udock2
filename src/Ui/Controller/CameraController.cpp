#include "Udock/Ui/Controller/CameraController.hpp"
#include "Udock/Settings.hpp"
#include <algorithm>
#include <glm/gtx/compatibility.hpp>

namespace udk
{
    const float CameraControllerSettings::TranslationSpeedDefault       = 150.f;
    const float CameraControllerSettings::TranslationFactorSpeedDefault = 2.f;
    const float CameraControllerSettings::RotationSpeedDefault          = 5e-3f;
    const float CameraControllerSettings::ElasticityFactorDefault       = 20.f;
    const float CameraControllerSettings::ElasticityThresholdDefault    = 1e-4f;

    FreeFly::FreeFly( const CameraControllerSettings * settings, Entity cameraEntity ) :
        m_settings( settings ), m_cameraEntity( cameraEntity )
    {
    }

    void FreeFly::update( const UiInput & uiInput )
    {
        auto & transform = m_cameraEntity.get<Transform>();
        if ( uiInput.mouseRightPressed )
        {
            transform.rotate(
                glm::dvec3( -m_settings->rotationSpeed * static_cast<float>( uiInput.deltaMousePosition.y ),
                            -m_settings->rotationSpeed * static_cast<float>( uiInput.deltaMousePosition.x ),
                            0.f ) );
        }
    }

    TrackBall::TrackBall( const CameraControllerSettings * settings, PhysicalWorld * world, Entity cameraEntity ) :
        m_settings( settings ), m_world( world ), m_cameraEntity( cameraEntity )
    {
    }

    bool TrackBall::update( const UiInput & uiInput, bool updateTarget )
    {
        Transform & transform = m_cameraEntity.get<Transform>();

        bool findProperTarget = true;
        if ( updateTarget )
        {
            const Ray ray { transform.getPosition(), transform.getDirection() };
            m_target                              = { ray.origin + ray.direction * 50.f };
            findProperTarget                      = m_world->intersect( ray, m_target );
            m_cameraEntity.get<Target>().position = m_target;
        }

        // Wheel.
        float deltaDistance = 0.f;
        if ( uiInput.deltaMouseWheel != 0.f )
        {
            deltaDistance = uiInput.deltaMouseWheel * 0.01f;
        }

        bool      needUpdate              = false;
        bool      mouseRightButtonPressed = false;
        glm::vec3 deltaVelocity {};
        if ( uiInput.mouseRightPressed )
        {
            mouseRightButtonPressed = true;
            deltaVelocity.x         = -uiInput.deltaMousePosition.x * 15.f;
            deltaVelocity.y         = uiInput.deltaMousePosition.y * 15.f;
        }

        if ( deltaDistance != 0.f )
        {
            deltaDistance *= m_settings->translationSpeed;

            if ( uiInput.isKeyPressed( SDL_SCANCODE_LSHIFT ) )
            {
                deltaDistance *= m_settings->translationFactorSpeed;
            }
            if ( uiInput.isKeyPressed( SDL_SCANCODE_LCTRL ) )
            {
                deltaDistance /= m_settings->translationFactorSpeed;
            }

            needUpdate = true;
        }

        glm::vec3 velocity {};
        // Set values from settings.
        if ( deltaVelocity != glm::vec3( 0.f ) )
        {
            velocity.x += m_settings->rotationSpeed * 10.f * deltaVelocity.x;
            velocity.y += m_settings->rotationSpeed * 10.f * deltaVelocity.y;
            velocity.z += m_settings->rotationSpeed * 10.f * deltaVelocity.z;
        }

        needUpdate |= velocity != glm::vec3( 0.f );

        // Update if needed.
        if ( needUpdate )
        {
            float distance = glm::distance( transform.getPosition(), m_target );
            distance       = std::clamp( distance - deltaDistance, 0.1f, 10000.f );

            const glm::quat rotation = { glm::vec3( -velocity.y, velocity.x, -velocity.z ) * uiInput.deltaTime };
            transform.rotateAround( rotation, m_target, distance );
        }

        return findProperTarget;
    }
} // namespace udk
