#include "Udock/Ui/Controller/KeyController.hpp"

namespace udk
{
    void KeyboardController::registerCallback( std::vector<KeyConfiguration> key, KeyCallback callback )
    {
        m_callbacks.emplace_back( std::make_pair( key, callback ) );
    }

    void KeyboardController::update( const UiInput & uiInput ) const
    {
        for ( const std::pair<std::vector<KeyConfiguration>, KeyCallback> & callback : m_callbacks )
        {
            bool isEnable = true;
            for ( const KeyConfiguration & keyCode : callback.first )
            {
                if ( !uiInput.isKeyActivated( keyCode.first, keyCode.second ) )
                {
                    isEnable = false;
                    break;
                }
            }

            if ( !isEnable )
                continue;

            callback.second( uiInput );
        }
    }
    void GameControllerController::registerCallback( std::vector<KeyConfiguration> key, KeyCallback callback )
    {
        m_callbacks.emplace_back( std::make_pair( key, callback ) );
    }

    void GameControllerController::update( const UiInput & uiInput ) const
    {
        if ( !uiInput.controller )
            return;

        for ( const std::pair<std::vector<KeyConfiguration>, KeyCallback> & callback : m_callbacks )
        {
            bool isEnable = true;
            for ( const KeyConfiguration & keyCode : callback.first )
            {
                if ( !uiInput.controller->isKeyActivated( keyCode.first, keyCode.second ) )
                {
                    isEnable = false;
                    break;
                }
            }

            if ( !isEnable )
                continue;

            callback.second( uiInput );
        }
    }
} // namespace udk
