#include "Udock/Ui/Controller/SpaceshipController.hpp"
#include "Udock/Physics/RigidBody.hpp"
#include "Udock/System/Camera.hpp"
#include "glm/gtx/norm.hpp"

namespace udk
{
    SpaceshipController::SpaceshipController( Entity spaceShip ) : m_spaceShip( spaceShip ) {}

    void SpaceshipController::update( const UiInput & lastInput,
                                      float           cameraStrength,
                                      float           controllerStrength,
                                      float           torqueReducer )
    {
        glm::vec3 force  = getForce( lastInput );
        glm::vec2 torque = getTorque( lastInput );

        auto & transform = m_spaceShip.get<Transform>();
        auto & body      = m_spaceShip.get<RigidBody>();

        float strength = cameraStrength;
        if ( lastInput.controller )
            strength = controllerStrength;

        const glm::mat3 rotation = glm::mat3( glm::toMat3( transform.getRotation() ) );
        if ( force != glm::vec3 {} )
            body.applyCentralForce( lastInput.deltaTime * rotation * force * strength );
        if ( torque != glm::vec2 {} )
            body.applyTorque( rotation * glm::vec3 { lastInput.deltaTime * strength * torqueReducer * torque, 0.f } );
    }

    glm::vec3 SpaceshipController::getForce( const UiInput & lastInput )
    {
        glm::vec3 force {};
        if ( getFrontInput( lastInput ) )
            force.z--;
        if ( getBackInput( lastInput ) )
            force.z++;
        if ( getLeftInput( lastInput ) )
            force.x--;
        if ( getRightInput( lastInput ) )
            force.x++;

        return force;
    }

    bool SpaceshipController::getFrontInput( const UiInput & lastInput )
    {
        bool isFront = lastInput.isKeyPressed( SDL_SCANCODE_W ) || lastInput.isKeyPressed( SDL_SCANCODE_UP );

        if ( lastInput.controller )
            isFront |= lastInput.controller->rightShoulder > 0.f;

        return isFront;
    }

    bool SpaceshipController::getBackInput( const UiInput & lastInput )
    {
        bool isBack = lastInput.isKeyPressed( SDL_SCANCODE_S ) || lastInput.isKeyPressed( SDL_SCANCODE_DOWN );

        if ( lastInput.controller )
            isBack |= lastInput.controller->leftShoulder > 0.f;

        return isBack;
    }

    bool SpaceshipController::getLeftInput( const UiInput & lastInput )
    {
        return lastInput.isKeyPressed( SDL_SCANCODE_A ) || lastInput.isKeyPressed( SDL_SCANCODE_LEFT );
    }

    bool SpaceshipController::getRightInput( const UiInput & lastInput )
    {
        return lastInput.isKeyPressed( SDL_SCANCODE_D ) || lastInput.isKeyPressed( SDL_SCANCODE_RIGHT );
    }

    glm::vec2 SpaceshipController::getTorque( const UiInput & lastInput )
    {
        glm::vec2 torque {};
        if ( lastInput.mouseRightPressed )
            torque = -glm::vec2 { lastInput.deltaMousePosition.y, lastInput.deltaMousePosition.x };

        if ( lastInput.controller )
        {
            glm::vec2 left  = glm::vec2 { lastInput.controller->left.y, lastInput.controller->left.x };
            glm::vec2 right = glm::vec2 { lastInput.controller->right.y, lastInput.controller->right.x };

            glm::vec2 input {};
            if ( glm::length2( left ) > glm::length2( right ) )
                input = left;
            else
                input = right;

            torque = -input * 10.f;
            torque.x *= .5f;
        }

        return torque;
    }

} // namespace udk
