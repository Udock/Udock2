#include "Udock/Ui/Window/HelpWindow.hpp"
#include <fmt/format.h>

namespace udk
{
    struct CommandPrinter
    {
        using Command = std::pair<std::string, std::string>;
        std::vector<Command> commands;

        void                     add( std::string name, std::string input ) { commands.emplace_back( name, input ); }
        std::vector<std::string> getAlignedCommands() const
        {
            std::size_t maximumWidth = 0;
            for ( const auto & [ name, input ] : commands )
                maximumWidth = std::max( maximumWidth, name.size() );

            std::vector<std::string> commandsTxt {};
            commandsTxt.reserve( commands.size() );
            for ( const auto & [ name, input ] : commands )
                commandsTxt.emplace_back(
                    fmt::format( "{}: {:>{}}", name, input, ( maximumWidth - name.size() ) + input.size() ) );

            return commandsTxt;
        }
    };

    HelpWindow::HelpWindow( const glm::vec2 & padding ) :
        FloatingWindow( "Help",
                        padding + glm::vec2 { 0.3f, 0.15f },
                        glm::vec2 { 0.6f, .85f } - padding * 2.f,
                        glm::vec4( 0., 0., 0., .9 ),
                        ImGuiWindowFlags_NoResize | ImGuiWindowFlags_AlwaysUseWindowPadding,
                        true,
                        true )
    {
        WindowComponentList controlsList { "" };

        const auto appendTextComponents = []( const std::vector<std::string> & strings, WindowComponentList & list )
        {
            for ( const std::string & str : strings )
                list.addComponent( Text( str ) );
        };

        CommandPrinter printer {};

        WindowComponentList cameraControls { "-- Camera docking --" };
        printer = {};
        printer.add( "Front", "W" );
        printer.add( "Left", "A" );
        printer.add( "Right", "S" );
        printer.add( "Back", "D" );
        printer.add( "Rotate", "Right click / Left and right stick" );
        printer.add( "Rotate around", "Left shift + Right click" );
        printer.add( "Display target", "T key" );
        appendTextComponents( printer.getAlignedCommands(), cameraControls );
        controlsList.addComponent( std::move( cameraControls ) );

        WindowComponentList conformationControls { "-- Exploring conformations --" };
        printer = {};
        printer.add( "Add new anchor", "Ctrl + Left click on molecule" );
        printer.add( "Add new line constraint", "Add anchors on two different molecules" );
        printer.add( "Apply constraints", "Space" );
        appendTextComponents( printer.getAlignedCommands(), conformationControls );
        controlsList.addComponent( std::move( conformationControls ) );

        WindowComponentList sceneControls { "-- Playing with the spaceship --" };
        printer = {};
        printer.add( "Change environment", "Tab" );
        printer.add( "Front", "W / Game controller right shoulder" );
        printer.add( "Left", "A" );
        printer.add( "Right", "S" );
        printer.add( "Back", "D / Game controller left shoulder" );
        printer.add( "Throw", "Left click / A key / Game controller A" );
        printer.add( "Bullet tool", "1 key" );
        printer.add( "Anchor tool", "2 key" );
        printer.add( "Change tool", "Game Controller B" );
        appendTextComponents( printer.getAlignedCommands(), sceneControls );
        controlsList.addComponent( std::move( sceneControls ) );

        WindowComponentList additionalControls {
            "",
            { Text( "Detailed information and video tutorial can be found under the project repository." ),
              Horizontal( "",
                          { Text( "Repository URL: https://udock.fr" ),
                            Button( "Copy", [] { ImGui::SetClipboardText( "https://gitlab.com/Udock/Udock2" ); } ) } ) }
        };
        controlsList.addComponent( std::move( additionalControls ) );

        addElement( controlsList );
    }
} // namespace udk
