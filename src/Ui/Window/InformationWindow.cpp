#include "Udock/Ui/Window/InformationWindow.hpp"
#include "Udock/Physics/Energy.hpp"
#include "Udock/Physics/PhysicalWorld.hpp"
#include "Udock/System/System.hpp"
#include "Udock/Utils/Io.hpp"
#include "Udock/Utils/Logger.hpp"
#include <portable-file-dialogs.h>

namespace udk
{
    InformationWindow::InformationWindow( UdockSettings *    settings,
                                          System *           system,
                                          PhysicalWorld *    physicalWorld,
                                          RemoveItemCallback removeItemCallback,
                                          ClearItemsCallback clearItemsCallback,
                                          glm::vec2          padding ) :
        FloatingWindow( "General",
                        padding,
                        glm::vec2 { 0.2f - padding.x * 2.f, 1.f - padding.y * 2.f },
                        glm::vec4( .5 ),
                        ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove
                            | ImGuiWindowFlags_AlwaysUseWindowPadding | ImGuiWindowFlags_AlwaysAutoResize,
                        false,
                        false ),
        m_settings( settings ), m_system( system ), m_physicalWorld( physicalWorld ),
        m_removeItemCallback( std::move( removeItemCallback ) ), m_clearItemsCallback( std::move( clearItemsCallback ) )
    {
        entt::registry & registry     = m_system->getRegistry();
        const auto       moleculeView = registry.view<Molecule>();
        for ( entt::entity entity : moleculeView )
        {
            const Molecule & molecule = registry.get<Molecule>( entity );
            m_displayedMolecule.emplace_back( entity, molecule.getName() );
        }

        m_moleculeListener = Listener<Molecule>( //
            m_system,
            [ this ]( Entity entity, Event eventType )
            {
                if ( eventType == Event::Construct )
                    onNewMolecule( entity, entity.get<Molecule>() );
                else if ( eventType == Event::Destroy )
                    onMoleculeDestroyed( entity );
            } );
        m_moleculeListener.subscribe( Event::Construct );
        m_moleculeListener.subscribe( Event::Destroy );

        WindowComponentList moleculeList { "" };
        moleculeList.addComponent( ListBox(
            "Molecules",
            [ this ]( std::size_t idx ) -> std::optional<std::string_view>
            {
                if ( idx < m_displayedMolecule.size() )
                    return m_displayedMolecule[ idx ].second;
                return {};
            },
            [ this ]( std::size_t selectedIdx ) { m_selectedMolecule = selectedIdx; } ) );
        moleculeList.addComponent( Button( "Remove selected",
                                           [ this ]()
                                           {
                                               if ( m_selectedMolecule < m_displayedMolecule.size() )
                                                   m_removeItemCallback(
                                                       m_displayedMolecule[ m_selectedMolecule ].first );
                                           } ) );
        moleculeList.addComponent( Button( "Clear constraints", [ this ]() { m_physicalWorld->clearConstraints(); } ) );
        moleculeList.addComponent( Button( "Clear scene", [ this ]() { m_clearItemsCallback(); } ) );
        addElement( Padding( glm::vec2 { 0.01f }, std::move( moleculeList ) ) );

        WindowComponentList optimization { "" };
        optimization.addComponent( Text( "Freeze molecules" ) );
        optimization.addComponent( //
            Horizontal( "",
                        { Checkbox( "##FreezeStatus",
                                    m_physicalWorld->getFreezeStatus(),
                                    [ & ]( bool value )
                                    {
                                        if ( m_optimizing )
                                        {
                                            return m_physicalWorld->getFreezeStatus();
                                        }

                                        m_physicalWorld->setFreeze( value );
                                        return value;
                                    } ),
                          ColoredText( "",
                                       [ this ]( std::string & label, glm::vec4 & color )
                                       {
                                           label = ( m_physicalWorld->getFreezeStatus() ? "Frozen" : "Free" );
                                           color = m_physicalWorld->getFreezeStatus()
                                                       ? glm::vec4 { 1.f, 0.f, 0.f, 1.f }
                                                       : glm::vec4 { 0.f, 1.f, 0.5f, 1.f };
                                       } ) } ) );
        optimization.addComponent( Text( "Optimization steps" ) );
        optimization.addComponent( Slider<std::size_t>( "##Optimizationsteps",
                                                        m_optimizingSteps,
                                                        { 100, 10000 },
                                                        [ & ]( std::size_t value )
                                                        {
                                                            m_optimizingSteps = value;
                                                            return value;
                                                        } ) );
        optimization.addComponent( Horizontal( "",
                                               { //
                                                 Button( "Optimize",
                                                         [ this ]
                                                         {
                                                             if ( !m_optimizing && m_displayedMolecule.size() > 1 )
                                                             {
                                                                 m_optimizing = true;
                                                                 m_physicalWorld->setFreeze( true );
                                                                 m_optimizationThread
                                                                     = std::thread( [ & ]() { optimize(); } );
                                                                 m_optimizationThread.detach();
                                                             }
                                                         } ) } ) );
        optimization.addComponent( //
            Conditional( m_optimizing,
                         ProgressBar( "Optimization progress",
                                      [ this ]() -> float {
                                          return static_cast<float>( m_currentOptimizingStep.load() )
                                                 / static_cast<float>( m_optimizingSteps );
                                      } ) ) );
        optimization.addComponent( //
            Horizontal( "",
                        { Conditional( m_optimizing, Button( "Stop", [ this ]() { m_optimizing = false; } ) ),
                          ColoredText( "",
                                       [ this ]( std::string & label, glm::vec4 & color )
                                       {
                                           if ( m_optimizing && !m_displayedMolecule.empty() )
                                           {
                                               label = fmt::format( "Optimizing {}",
                                                                    m_displayedMolecule[ m_selectedMolecule ].second );
                                               color = glm::vec4 { 0.f, 1.f, 0.5f, 1.f };
                                           }
                                           else
                                           {
                                               label = "Idle";
                                               color = glm::vec4 { 0.f, 0.f, 1.f, 1.f };
                                           }
                                       } ) } ) );
        optimization.addComponent( Text( "",
                                         [ this ]( std::string & label )
                                         {
                                             std::optional<float> optimizedValue = m_optimizedValue.load();
                                             if ( optimizedValue )
                                                 label = fmt::format( "Last optimized value: {:.4f}", *optimizedValue );
                                             else
                                                 label = "No optimization result yet";
                                         } ) );
        addElement( Padding( glm::vec2 { 0.01f }, std::move( optimization ) ) );

        WindowComponentList save { "Save" };
        save.addComponent( Button( "Save all", [ this ] { saveScene(); } ) );
        save.addComponent( Button( "Save as", [ this ] { saveAs(); } ) );
        addElement( Padding( glm::vec2 { 0.01f }, std::move( save ) ) );
    }

    InformationWindow::~InformationWindow()
    {
        // Handle detached optimizing thread
        if ( m_optimizing )
        {
            // notify closing
            m_optimizingSteps = 0;

            // Wait for optimizing thread completion
            while ( m_optimizing ) {}
        }
    }

    void InformationWindow::optimize()
    {
        m_currentOptimizingStep = 0;

        if ( m_displayedMolecule.size() < 2 )
        {
            m_optimizedValue = std::optional<float> {};
            m_optimizing     = false;
            return;
        }

        EnergyComputationData energyData {};
        entt::registry &      registry = m_system->getRegistry();
        for ( int32_t i = int32_t( m_displayedMolecule.size() ) - 1; i != static_cast<std::size_t>( -1 ); i-- )
        {
            const auto & [ molecule, transform ] = registry.get<Molecule, Transform>( m_displayedMolecule[ i ].first );
            energyData.entities.emplace_back(
                EnergyComputationEntity { molecule.getAtoms(), molecule.getAccelerationStructure(), transform.get() } );
        }

        float lastDelta  = -std::numeric_limits<float>::max();
        float bestEnergy = std::numeric_limits<float>::max();
        m_optimizedValue = bestEnergy;

        glm::mat4 bestTransform { 1.f };
        for ( std::size_t i = 0; i < m_optimizingSteps && m_optimizing; i++ )
        {
            m_currentOptimizingStep = i;

            const float stepSize = m_settings->optimizationStepSize * static_cast<float>( m_optimizingSteps - i )
                                   / static_cast<float>( m_optimizingSteps );

            energyData.maxDistance = m_settings->energyMaxRadius;

            std::size_t  id        = m_displayedMolecule.size() - 1 - m_selectedMolecule;
            entt::entity optimized = m_displayedMolecule[ m_selectedMolecule ].first;
            optimizationStep( id, lennardJonesCoulombic, stepSize, energyData, lastDelta, bestTransform, bestEnergy );

            if ( lastDelta < 0.f )
            {
                auto & currentTransform = registry.get<Transform>( optimized );
                currentTransform.set( energyData.entities[ id ].transform );

                m_optimizedValue = bestEnergy;
            }
        }

        m_optimizedValue = bestEnergy;
        m_optimizing     = false;
    }

    void InformationWindow::onNewMolecule( entt::entity entity, const Molecule & molecule )
    {
        m_optimizedValue = std::optional<float> {};
        m_displayedMolecule.emplace_back( std::make_pair( entity, molecule.getName() ) );
    }

    void InformationWindow::onMoleculeDestroyed( entt::entity entity )
    {
        m_optimizedValue = std::optional<float> {};
        for ( std::size_t i = 0; i < m_displayedMolecule.size(); )
        {
            if ( m_displayedMolecule[ i ].first == entity )
            {
                m_displayedMolecule.erase( m_displayedMolecule.begin() + i );
                break;
            }
            i++;
        }
    }

    void InformationWindow::saveScene() const
    {
        if ( !m_optimizing )
        {
            pfd::select_folder folderDialog { "Choose folder to write the scene into.", "./" };
            const std::string  result = folderDialog.result();
            if ( !result.empty() )
            {
                const Path outputFolder { result };

                std::unordered_map<std::string_view, std::size_t> moleculeUsedName {};

                entt::registry & registry     = m_system->getRegistry();
                auto             moleculeView = registry.view<Molecule, RigidBody>();
                for ( const entt::entity entity : moleculeView )
                {
                    const auto & [ molecule, transform ] = registry.get<Molecule, Transform>( entity );

                    const std::string_view originalMoleculeName = molecule.getName();

                    std::string moleculeName = std::string( originalMoleculeName );
                    if ( moleculeUsedName.find( originalMoleculeName ) != moleculeUsedName.end() )
                    {
                        moleculeName += std::to_string( moleculeUsedName[ originalMoleculeName ] );
                    }
                    else
                    {
                        moleculeUsedName[ originalMoleculeName ] = 0;
                    }

                    writeMolecule( result / Path { moleculeName + ".mol2" }, molecule, transform.get() );
                    moleculeUsedName[ originalMoleculeName ] += 1;
                }
            }
        }
    }

    void InformationWindow::saveAs() const
    {
        if ( !m_optimizing )
        {
            entt::registry & registry     = m_system->getRegistry();
            auto             moleculeView = registry.view<Molecule, RigidBody>();
            for ( const entt::entity entity : moleculeView )
            {
                const auto & [ molecule, transform ] = registry.get<Molecule, Transform>( entity );
                const std::string_view molName       = molecule.getName();
                pfd::save_file         fileDialog { fmt::format( "Choose files to write to molecule {}", molName ),
                                            fmt::format( "{}.mol2", molName ),
                                                    { "Mol2 files (.mol2)", "*.Mol2" },
                                            pfd::opt::none };

                const std::string result = fileDialog.result();
                if ( !result.empty() )
                {
                    writeMolecule( result, molecule, transform.get() );
                }
            }
        }
    }

} // namespace udk
