#include "Udock/Ui/Window/EnergyWindow.hpp"
#include "Udock/Utils/Logger.hpp"
#include <fmt/format.h>

namespace udk
{
    EnergyWindow::EnergyWindow( UdockSettings * settings, System * system, PhysicalWorld * world, glm::vec2 padding ) :
        FloatingWindow( "Energy",
                        padding + glm::vec2 { 0.3f, 0.f },
                        glm::vec2 { 0.6f, 0.15f } - 2.f * padding.x,
                        glm::vec4( 0.f, 0.f, 0.f, 0.f ),
                        ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove,
                        false,
                        false ),
        m_settings( settings ), m_system( system ), m_world( world )
    {
        addElement( WindowComponentList(
            "",
            { Horizontal( "",
                          { Text( "Score",
                                  [ this ]( std::string & label )
                                  { label = fmt::format( "Score: {:.2f}  -", m_energyScore.load() ); } ),
                            ColoredText( "MinScore",
                                         [ this ]( std::string & label, glm::vec4 & color )
                                         {
                                             if ( m_improveIndicatorLife > 0 )
                                             {
                                                 m_improveIndicatorLife--;
                                                 color = glm::vec4 { 0.f, 1.f, 0.5f, 1.f };
                                             }
                                             else
                                             {
                                                 color = glm::vec4 { 1.f };
                                             }
                                             label = fmt::format( " Best score: {:.2f}", m_minScore );
                                         } ) } ),
              FifoLinePlotter(
                  "",
                  [ this ] { return getLastEnergyScore(); },
                  256,
                  glm::vec2 { 0.6f - 2.f * padding.x, 0.3f - padding.y * 4.f },
                  { 0.f, std::numeric_limits<float>::max() },
                  [ this ] { return m_needsUpdate; } ) },
            false ) );

        m_moleculeListener = Listener<Molecule>(
            m_system, [ this ]( Entity /* entity */, Event /* eventType */ ) { updateMoleculeData(); } );
        m_moleculeListener.subscribe( Event::Construct );
        m_moleculeListener.subscribe( Event::Destroy );

        const entt::registry & registry  = m_system->getRegistry();
        const auto             molecules = registry.view<Molecule>();
        m_energyData.entities.reserve( molecules.size() );
        for ( const entt::entity current : molecules )
        {
            const auto & [ currentMolecule, transform ] = registry.get<Molecule, Transform>( current );
            m_energyData.entities.emplace_back( EnergyComputationEntity {
                currentMolecule.getAtoms(), currentMolecule.getAccelerationStructure(), transform.get() } );
        }

        m_energyThread = std::thread(
            [ this ]()
            {
                bool                  initialized = false;
                EnergyComputationData currentData;
                while ( m_mustComputeEnergy )
                {
                    if ( m_fullUpdate || !initialized )
                    {
                        currentData   = m_energyData;
                        m_fullUpdate  = false;
                        initialized   = true;
                        m_energyScore = 0.f;
                    }

                    if ( m_energyUpdate && currentData.entities.size() == m_energyData.entities.size() )
                    {
                        for ( std::size_t i = 0; i < m_energyData.entities.size(); i++ )
                            currentData.entities[ i ].transform = m_energyData.entities[ i ].transform;

                        currentData.maxDistance = m_energyData.maxDistance;
                        m_energyUpdate          = false;

                        m_energyScore = computeEnergy( currentData );
                    }
                }
            } );
    }

    EnergyWindow::~EnergyWindow()
    {
        m_mustComputeEnergy = false;
        if ( m_energyThread.joinable() )
            m_energyThread.join();
    }

    float EnergyWindow::getLastEnergyScore()
    {
        const bool isUpdating = m_energyUpdate || m_fullUpdate;
        const bool updatable  = !isUpdating && m_system->size() > 0;
        if ( m_needsUpdate )
        {
            m_minScore    = std::numeric_limits<float>::max();
            m_energyScore = 0.f;
        }

        if ( m_needsUpdate && updatable )
        {
            const entt::registry & registry  = m_system->getRegistry();
            const auto             molecules = registry.view<Molecule>();

            m_energyData.entities.clear();
            m_energyData.entities.reserve( molecules.size() );
            for ( const entt::entity current : molecules )
            {
                const auto & [ currentMolecule, body, transform ]
                    = registry.get<Molecule, RigidBody, Transform>( current );
                m_energyData.entities.emplace_back( EnergyComputationEntity {
                    currentMolecule.getAtoms(), currentMolecule.getAccelerationStructure(), transform.get() } );
            }

            m_fullUpdate  = true;
            m_needsUpdate = false;
        }
        else if ( updatable )
        {
            const entt::registry & registry  = m_system->getRegistry();
            const auto             molecules = registry.view<Molecule>();

            for ( std::size_t i = 0; i < molecules.size(); i++ )
                m_energyData.entities[ i ].transform = registry.get<Transform>( molecules[ i ] ).get();
        }

        m_energyData.maxDistance = m_settings->energyMaxRadius;
        m_energyUpdate           = true;

        const float value = m_energyScore.load();
        if ( m_minScore
             == std::numeric_limits<float>::max() ) // Initialization, comparison is ok since it the exact value
        {
            m_minScore = value;
        }
        else if ( value < m_minScore )
        {
            m_minScore             = value;
            m_improveIndicatorLife = ImproveIndicatorLife;
        }

        return value;
    }

    void EnergyWindow::updateMoleculeData() { m_needsUpdate = true; }

} // namespace udk
