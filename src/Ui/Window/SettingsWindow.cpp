#include "Udock/Ui/Window/SettingsWindow.hpp"

namespace udk
{
    SettingsWindow::SettingsWindow( UdockSettings * settings, const glm::vec2 & padding ) :
        FloatingWindow( "Settings",
                        padding + glm::vec2 { 0.3f, 0.15f },
                        glm::vec2 { 0.6f, .85f } - padding * 2.f,
                        glm::vec4( 0., 0., 0., .9 ),
                        ImGuiWindowFlags_NoResize | ImGuiWindowFlags_AlwaysUseWindowPadding,
                        true,
                        true ),
        m_settings( settings ), m_isRealisticSky( m_settings->skyboxType == SkyboxType::Realistic ),
        m_isFlatBackground( m_settings->skyboxType == SkyboxType::Flat )
    {
        WindowComponentList settingsList { "" };
        settingsList.addComponent( WindowComponentList( "Target settings",
                                                        {
                                                            Checkbox( "Display camera target",
                                                                      m_settings->displayTarget,
                                                                      [ & ]( bool value )
                                                                      {
                                                                          m_settings->displayTarget = value;
                                                                          return value;
                                                                      } ),
                                                            Slider<float>( "Target size",
                                                                           m_settings->targetSize,
                                                                           { 0.2f, 2.f },
                                                                           [ & ]( float value )
                                                                           {
                                                                               m_settings->targetSize = value;
                                                                               return value;
                                                                           } ),
                                                        } ) );

        settingsList.addComponent( WindowComponentList(
            "Docking settings",
            { Slider<float>( "Camera translation speed",
                             m_settings->cameraControllerSettings.translationSpeed,
                             { 100.f, 200.f },
                             [ & ]( float value )
                             {
                                 m_settings->cameraControllerSettings.translationSpeed = value;
                                 return value;
                             } ),
              Slider<float>( "Camera translation factor speed",
                             m_settings->cameraControllerSettings.translationFactorSpeed,
                             { 1.f, 4.f },
                             [ & ]( float value )
                             {
                                 m_settings->cameraControllerSettings.translationFactorSpeed = value;
                                 return value;
                             } ),
              Slider<float>( "Camera rotation speed",
                             m_settings->cameraControllerSettings.rotationSpeed,
                             { 1e-3f, 1e-2f },
                             [ & ]( float value )
                             {
                                 m_settings->cameraControllerSettings.rotationSpeed = value;
                                 return value;
                             } ),
              Slider<float>( "Camera elasticity factor",
                             m_settings->cameraControllerSettings.elasticityFactor,
                             { 10.f, 30.f },
                             [ & ]( float value )
                             {
                                 m_settings->cameraControllerSettings.elasticityFactor = value;
                                 return value;
                             } ),
              Slider<float>( "Camera elasticity threshold",
                             m_settings->cameraControllerSettings.elasticityThreshold,
                             { 1e-5f, 1e-3f },
                             [ & ]( float value )
                             {
                                 m_settings->cameraControllerSettings.elasticityThreshold = value;
                                 return value;
                             } ),
              Slider<float>( "Entity angular velocity",
                             m_settings->entityControllerSettings.entityAngularVelocity,
                             { 4e2f, 4e4f },
                             [ & ]( float value )
                             {
                                 m_settings->entityControllerSettings.entityAngularVelocity = value;
                                 return value;
                             } ),
              Slider<float>( "Angular decay speed",
                             m_settings->entityControllerSettings.userInputAngularDecaySpeed,
                             { 50.f, 150.f },
                             [ & ]( float value )
                             {
                                 m_settings->entityControllerSettings.userInputAngularDecaySpeed = value;
                                 return value;
                             } ) } ) );

        settingsList.addComponent( WindowComponentList {
            "Spaceship settings",
            { Slider<float>( "Bullet radius",
                             m_settings->spaceshipSettings.bulletRadius,
                             { 1.f, 10.f },
                             [ & ]( float value )
                             {
                                 m_settings->spaceshipSettings.bulletRadius = value;
                                 return value;
                             } ),
              Slider<float>( "Tool anchor bullet strength",
                             m_settings->spaceshipSettings.toolAnchorBulletStrength,
                             { 2e4f, 5e4f },
                             [ & ]( float value )
                             {
                                 m_settings->spaceshipSettings.toolAnchorBulletStrength = value;
                                 return value;
                             } ),
              Slider<float>( "Tool collision bullet strength",
                             m_settings->spaceshipSettings.toolCollisionBulletStrength,
                             { 5e4f, 5e5f },
                             [ & ]( float value )
                             {
                                 m_settings->spaceshipSettings.toolCollisionBulletStrength = value;
                                 return value;
                             } ),
              Slider<float>( "Camera input Strength",
                             m_settings->spaceshipSettings.cameraInputStrength * 1e-4f,
                             { 0.f, 8.f },
                             [ & ]( float value )
                             {
                                 m_settings->spaceshipSettings.cameraInputStrength = value * 1e4f;
                                 return value;
                             } ),
              Slider<float>( "Game controller input Strength",
                             m_settings->spaceshipSettings.gameControllerCameraInputStrength,
                             { 1e3f, 6e4f },
                             [ & ]( float value )
                             {
                                 m_settings->spaceshipSettings.gameControllerCameraInputStrength = value;
                                 return value;
                             } ),
              Slider<float>( "Torque strength reducer",
                             m_settings->spaceshipSettings.torqueInputStrengthReducer,
                             { 0.f, 1.f },
                             [ & ]( float value )
                             {
                                 m_settings->spaceshipSettings.torqueInputStrengthReducer = value;
                                 return value;
                             } ) } } );

        settingsList.addComponent( WindowComponentList(
            "Molecular surface settings",
            { RgbaColorPicker(
                  "Positive charge",
                  [ this ]( const glm::vec4 & color ) { m_settings->moleculeSurfaceColors.positiveCharge = color; },
                  m_settings->moleculeSurfaceColors.positiveCharge ),
              RgbaColorPicker(
                  "Neutral charge",
                  [ this ]( const glm::vec4 & color ) { m_settings->moleculeSurfaceColors.neutralCharge = color; },
                  m_settings->moleculeSurfaceColors.neutralCharge ),
              RgbaColorPicker(
                  "Negative charge",
                  [ this ]( const glm::vec4 & color ) { m_settings->moleculeSurfaceColors.negativeCharge = color; },
                  m_settings->moleculeSurfaceColors.negativeCharge ),
              Slider<float>( "Contrast",
                             m_settings->moleculeSurfaceColors.contrast,
                             { 1.f, 10.f },
                             [ this ]( float value )
                             {
                                 m_settings->moleculeSurfaceColors.contrast = value;
                                 return value;
                             } ) } ) );

        settingsList.addComponent(
            WindowComponentList( "Constraints settings",
                                 { RgbaColorPicker(
                                       "Constraints color",
                                       [ this ]( const glm::vec4 & color ) { m_settings->constraintColor = color; },
                                       m_settings->constraintColor ),
                                   Slider<float>( "Docking speed",
                                                  m_settings->dockingSpeed,
                                                  { 2.f, 100.f },
                                                  [ this ]( float value )
                                                  {
                                                      m_settings->dockingSpeed = value;
                                                      return value;
                                                  } ) } ) );

        settingsList.addComponent( WindowComponentList( "Shading settings",
                                                        {
                                                            Checkbox( "Enable shading with environment",
                                                                      m_settings->enableShadingWithEnvironment,
                                                                      [ & ]( bool value )
                                                                      {
                                                                          m_settings->enableShadingWithEnvironment
                                                                              = value;
                                                                          return value;
                                                                      } ),

                                                        } ) );

        settingsList.addComponent( WindowComponentList( "Energy computation settings",
                                                        { Slider<float>( "Max distance",
                                                                         m_settings->energyMaxRadius,
                                                                         { 1.f, 50.f },
                                                                         [ this ]( float value )
                                                                         {
                                                                             m_settings->energyMaxRadius = value;
                                                                             return value;
                                                                         } ) } ) );

        std::size_t selected = 0;
        switch ( m_settings->skyboxType )
        {
        case SkyboxType::Space: selected = 1; break;
        case SkyboxType::Flat: selected = 2; break;
        default: break;
        }

        settingsList.addComponent( WindowComponentList(
            "Skybox Settings",
            { ComboBox(
                  "Environment",
                  [ this ]( std::size_t idx ) -> std::optional<std::string_view>
                  {
                      switch ( idx )
                      {
                      case 0: return "Realistic sky";
                      case 1: return "Space";
                      case 2: return "Flat";
                      default: return {};
                      }
                  },
                  [ this ]( std::size_t selectedIdx )
                  {
                      switch ( selectedIdx )
                      {
                      case 0: m_settings->skyboxType = SkyboxType::Realistic; break;
                      case 1: m_settings->skyboxType = SkyboxType::Space; break;
                      case 2: m_settings->skyboxType = SkyboxType::Flat; break;
                      }

                      m_isRealisticSky   = m_settings->skyboxType == SkyboxType::Realistic;
                      m_isFlatBackground = m_settings->skyboxType == SkyboxType::Flat;
                  },
                  selected ),
              Conditional( m_isRealisticSky,
                           WindowComponentList( "Hosek et al. sky settings",
                                                { Slider<float>( "Turbidity",
                                                                 m_settings->turbidity,
                                                                 { 1.f, 10.f },
                                                                 [ this ]( float value )
                                                                 {
                                                                     m_settings->turbidity = value;
                                                                     return value;
                                                                 } ),
                                                  Slider<float>( "Ground albedo",
                                                                 m_settings->groundAlbedo,
                                                                 { 0.f, 1.f },
                                                                 [ this ]( float value )
                                                                 {
                                                                     m_settings->groundAlbedo = value;
                                                                     return value;
                                                                 } ) } ) ),
              Conditional( m_isFlatBackground,
                           RgbaColorPicker(
                               "Background color",
                               [ this ]( const glm::vec4 & color ) { m_settings->backgroundColor = color; },
                               m_settings->backgroundColor ) ) } ) );

        addElement( Padding( glm::vec2 { 0.01f }, std::move( settingsList ) ) );
    }
} // namespace udk
