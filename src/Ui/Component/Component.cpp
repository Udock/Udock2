#include "Udock/Ui/Component/Component.hpp"
#include <imgui.h>

namespace udk
{
    Component::Component( std::string name ) : m_name( std::move( name ) ) {}

    Text::Text( std::string name, TextCallback callback, bool wrapped ) :
        Component( std::move( name ) ), m_callback( std::move( callback ) ), m_wrapped( wrapped )
    {
    }

    void Text::operator()()
    {
        if ( m_callback )
            m_callback( m_name );

        if ( m_wrapped )
            ImGui::TextWrapped( "%s", m_name.c_str() );
        else
            ImGui::Text( "%s", m_name.c_str() );
    }

    ColoredText::ColoredText( std::string name, ColoredTextCallback callback, bool wrapped ) :
        Component( std::move( name ) ), m_callback( std::move( callback ) ), m_wrapped( wrapped )
    {
    }

    void ColoredText::operator()()
    {
        if ( m_callback )
            m_callback( m_name, m_color );
        ImGui::PushStyleColor( ImGuiCol_Text, ImVec4( m_color.x, m_color.y, m_color.z, m_color.w ) );
        if ( m_wrapped )
            ImGui::TextWrapped( "%s", m_name.c_str() );
        else
            ImGui::Text( "%s", m_name.c_str() );
        ImGui::PopStyleColor();
    }

    ActionComponent::ActionComponent( std::string name, UiAction action ) :
        Component( std::move( name ) ), m_action( std::move( action ) )
    {
    }

    void ActionComponent::execute() const
    {
        if ( m_action )
        {
            m_action();
        }
    }

    Button::Button( std::string name, UiAction callback ) : ActionComponent( std::move( name ), std::move( callback ) )
    {
    }

    void Button::operator()()
    {
        if ( ImGui::Button( m_name.c_str() ) )
            execute();
    }

} // namespace udk
