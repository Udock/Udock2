#include "Udock/Ui/Component/UiManager.hpp"

namespace udk
{
    void UiManager::addWindow( std::string name, std::unique_ptr<FloatingWindow> component )
    {
        m_windows.emplace( std::move( name ), std::move( component ) );
    }

    void UiManager::removeWindow( const std::string & name ) { m_windows.erase( name ); }

    void UiManager::draw()
    {
        if ( m_menuBar.has_value() )
        {
            ( *m_menuBar )();
        }

        for ( WindowHashMap::iterator window = m_windows.begin(); window != m_windows.end(); )
        {
            ( *window->second )();
            if ( !window->second->isOpen() )
                window = m_windows.erase( window );
            else
                ++window;
        }
    }
} // namespace udk
