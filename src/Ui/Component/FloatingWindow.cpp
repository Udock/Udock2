#include "Udock/Ui/Component/FloatingWindow.hpp"

namespace udk
{
    FloatingWindow::FloatingWindow( std::string      title,
                                    glm::vec2        position,
                                    glm::ivec2       pixelShift,
                                    glm::vec2        size,
                                    glm::vec4        backgroundColor,
                                    ImGuiWindowFlags flags,
                                    bool             closable,
                                    bool             movable ) :
        Component( std::move( title ) ),
        m_position( std::move( position ) ), m_size( std::move( size ) ), m_pixelShift( pixelShift ),
        m_backgroundColor( std::move( backgroundColor ) ), m_flags( flags ), m_closable( closable ),
        m_movable( movable )
    {
    }
    FloatingWindow::FloatingWindow( std::string      title,
                                    glm::vec2        position,
                                    glm::vec2        size,
                                    glm::vec4        backgroundColor,
                                    ImGuiWindowFlags flags,
                                    bool             closable,
                                    bool             movable ) :
        FloatingWindow( title, position, {}, size, backgroundColor, flags, closable, movable )
    {
    }

    void FloatingWindow::operator()()
    {
        const ImGuiIO & io = ImGui::GetIO();
        ImGui::SetNextWindowPos( ImVec2 { io.DisplaySize.x * m_position.x + static_cast<float>( m_pixelShift.x ),
                                          io.DisplaySize.y * m_position.y + static_cast<float>( m_pixelShift.y ) },
                                 m_movable ? ImGuiCond_FirstUseEver : ImGuiCond_Always );
        ImGui::SetNextWindowSize( ImVec2( io.DisplaySize.x * m_size.x, io.DisplaySize.y * m_size.y ),
                                  ImGuiCond_Always );
        ImGui::PushStyleColor(
            ImGuiCol_WindowBg,
            ImVec4 { m_backgroundColor.x, m_backgroundColor.y, m_backgroundColor.z, m_backgroundColor.w } );

        bool * windowState = nullptr;
        if ( m_closable )
            windowState = &m_open;

        if ( ImGui::Begin( m_name.c_str(), windowState, m_flags ) )
        {
            for ( WindowComponent & windowElement : m_windowElements )
            {
                windowElement();
            }
        }
        ImGui::End();
        ImGui::PopStyleColor(); // for transparency
    }
} // namespace udk
