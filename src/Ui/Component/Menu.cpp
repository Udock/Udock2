#include "Udock/Ui/Component/Menu.hpp"
#include <imgui.h>

namespace udk
{
    MenuItem::MenuItem( std::string name, UiAction action ) : ActionComponent( std::move( name ), std::move( action ) )
    {
    }

    void MenuItem::operator()()
    {
        if ( ImGui::MenuItem( m_name.c_str() ) )
        {
            execute();
        }
    }

    MenuField::MenuField( std::string name ) : Component( std::move( name ) ) {}

    void MenuField::addItem( MenuItem item ) { m_items.emplace_back( std::move( item ) ); }

    void MenuField::operator()()
    {
        if ( ImGui::BeginMenu( m_name.c_str() ) )
        {
            for ( MenuItem & item : m_items )
            {
                item();
            }

            ImGui::EndMenu();
        }
    }

    MenuBar::MenuBar( glm::ivec2 padding ) : Component( "" ), m_padding( std::move( padding ) ) {}

    void MenuBar::operator()()
    {
        ImGui::PushStyleVar( ImGuiStyleVar_WindowPadding,
                             ImVec2( static_cast<float>( m_padding.x ), static_cast<float>( m_padding.y ) ) );

        if ( ImGui::BeginMainMenuBar() )
        {
            for ( std::function<void()> & field : m_fields )
            {
                field();
            }
            ImGui::EndMainMenuBar();
        }
        ImGui::PopStyleVar();
    }

} // namespace udk
