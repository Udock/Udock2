#include "Udock/Ui/Component/WindowComponent.hpp"
#include "Udock/Renderer/Texture.hpp"
#include <imgui.h>

namespace udk
{
    Checkbox::Checkbox( std::string name, bool value, ValueChangedCallback<bool> valueChangedCallback ) :
        ValueComponent<bool>( std::move( name ), value, std::move( valueChangedCallback ) )
    {
    }

    void Checkbox::operator()()
    {
        m_value = m_valueChangedCallback( m_value );
        ImGui::Checkbox( m_name.c_str(), &m_value );
    }

    Padding::Padding( glm::vec2 padding, WindowComponent element ) :
        Component( "" ), m_padding( std::move( padding ) ), m_element( std::move( element ) )
    {
    }

    void Padding::operator()()
    {
        const ImGuiIO & io     = ImGui::GetIO();
        const ImVec2    cursor = ImGui::GetCursorPos();
        const glm::vec2 newCursorPos
            = glm::vec2 { cursor.x, cursor.y } + glm::vec2 { io.DisplaySize.x, io.DisplaySize.y } * m_padding;
        ImGui::SetCursorPos( ImVec2 { newCursorPos.x, newCursorPos.y } );
        m_element();
    }

    WindowComponentList::WindowComponentList( std::string                  name,
                                              std::vector<WindowComponent> elements,
                                              bool                         separator ) :
        Component( std::move( name ) ),
        m_elements( std::move( elements ) ), m_separator( separator )
    {
    }

    void WindowComponentList::operator()()
    {
        ImGui::BeginGroup();
        if ( !m_name.empty() )
            ImGui::Text( "%s", m_name.c_str() );

        for ( WindowComponent & element : m_elements )
        {
            element();
        }
        ImGui::EndGroup();

        if ( m_separator )
            ImGui::Separator();
    }

    ListBox::ListBox( std::string name, DisplayCallback displayCallback, SelectedCallback selectedCallback ) :
        Component( std::move( name ) ), m_displayCallback( std::move( displayCallback ) ),
        m_selectedCallback( std::move( selectedCallback ) )
    {
    }

    void ListBox::operator()()
    {
        ImGui::BeginGroup();

        ImGui::Text( "%s", m_name.c_str() );
        if ( ImGui::BeginListBox( ( "##" + m_name ).c_str() ) )
        {
            std::size_t                     idx = 0;
            std::optional<std::string_view> currentName;
            while ( ( currentName = m_displayCallback( idx ) ) )
            {
                const bool        isSelected = m_selectedIdx == idx;
                const std::string currentTag = "##" + std::to_string( idx );
                if ( ImGui::Selectable( ( currentName.value().data() + currentTag ).c_str(), isSelected ) )
                    m_selectedIdx = idx;

                if ( isSelected )
                    ImGui::SetItemDefaultFocus();

                idx++;
            }
            ImGui::EndListBox();

            if ( m_selectedCallback )
                m_selectedCallback( m_selectedIdx );
        }
        ImGui::EndGroup();
    }

    FifoLinePlotter::FifoLinePlotter( std::string             name,
                                      ValueProducer           valueProducer,
                                      std::size_t             displayMaxSize,
                                      glm::vec2               size,
                                      std::pair<float, float> boundaries,
                                      ValueResetter           resetter ) :
        Component( std::move( name ) ),
        m_producer( std::move( valueProducer ) ), m_displayMaxSize( displayMaxSize ),
        m_boundaries( std::move( boundaries ) ), m_size( std::move( size ) ), m_resetter( resetter )
    {
        m_values.reserve( m_displayMaxSize );
    }

    void FifoLinePlotter::operator()()
    {
        if ( m_resetter && m_resetter() )
            m_values.clear();

        const float current  = m_producer();
        const bool  newValue = m_values.empty() || current != m_values.back();
        if ( newValue )
            m_values.emplace_back( current );

        if ( m_boundaries.first < std::numeric_limits<float>::max() )
            m_boundaries.first = std::min( m_values.back(), m_boundaries.first );
        if ( m_boundaries.second < std::numeric_limits<float>::max() )
            m_boundaries.second = std::max( m_values.back(), m_boundaries.second );
        if ( !m_name.empty() )
            ImGui::Text( "%s", m_name.c_str() );

        const ImGuiIO & io       = ImGui::GetIO();
        const glm::vec2 plotSize = glm::vec2 { io.DisplaySize.x, io.DisplaySize.y } * m_size;
        ImGui::PlotLines( ( "##" + m_name ).c_str(),
                          m_values.data(),
                          static_cast<int>( m_values.size() ),
                          0,
                          nullptr,
                          m_boundaries.first,
                          m_boundaries.second,
                          ImVec2( plotSize.x, plotSize.y ) );

        if ( newValue && m_values.size() >= m_displayMaxSize )
        {
            m_values.erase( m_values.begin() );
        }
    }

    RgbaColorPicker::RgbaColorPicker( std::string name, OnColorChange onColorChange, glm::vec4 defaultValue ) :
        Component( std::move( name ) ), m_onColorChange( std::move( onColorChange ) ),
        m_value( std::move( defaultValue ) )
    {
    }

    void RgbaColorPicker::operator()()
    {
        if ( !m_name.empty() )
            ImGui::Text( "%s", m_name.c_str() );
        if ( ImGui::ColorEdit4(
                 ( "##" + m_name ).c_str(), reinterpret_cast<float *>( &m_value ), ImGuiColorEditFlags_NoAlpha ) )
        {
            m_onColorChange( m_value );
        }
    }

    Horizontal::Horizontal( std::string name, std::vector<WindowComponent> elements ) :
        WindowComponentList( std::move( name ), std::move( elements ) )
    {
    }

    void Horizontal::operator()()
    {
        ImGui::BeginGroup();
        if ( !m_name.empty() )
            ImGui::Text( "%s", m_name.c_str() );

        for ( std::size_t i = 0; i < m_elements.size(); i++ )
        {
            m_elements[ i ]();
            if ( i < m_elements.size() - 1 )
                ImGui::SameLine();
        }
        ImGui::EndGroup();
    }

    ProgressBar::ProgressBar( std::string name, UpdateProgression updateProgress ) :
        Component( std::move( name ) ), m_updateProgress( std::move( updateProgress ) )
    {
    }

    void ProgressBar::operator()() { ImGui::ProgressBar( m_updateProgress(), ImVec2( 0.0f, 0.0f ) ); }

    Conditional::Conditional( std::atomic<bool> & condition, WindowComponent component ) :
        Component( "" ), m_condition( condition ), m_component( std::move( component ) )
    {
    }

    void Conditional::operator()()
    {
        if ( m_condition )
            m_component();
    }

    ComboBox::ComboBox( std::string      name,
                        DisplayCallback  displayCallback,
                        SelectedCallback selectedCallback,
                        std::size_t      selectedIdx ) :
        Component( std::move( name ) ),
        m_displayCallback( std::move( displayCallback ) ), m_selectedCallback( std::move( selectedCallback ) ),
        m_selectedIdx( selectedIdx )
    {
    }

    void ComboBox::operator()()
    {
        ImGui::BeginGroup();
        std::vector<std::string_view>   elements {};
        std::optional<std::string_view> currentName;
        std::size_t                     idx = 0;
        while ( ( currentName = m_displayCallback( idx++ ) ) )
        {
            elements.emplace_back( *currentName );
        }

        ImGui::Text( "%s", m_name.c_str() );
        if ( ImGui::BeginCombo( ( "##" + m_name ).c_str(), elements[ m_selectedIdx ].data() ) )
        {
            for ( std::size_t i = 0; i < elements.size(); i++ )
            {
                const bool        isSelected = m_selectedIdx == idx;
                const std::string currentTag = "##" + std::to_string( idx );
                if ( ImGui::Selectable( ( elements[ i ].data() + currentTag ).c_str(), isSelected ) )
                {
                    m_selectedIdx = i;
                    if ( m_selectedCallback )
                        m_selectedCallback( m_selectedIdx );
                }

                if ( isSelected )
                    ImGui::SetItemDefaultFocus();
            }
            ImGui::EndCombo();
        }
        ImGui::EndGroup();
    }

    Image::Image( std::string name, Texture * texture ) : Component( std::move( name ) ), m_texture( texture ) {}

    void Image::operator()()
    {
        const glm::ivec2 & dimensions = m_texture->getDimensions();
        ImGui::Image( reinterpret_cast<void *>( static_cast<intptr_t>( m_texture->getHandle() ) ),
                      ImVec2( static_cast<float>( dimensions.x ), static_cast<float>( dimensions.y ) ) );
    }

} // namespace udk
