#include "Udock/Ui/State/Spaceship.hpp"
#include "Udock/Physics/Constraint.hpp"
#include "Udock/Physics/Projectile.hpp"
#include "Udock/Renderer/Pass/Skybox.hpp"
#include "Udock/Settings.hpp"
#include "Udock/Ui/Component/UiManager.hpp"
#include "Udock/Ui/State/Docking.hpp"
#include "Udock/Utils/Io.hpp"
#include "Udock/Utils/Logger.hpp"

namespace udk
{
    const float SpaceshipSettings::BulletRadiusDefault                      = 3.f;
    const float SpaceshipSettings::ToolAnchorBulletStrengthDefault          = 5e4f;
    const float SpaceshipSettings::ToolCollisionBulletStrengthDefault       = 3e4f * 5.f;
    const float SpaceshipSettings::CameraInputStrengthDefault               = 2.5e4f;
    const float SpaceshipSettings::GameControllerCameraInputStrengthDefault = 1.5e4f;
    const float SpaceshipSettings::TorqueInputStrengthReducerDefault        = 3e-1f;

    SpaceshipState::SpaceshipState( UiManager *     uiManager,
                                    UdockSettings * settings,
                                    System *        system,
                                    PhysicalWorld * world,
                                    Entity          cameraEntity ) :
        State( uiManager, settings, system, world, cameraEntity ),
        m_ship( m_system->create() )
    {
        m_settings->displayTarget = false;
        m_settings->skyboxType    = SkyboxType::Space;

        Mesh raptorShip = udk::readObj( "./resources/Model/RaptorSpaceship/RaptorSpaceship.obj" );

        constexpr glm::vec3 shift = glm::vec3 { 0.f, 0.f, 1.f } * 35.f + glm::vec3 { 0.f, 1.f, 0.f } * 15.f;

        auto & camera          = m_cameraEntity.get<Camera>();
        auto & cameraTransform = m_cameraEntity.get<Transform>();
        camera.lookAt( m_system->getBoundingBox() );

        Transform & shipTransform = m_ship.emplace<Transform>();
        shipTransform.setPosition( cameraTransform.getPosition() );
        shipTransform.setRotation( cameraTransform.getRotation() );

        cameraTransform.setParent( &shipTransform );
        cameraTransform.reset();
        cameraTransform.setPosition( shift );

        glm::vec3 min = glm::vec3 { shift };
        glm::vec3 max = glm::vec3 { shift };
        for ( const auto & v : raptorShip.vertices )
        {
            min = glm::min( min, v );
            max = glm::max( max, v );
        }

        std::unique_ptr<SphereCollider> meshCollider
            = std::make_unique<SphereCollider>( glm::distance( min, max ) * .5f );
        RigidBody & shipBody = m_ship.emplace<RigidBody>( std::move( meshCollider ), ( min + max ) * .5f );
        shipBody.setCollisionCallback(
            [ this ]( RigidBody & current, RigidBody & other, const glm::vec3 & contactPoint )
            {
                entt::registry & registry = m_system->getRegistry();
                Entity           currentEntity;
                for ( const auto entity : registry.view<RigidBody>() )
                {
                    auto & r = registry.get<RigidBody>( entity );
                    if ( &r == &current )
                        currentEntity = Entity( registry, entity );
                }

                const glm::vec3 relative
                    = glm::inverse( currentEntity.get<Transform>().get() ) * glm::vec4( contactPoint, 1.f );

                const SpaceshipSettings & settings = m_settings->spaceshipSettings;
                current.applyForce( -glm::normalize( relative ) * 1e2f, relative );
            } );
        m_ship.emplace<Mesh>( std::move( raptorShip ) );

        m_spaceshipController = SpaceshipController( m_ship );

        m_keyboardController.registerCallback(
            {
                { SDL_SCANCODE_TAB, KeyAction::Up },
            },
            [ this ]( const UiInput & /* uiInput */ )
            {
                m_cameraEntity.get<Transform>().setParent( nullptr );
                m_done = true;
            } );
        m_keyboardController.registerCallback(
            {
                { SDL_SCANCODE_1, KeyAction::Up },
            },
            [ this ]( const UiInput & /* uiInput */ ) { m_isAnchorTool = false; } );
        m_keyboardController.registerCallback(
            {
                { SDL_SCANCODE_2, KeyAction::Up },
            },
            [ this ]( const UiInput & /* uiInput */ ) { m_isAnchorTool = true; } );
        m_keyboardController.registerCallback(
            {
                { SDL_SCANCODE_Q, KeyAction::Down },
            },
            [ this ]( const UiInput & /* uiInput */ ) { fire(); } );
        m_keyboardController.registerCallback( { { SDL_SCANCODE_SPACE, KeyAction::Pressed } },
                                               [ this ]( const UiInput & /* uiInput */ )
                                               { m_world->applyConstraints( m_settings->dockingSpeed ); } );

        m_gameController.registerCallback( { { SDL_CONTROLLER_BUTTON_A, KeyAction::Down } },
                                           [ this ]( const UiInput & /* uiInput */ ) { fire(); } );
        m_gameController.registerCallback( { { SDL_CONTROLLER_BUTTON_B, KeyAction::Down } },
                                           [ this ]( const UiInput & /* uiInput */ )
                                           { m_isAnchorTool = !m_isAnchorTool; } );
        m_gameController.registerCallback( { { SDL_CONTROLLER_BUTTON_X, KeyAction::Pressed } },
                                           [ this ]( const UiInput & /* uiInput */ )
                                           { m_world->applyConstraints( m_settings->dockingSpeed ); } );

        std::unique_ptr<FloatingWindow> spaceshipUi = std::make_unique<FloatingWindow>(
            "Sight",
            glm::vec2 { .5f, 0.6f },
            glm::ivec2 { -32 },
            glm::vec2 { 0.5f },
            glm::vec4 { 0.f },
            ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoInputs | ImGuiWindowFlags_NoMove,
            false,
            false );
        m_sightTexture = Texture( "./resources/Texture/Sight.png" );
        spaceshipUi->addElement( Image( "", &m_sightTexture ) );
        m_uiManager->addWindow( "Sight", std::move( spaceshipUi ) );

        std::unique_ptr<FloatingWindow> indicatorUi = std::make_unique<FloatingWindow>(
            "Indicator",
            glm::vec2 { .5f - .25f, 0.99f },
            glm::ivec2 { 5, -32 },
            glm::vec2 { 0.5f },
            glm::vec4 { 0.f },
            ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoInputs | ImGuiWindowFlags_NoMove,
            false,
            false );
        indicatorUi->addElement( // format
            Text( "",
                  [ this ]( std::string & text )
                  {
                      text = std::string( "Bullet mode: " ) + ( m_isAnchorTool ? "Anchor" : " Push " );

                      // Center label
                      // From: https://github.com/ocornut/imgui/discussions/3862#discussioncomment-422097
                      ImGuiStyle & style = ImGui::GetStyle();

                      const float size  = ImGui::CalcTextSize( text.c_str() ).x + style.FramePadding.x * 2.0f;
                      const float avail = ImGui::GetContentRegionAvail().x;
                      const float off   = ( avail - size ) * .5f;
                      if ( off > 0.0f )
                          ImGui::SetCursorPosX( ImGui::GetCursorPosX() + off );
                  } ) );
        m_uiManager->addWindow( "Indicator", std::move( indicatorUi ) );

        m_chrono.start();
    }

    SpaceshipState::~SpaceshipState()
    {
        Entity           tempAnchor = {};
        entt::registry & registry   = m_system->getRegistry();
        for ( entt::entity entity : registry.view<TemporaryAnchor>() )
        {
            tempAnchor = { registry, entity };
            tempAnchor.remove<TemporaryAnchor>();
            break;
        }

        m_ship.destroy();
        m_system->getRegistry().clear<Projectile>();
        m_uiManager->removeWindow( "Sight" );
        m_uiManager->removeWindow( "Indicator" );
    }

    void SpaceshipState::update( const UiInput & lastInput )
    {
        if ( lastInput.mouseLeftClicked )
            fire();

        m_keyboardController.update( lastInput );
        m_gameController.update( lastInput );

        const SpaceshipSettings & settings = m_settings->spaceshipSettings;
        m_spaceshipController.update( lastInput,
                                      settings.cameraInputStrength,
                                      settings.gameControllerCameraInputStrength,
                                      settings.torqueInputStrengthReducer );
    }

    void SpaceshipState::resetCamera()
    {
        const Aabb & worldBB = m_system->getBoundingBox();

        const glm::vec3 target   = ( worldBB.minimum + worldBB.maximum ) * .5f;
        const float     bbRadius = glm::compMax( glm::abs( worldBB.maximum - target ) );
        const float     distance = bbRadius / std::tan( m_cameraEntity.get<Camera>().getFov() * .5f );

        Transform &     shipTransform = m_ship.get<Transform>();
        const glm::vec3 direction     = shipTransform.getDirection();

        shipTransform.setPosition( target - direction * 2.f * distance );
        shipTransform.lookAt( target );
    }

    std::unique_ptr<State> SpaceshipState::next() const
    {
        return std::make_unique<DockingState>( m_uiManager, m_settings, m_system, m_world, m_cameraEntity );
    }

    void SpaceshipState::fire()
    {
        constexpr uint32_t MsBetweenFire = 200;
        ImGuiIO &          io            = ImGui::GetIO();
        if ( io.WantCaptureMouse || m_chrono.currentTime() < MsBetweenFire )
            return;

        m_chrono.start();

        const Camera &    camera          = m_cameraEntity.get<Camera>();
        const Transform & cameraTransform = m_cameraEntity.get<Transform>();

        // Raycast scene from camera to place anchor
        // Reference: https://www.shadertoy.com/view/ltyXWh
        const glm::vec2 px        = glm::vec2 { .5f, 0.4f } * 2.f - 1.f;
        glm::vec3       direction = glm::inverse( camera.getProjectionMatrix() ) * glm::vec4( px, 0.f, 1.f );
        direction                 = glm::inverse( camera.getViewMatrix() ) * glm::vec4( direction, 0.f );
        direction                 = glm::normalize( direction );

        const SpaceshipSettings & settings = m_settings->spaceshipSettings;

        // Since the ship is part of the physical world, make sure the ray starts after it
        constexpr float BulletStartDistance = 20.f;
        const glm::vec3 cameraPosition      = cameraTransform.getPosition();
        const glm::vec3 position            = cameraPosition + BulletStartDistance * direction;
        const Ray       ray { position, direction };

        glm::vec3 hitPosition {};
        Entity    hitEntity = { m_system->getRegistry(), entt::null };
        if ( !m_world->intersect( ray, hitPosition, hitEntity ) || !hitEntity.all_of<Molecule>() )
        {
            hitEntity = { m_system->getRegistry(), entt::null };

            // Even if we hit nothing, we still need the bullet to appear
            constexpr float BulletEndDistance = 100.f;
            hitPosition                       = ray.origin + ray.direction * BulletEndDistance;
        }

        Entity      newProjectile = m_system->create();
        Transform & transform     = newProjectile.emplace<Transform>( position );

        // Increase the radius for collision detection
        RigidBody & body = newProjectile.emplace<RigidBody>(
            std::make_unique<SphereCollider>( settings.bulletRadius * 2.f ), transform );
        body.ignoreCollisionWith( m_ship.get<RigidBody>() );

        constexpr float bulletLifeTimeInSeconds = 2.f;
        Projectile &    projectile              = newProjectile.emplace<Projectile>(
            newProjectile, hitEntity, hitPosition, settings.bulletRadius, bulletLifeTimeInSeconds );

        if ( m_isAnchorTool )
        {
            body.setIgnoreCollision( true );
            projectile.setHitCallback(
                [ this ]( Entity hitEntity, const glm::vec3 & hitPosition )
                {
                    if ( !hitEntity.valid() || !hitEntity.all_of<Transform, Molecule, RigidBody>() )
                        return;

                    entt::registry & registry = m_system->getRegistry();

                    Entity tempAnchor = {};
                    for ( entt::entity entity : registry.view<TemporaryAnchor>() )
                    {
                        tempAnchor = { registry, entity };
                        break;
                    }

                    const auto & transform = hitEntity.get<Transform>();
                    if ( tempAnchor && tempAnchor.entity() != hitEntity.entity() )
                    {
                        Entity firstAnchor = m_system->create();
                        firstAnchor.emplace<Anchor>(
                            tempAnchor,
                            tempAnchor.get<Transform>().get()
                                * glm::vec4( tempAnchor.get<TemporaryAnchor>().relativePosition, 1.f ) );
                        tempAnchor.remove<TemporaryAnchor>();

                        Entity secondAnchor = m_system->create();
                        secondAnchor.emplace<Anchor>( hitEntity, hitPosition );

                        const Entity newConstraint = m_system->create();
                        newConstraint.emplace<LineConstraint>( firstAnchor, secondAnchor );
                    }
                    else
                    {
                        if ( tempAnchor )
                            tempAnchor.remove<TemporaryAnchor>();

                        hitEntity.emplace<TemporaryAnchor>( glm::inverse( transform.get() )
                                                            * glm::vec4( hitPosition, 1.f ) );
                    }
                } );
        }

        projectile.fire( direction,
                         m_isAnchorTool ? settings.toolAnchorBulletStrength : settings.toolCollisionBulletStrength );
    }
} // namespace udk
