#include "Udock/Ui/State/Docking.hpp"
#include "Udock/Settings.hpp"
#include "Udock/Ui/State/Spaceship.hpp"
#include "Udock/Utils/Logger.hpp"

namespace udk
{
    DockingState::DockingState( UiManager *     uiManager,
                                UdockSettings * settings,
                                System *        system,
                                PhysicalWorld * world,
                                Entity          camera ) :
        State( uiManager, settings, system, world, camera ),
        m_entityController( &m_settings->entityControllerSettings, m_system, m_world, m_cameraEntity ),
        m_trackBall( &m_settings->cameraControllerSettings, m_world, m_cameraEntity ),
        m_freeFly( &m_settings->cameraControllerSettings, m_cameraEntity )
    {
        m_settings->skyboxType   = SkyboxType::Realistic;
        m_settings->enableTrails = true;

        m_keyboardController.registerCallback( { { SDL_SCANCODE_T, KeyAction::Up } },
                                               [ this ]( const UiInput & /* uiInput */ )
                                               { m_settings->displayTarget = !m_settings->displayTarget; } );
        m_keyboardController.registerCallback( { { SDL_SCANCODE_SPACE, KeyAction::Pressed } },
                                               [ this ]( const UiInput & /* uiInput */ )
                                               { m_world->applyConstraints( m_settings->dockingSpeed ); } );
        m_keyboardController.registerCallback(
            {
                { SDL_SCANCODE_TAB, KeyAction::Up },
            },
            [ this ]( const UiInput & /* uiInput */ ) { m_done = true; } );

        resetCamera();
    }

    void DockingState::update( const UiInput & lastInput )
    {
        m_keyboardController.update( lastInput );
        m_entityController.update( lastInput );

        static bool wasFreefly = true;
        if ( lastInput.isKeyPressed( SDL_SCANCODE_LSHIFT ) )
        {
            glm::vec3 translation {};
            if ( lastInput.isKeyPressed( SDL_SCANCODE_W ) || lastInput.isKeyPressed( SDL_SCANCODE_UP ) )
                translation.z++;
            if ( lastInput.isKeyPressed( SDL_SCANCODE_S ) || lastInput.isKeyPressed( SDL_SCANCODE_DOWN ) )
                translation.z--;
            if ( lastInput.isKeyPressed( SDL_SCANCODE_A ) || lastInput.isKeyPressed( SDL_SCANCODE_LEFT ) )
                translation.x++;
            if ( lastInput.isKeyPressed( SDL_SCANCODE_D ) || lastInput.isKeyPressed( SDL_SCANCODE_RIGHT ) )
                translation.x--;
            if ( lastInput.isKeyPressed( SDL_SCANCODE_R ) )
                translation.y++;
            if ( lastInput.isKeyPressed( SDL_SCANCODE_F ) )
                translation.y--;

            translation *= m_settings->cameraControllerSettings.translationSpeed;
            translation *= lastInput.deltaTime;

            if ( translation != glm::vec3 {} )
                m_cameraEntity.get<Transform>().move( translation );

            const bool updateTarget = wasFreefly || translation.x != 0.f || translation.y != 0.f;
            wasFreefly              = !m_trackBall.update( lastInput, updateTarget );
        }
        else
        {
            glm::vec3 translation {};

            if ( lastInput.isKeyPressed( SDL_SCANCODE_W ) || lastInput.isKeyPressed( SDL_SCANCODE_UP ) )
                translation.z++;
            if ( lastInput.isKeyPressed( SDL_SCANCODE_S ) || lastInput.isKeyPressed( SDL_SCANCODE_DOWN ) )
                translation.z--;
            if ( lastInput.isKeyPressed( SDL_SCANCODE_A ) || lastInput.isKeyPressed( SDL_SCANCODE_LEFT ) )
                translation.x++;
            if ( lastInput.isKeyPressed( SDL_SCANCODE_D ) || lastInput.isKeyPressed( SDL_SCANCODE_RIGHT ) )
                translation.x--;
            if ( lastInput.isKeyPressed( SDL_SCANCODE_R ) )
                translation.y++;
            if ( lastInput.isKeyPressed( SDL_SCANCODE_F ) )
                translation.y--;

            translation *= m_settings->cameraControllerSettings.translationSpeed;
            translation *= lastInput.deltaTime;

            if ( translation != glm::vec3 {} )
                m_cameraEntity.get<Transform>().move( translation );
            m_freeFly.update( lastInput );

            wasFreefly = true;
        }

        if ( m_settings->displayTarget && !lastInput.isKeyPressed( SDL_SCANCODE_LSHIFT ) )
        {
            const Transform & transform = m_cameraEntity.get<Transform>();
            const Ray         ray { transform.getPosition(), transform.getDirection() };
            glm::vec3         target = ray.origin + ray.direction * 50.f;
            m_world->intersect( ray, target );
            m_cameraEntity.get<Target>().position = target;
        }
    }

    std::unique_ptr<State> DockingState::next() const
    {
        return std::make_unique<SpaceshipState>( m_uiManager, m_settings, m_system, m_world, m_cameraEntity );
    }

    void DockingState::resetCamera()
    {
        const Aabb & worldBB = m_system->getBoundingBox();
        m_cameraEntity.get<Camera>().lookAt( worldBB );
    }

} // namespace udk
