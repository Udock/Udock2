#include "Udock/Ui/State/State.hpp"

namespace udk
{
    State::State( UiManager *     uiManager,
                  UdockSettings * settings,
                  System *        system,
                  PhysicalWorld * world,
                  Entity          camera ) :
        m_uiManager( uiManager ),
        m_settings( settings ), m_system( system ), m_world( world ), m_cameraEntity( camera )
    {
    }
} // namespace udk
