<h1 align="center">Udock</h1>

![Logo](readme/Udock2-Banner.png)

- [Udock](#udock)
    - [Introduction](#introduction)
    - [Usage and prebuilt release](#usage-and-prebuilt-release)
    - [Build](#build)
    - [Acknowledgement](#acknowledgement)

## Introduction

Udock is a free interactive multibody protein-protein docking system, intended for both naive and experienced users.
UDock is developed at the Conservatoire National des Arts et Metiers, France, by the Centre d'Etudes et de Recherche
en Informatique et Communications (CEDRIC), the Laboratoire de Genomique, Bioinformatique, et Chimie Moleculaire (GBCM)
and the XLIM.

For further information on how to use this software, please refer to the [__Tutorial__](Tutorial.md) and the following
video:

![Udock2 video tutorial](readme/Udock2-Tutorial.mp4){width=100%}

## Prebuilt release

Prebuilt release can be found under [Udock2's website](http://udock.fr) for both Windows and Linux.

**Note that the Linux pre-built binaries require the package `libsdl2-2.0-0` which can be installed with the following command:**

```
sudo apt install libsdl2-2.0-0
```

If you are unable to launch the executable, it may be caused by a problem of permission. The execution permission of the file "Udock" can be enabled with the following command:

```
chmod u+x Udock
```

## Build from source

The project has been tested on Windows 10 (VS2019-22), Ubuntu 20/22 (g++-9/11), and macOS Ventura 13.4.1 with an Apple Silicon M1 Pro (Apple clang 14.0.3).

To clone the project and its dependencies:

```
git clone --recurse-submodules -j8 https://gitlab.com/Udock/Udock2
```

**Note: You cannot use Gitlab UI to download the project as an archive since it will not include submodules.**

The project is using CMake, you may first create a build folder with the following command at the root of the project:

```
mkdir build
cd build
```

On Windows, you can configure and build the project with your favorite IDE or with:

```
cmake ..
cmake --build . --target Udock --config Release
```

On other platforms, you may need the following commands to get a Release build:

```
cmake -DCMAKE_BUILD_TYPE=Release ..
cmake --build . --target Udock
```

After building, executable and resource folders are located in the `build/bin` folder.

We refer to [SDL's linux README](https://github.com/libsdl-org/SDL/blob/main/docs/README-linux.md) regarding required
packages installation.

## Acknowledgement

The following resources have been used for the development of this project:

- [Cubemaps](https://learnopengl.com/Advanced-OpenGL/Cubemaps)
  and [Diffuse irradiance](https://learnopengl.com/PBR/IBL/Diffuse-irradiance)
  chapter of LearnOpengl [Joey de Vries](https://twitter.com/JoeyDeVriez) licensed under the terms of
  the [CC BY-NC 4.0 license](https://creativecommons.org/licenses/by-nc/4.0/).
- [Raptor Spaceship Low Poly](https://skfb.ly/osJ6J) by Noddity is licensed under the terms of
  the [CC BY 4.0 license](http://creativecommons.org/licenses/by/4.0/) and
  converted in OBJ with Blender.
- [Milky Way HDRI file](http://www.hdrlabs.com/sibl/archive.html) by HDRlabs is licensed under the terms of
  the [CC BY-NC-SA 3.0 license](https://creativecommons.org/licenses/by-nc-sa/3.0/)
  and converted in cubemap with [HDRI-to-CubeMap](https://matheowis.github.io/HDRI-to-CubeMap/) by Mateusz Wisniowski.

This project would also not have been made without the help of the following libraries:

- [Bullet Physics SDK](https://github.com/bulletphysics/bullet3)
- [Chemfiles: a library for reading and writing chemistry files](https://github.com/sacraiou/chemfiles)
- [entt](https://github.com/skypjack/entt)
- [fmt](https://github.com/fmtlib/fmt)
- [gl3w: Simple OpenGL core profile loading](https://github.com/skaslev/gl3w)
- [glm](https://github.com/g-truc/glm)
- [Guidelines Support Library (GSL)](https://github.com/microsoft/GSL)
- [Dear ImGui](https://github.com/ocornut/imgui)
- [Marching Cube C++](https://github.com/aparis69/MarchingCubeCpp)
- [Portable File Dialogs](https://github.com/samhocevar/portable-file-dialogs)
- [Simple DirectMedia Layer (SDL) Version 2.0](https://github.com/libsdl-org/SDL)
- [stb](https://github.com/nothings/stb)
- [tinyobjloader](https://github.com/tinyobjloader/tinyobjloader)
