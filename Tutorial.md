# How to use Udock

First of all, Udock2 is a tool dedicated to multi-body protein-protein docking allowing the user to propose a geometry of a molecular complex with a real-time evaluation of its binding energy. This is made by matching shapes and overall blue regions with overall red regions on the surface of several molecules. 

### Visualisation

Once launched, a molecule can be loaded by selecting it through the top left button __Open__ or by __drag and drop__ on
the window.

Molecules can be rotated with the __left click__ while the camera is moved with the __right click__ and __WASD__ keys.
Note that the local trackball camera is enabled by moving the camera with __shift + right click__. Its focus point is located in the center of the window and at the surface of a targeted molecule. It can be
visualized by pressing the __T__ key.

A molecule can be removed by first selecting one item within the list on the left panel and then clicking on
__Remove selected__. The scene can be fully cleared by clicking on __Clear scene__.

### Performing Interactive Molecular Docking

Interactive Molecular Docking can be achieved by placing anchors with __control + left click__ on the desired part of the protein. After placing anchors on two different proteins a link appears representing the linear constraint. Molecules are attracted through the link by pressing the __space bar__.

All constraints can be cleared from the button __Clear constraints__ on the left panel below the list of molecules.

### Binding Energy estimation

The protein surface color represents its electrostatic potential where negative values are represented in red while positive values are represented in blue. The energy of the system can be evaluated in real-time by pulling closer two molecular parts which can be achieved with anchors. The curve, at the top of the window, provides a real-time feedback which guides the exploration. The __Optimize__ button on the left panel allows to automatically refine a proposed conformation. The optimization is performed on the position and rotation of a single body which can be selected from the list on the left panel. Optimization step count can be modified through the slider on the left panel below __Optimization steps__.

Once optimized, the system is frozen to avoid unwanted manipulation. It can be unfrozen by clicking on the checkbox below the label __Freeze molecules__.

### Using the data on other softwares

Once an interesting conformation has been found, all molecules can be saved as separated files with the __Save all__
button at the bottom of the left panel. The button __Save as__ allows to specify the name of each of the
output files.

### Playing with the spaceship

The __Tab__ key allows to change the scene. In the spaceship scene, a playful mode, the same controls are offered
with the additional ability to use a game controller. The spaceship can throw bullets with the __A__ key, the
__left click__ or the __game controller A__ key. These bullets can be used to position anchors on hit by
default or by pressing the __2__ key. It can also serve to move molecular entities by changing of mode by pressing
the __1__ key. These tools can be changed with the __game controller B__ key.  Finally, constraints can be applied
with the __X__ key of the game controller.
