#ifndef UDOCK_MATH_HPP
#define UDOCK_MATH_HPP

#include <glm/glm.hpp>
#include <glm/gtx/component_wise.hpp>
#include <random>

namespace udk
{
    struct Aabb
    {
        glm::vec3 minimum;
        glm::vec3 maximum;
    };

    struct Ray
    {
        glm::vec3 origin;
        glm::vec3 direction;
    };

    constexpr std::size_t nextPowerOfTwoValue( const std::size_t baseNumber )
    {
        std::size_t i = 1;
        while ( baseNumber > i )
            i <<= 1;
        return i;
    }

    constexpr std::size_t nextPowerOfTwoExponent( std::size_t baseNumber )
    {
        uint32_t exponent = 0;
        while ( baseNumber >>= 1 )
        {
            exponent++;
        }
        return exponent;
    }

    inline int random( int min, int max )
    {
        static std::random_device device {};
        std::mt19937              engine( device() );

        // in the range [min, max]
        std::uniform_int_distribution<> distribution( min, max );
        return distribution( engine );
    }

    inline float random( float min, float max )
    {
        static std::random_device device {};
        std::mt19937              engine( device() );

        // in the range [min, max)
        std::uniform_real_distribution<float> distribution( min, max );
        return distribution( engine );
    }

} // namespace udk

#endif // UDOCK_MATH_HPP
