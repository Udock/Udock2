#ifndef UDOCK_UTILS_HPP
#define UDOCK_UTILS_HPP

#include <filesystem>
#include <gsl/span>

namespace udk
{
    using Path = std::filesystem::path;

    template<class Type>
    using Span = gsl::span<Type>;

    template<class Enum>
    constexpr typename std::underlying_type<Enum>::type toUnderlying( const Enum e ) noexcept
    {
        return static_cast<typename std::underlying_type<Enum>::type>( e );
    }
} // namespace udk

#endif // UDOCK_UTILS_HPP
