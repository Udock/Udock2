#ifndef UDOCK_MACRO_HPP
#define UDOCK_MACRO_HPP

#include "Udock/Core/Types.hpp"

#define UDOCK_DECLARE_BITWISE_FUNCTIONS( Type )                                      \
    inline constexpr Type operator&( const Type l, const Type r )                    \
    {                                                                                \
        return static_cast<Type>( udk::toUnderlying( l ) & udk::toUnderlying( r ) ); \
    }                                                                                \
                                                                                     \
    inline constexpr Type operator|( const Type l, const Type r )                    \
    {                                                                                \
        return static_cast<Type>( udk::toUnderlying( l ) | udk::toUnderlying( r ) ); \
    }                                                                                \
                                                                                     \
    inline constexpr Type operator~( const Type m ) { return static_cast<Type>( ~udk::toUnderlying( m ) ); }

#endif // UDOCK_MACRO_HPP
