#ifndef UDOCK_ENTITY_HPP
#define UDOCK_ENTITY_HPP

#include "Udock/Core/Math.hpp"
#include "Udock/Data/Mesh.hpp"
#include "Udock/System/Transform.hpp"
#include <BulletCollision/CollisionShapes/btTriangleIndexVertexArray.h>
#include <BulletCollision/Gimpact/btGImpactShape.h>
#include <BulletDynamics/Dynamics/btRigidBody.h>
#include <LinearMath/btDefaultMotionState.h>
#include <functional>

namespace udk
{
    inline btVector3 toBullet( const glm::vec3 & v ) { return btVector3 { v.x, v.y, v.z }; }
    inline glm::vec3 toGlm( const btVector3 & v ) { return { v.x(), v.y(), v.z() }; }
    inline glm::mat3 toGlm( const btMatrix3x3 & m )
    {
        return { toGlm( m.getColumn( 0 ) ), toGlm( m.getColumn( 1 ) ), toGlm( m.getColumn( 2 ) ) };
    }

    class Collider
    {
      public:
        Collider() = default;
        Collider( std::unique_ptr<btCollisionShape> && collisionShape ) :
            m_collisionShape( std::move( collisionShape ) )
        {
        }

        virtual ~Collider() = default;

        btCollisionShape * getHandle() { return m_collisionShape.get(); };

      protected:
        std::unique_ptr<btCollisionShape> m_collisionShape;
    };

    class MeshCollider : public Collider
    {
      public:
        MeshCollider( const Mesh & surface );

      private:
        std::unique_ptr<btTriangleIndexVertexArray> m_vertexArray;
    };

    class ConvexCollider : public Collider
    {
      public:
        ConvexCollider( const std::vector<glm::vec3> & surface );
    };

    class SphereCollider : public Collider
    {
      public:
        SphereCollider( float radius );
    };

    class PhysicalWorld;
    class RigidBody
    {
      public:
        static constexpr auto in_place_delete = true;

        using CollisionCallback = std::function<
            void( RigidBody & /* current */, RigidBody & /* other */, const glm::vec3 & /* contactPoint */ )>;

        RigidBody() = default;
        RigidBody( std::unique_ptr<Collider> && collider, const Transform & initialTransform = {} );

        glm::vec3 getAngularVelocity() const;
        glm::vec3 getLinearVelocity() const;
        void      setAngularVelocity( const glm::vec3 & v );
        void      setLinearVelocity( const glm::vec3 & v );
        void      applyForce( const glm::vec3 & force, const glm::vec3 & relativePosition );
        void      applyCentralForce( const glm::vec3 & force );
        void      applyTorque( const glm::vec3 & torque );

        void ignoreCollisionWith( const RigidBody & other );
        void setIgnoreCollision( bool value );
        void setCollisionCallback( CollisionCallback callback );

        void setFreeze( bool freezeStatus );
        Aabb getBoundingBox() const;

        void update( float deltaTime, Transform & toUpdate );

        friend PhysicalWorld;

      private:
        void      setTransform( const glm::mat4 & newTransform );
        glm::mat4 getTransform() const;
        void      notifyCollisionWith( RigidBody & other, const glm::vec3 & contactPoint );

        std::unique_ptr<Collider>             m_collider;
        std::unique_ptr<btDefaultMotionState> m_motionState;
        std::unique_ptr<btRigidBody>          m_handle;

        bool              m_freezeStatus = false;
        CollisionCallback m_collisionCallback {};
    };
} // namespace udk

#endif // UDOCK_ENTITY_HPP
