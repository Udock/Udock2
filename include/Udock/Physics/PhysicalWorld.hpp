#ifndef UDOCK_PHYSICALWORLD_HPP
#define UDOCK_PHYSICALWORLD_HPP

#include "Udock/Physics/RigidBody.hpp"
#include "Udock/System/Listener.hpp"
#include <BulletCollision/BroadphaseCollision/btDbvtBroadphase.h>
#include <BulletCollision/CollisionDispatch/btDefaultCollisionConfiguration.h>
#include <BulletDynamics/ConstraintSolver/btSequentialImpulseConstraintSolver.h>
#include <BulletDynamics/Dynamics/btDiscreteDynamicsWorld.h>
#include <atomic>
#include <entt/entt.hpp>

namespace udk
{
    class System;

    struct Destroyable
    {
    };
    class PhysicalWorld
    {
      public:
        PhysicalWorld( System * system );
        void clearConstraints();

        void setFreeze( bool freezeStatus );
        bool getFreezeStatus() const { return m_freezeStatus; }

        RigidBody * intersect( const Ray & ray, Entity & hitEntity );
        bool        intersect( const Ray & ray, glm::vec3 & hit );
        bool        intersect( const Ray & ray, glm::vec3 & hitPosition, RigidBody *& hitRigidBody );
        bool        intersect( const Ray & ray, glm::vec3 & hitPosition, Entity & hitEntity );

        void applyConstraints( float strength );
        void update();

      private:
        std::pair<const btRigidBody *, glm::vec3> intersect( const Ray & ray, const float maxDistance = 1000.f );

        void addRigidBody( RigidBody & body );
        void removeRigidBody( Entity removedEntity, RigidBody & body );

        System * m_system;

        Listener<RigidBody> m_rigidBodyListener;

        btDefaultCollisionConfiguration     m_collisionConfiguration;
        btCollisionDispatcher               m_dispatcher;
        btDbvtBroadphase                    m_overlappingPairCache;
        btSequentialImpulseConstraintSolver m_solver;
        btDiscreteDynamicsWorld             m_world;

        std::atomic<bool> m_freezeStatus = false;
    };
} // namespace udk

#endif // UDOCK_PHYSICALWORLD_HPP
