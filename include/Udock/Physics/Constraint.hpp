#ifndef UDOCK_CONSTRAINT_HPP
#define UDOCK_CONSTRAINT_HPP

#include "Udock/Physics/RigidBody.hpp"
#include "Udock/System/System.hpp"
#include <entt/entt.hpp>

namespace udk
{
    struct Anchor
    {
        static constexpr bool in_place_delete = true;

        // Weak reference, the parent is not depending
        // on the constraint
        Entity    parent;
        glm::vec3 relativePosition {};

        Anchor( Entity selectedParent, glm::vec3 worldPosition );
        glm::vec3 getWorldPosition() const;
    };

    struct LineConstraint
    {
        // Strong references (if the constraint is destroyed,
        // both child anchors are destroyed)
        Entity firstChild;
        Entity secondChild;

        void apply( const glm::vec3 & worldCenter, float strength );
    };
} // namespace udk

#endif // UDOCK_CONSTRAINT_HPP
