#ifndef UDOCK_ENERGY_HPP
#define UDOCK_ENERGY_HPP

#include "Udock/Data/Molecule.hpp"
#include "Udock/Physics/RigidBody.hpp"

namespace udk
{
    struct EnergyComputationEntity
    {
        std::vector<Atom>     atoms;
        AccelerationStructure accelerationStructures;
        glm::mat4             transform;
    };

    struct EnergyComputationData
    {
        std::vector<EnergyComputationEntity> entities;
        float                                maxDistance;
    };

    using EnergyComputationFunction = std::function<float( const glm::vec3 & /* point1 */,
                                                           float /* radius1 */,
                                                           float /* epsilon1 */,
                                                           float /* charge1 */,
                                                           const glm::vec3 & /* point2 */,
                                                           float /* radius2 */,
                                                           float /* epsilon2 */,
                                                           float /* charge2  */ )>;

    float lennardJonesCoulombic( const glm::vec3 & point1,
                                 float             radius1,
                                 float             epsilon1,
                                 float             charge1,
                                 const glm::vec3 & point2,
                                 float             radius2,
                                 float             epsilon2,
                                 float             charge2 );

    float computeEnergy( const EnergyComputationData &     data,
                         const EnergyComputationFunction & energyComputation = lennardJonesCoulombic );

    void optimizationStep( const std::size_t                 toOptimizeId,
                           const EnergyComputationFunction & energyComputation,
                           float                             stepSize,
                           EnergyComputationData &           data,
                           float &                           lastDelta,
                           glm::mat4 &                       bestTransform,
                           float &                           bestEnergy );

} // namespace udk

#endif // UDOCK_ENERGY_HPP
