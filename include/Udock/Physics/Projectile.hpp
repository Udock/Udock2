#ifndef UDOCK_PROJECTILE_HPP
#define UDOCK_PROJECTILE_HPP

#include "Udock/System/System.hpp"
#include "Udock/Utils/Chrono.hpp"
#include <entt/entity/handle.hpp>
#include <glm/vec3.hpp>

namespace udk
{
    class RigidBody;
    class Projectile
    {
      public:
        using ProjectileHitCallback = std::function<void( Entity & /* hitEntity */, const glm::vec3 & /* hitPoint */ )>;

        Projectile( Entity                entity,
                    Entity                hitEntity,
                    glm::vec3             hitPoint,
                    const float           radius            = 10.f,
                    const float           lifeTimeInSeconds = 1.f,
                    ProjectileHitCallback callback          = {} );
        ~Projectile() = default;

        float getRadius() const { return m_radius; }
        void  setHitCallback( ProjectileHitCallback callback );

        void fire( const glm::vec3 & direction, const float strength = 1e5f );
        void update();

      private:
        Entity                m_entity;
        Entity                m_hitEntity;
        glm::vec3             m_hitPoint;
        glm::vec3             m_lastForce {};
        float                 m_radius;
        float                 m_lifeTimeInSeconds;
        Chrono                m_chrono;
        ProjectileHitCallback m_callback;
    };
} // namespace udk

#endif // UDOCK_PROJECTILE_HPP
