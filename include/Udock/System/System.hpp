#ifndef UDOCK_SYSTEM_HPP
#define UDOCK_SYSTEM_HPP

#include "Udock/Data/Molecule.hpp"
#include <entt/entity/registry.hpp>

namespace udk
{
    using Entity = entt::handle;
    class System
    {
      public:
        System();
        ~System() = default;

        Entity create();
        Entity create( Molecule molecule );
        void   erase( const entt::entity toRemove );

        template<class ComponentType>
        void clear()
        {
            const auto components = m_registry.view<ComponentType>();
            m_registry.destroy( components.begin(), components.end() );

            m_positionCounter = 0;
            m_aabb            = { glm::vec3 {}, glm::vec3 {} };
            m_lastPosition    = {};
        }

        entt::registry & getRegistry() { return m_registry; }
        std::size_t      size() const { return m_registry.size(); }

        void         resetMoleculePosition();
        const Aabb & getBoundingBox() const { return m_aabb; }

      private:
        entt::registry m_registry {};

        Aabb        m_aabb { glm::vec3 {}, glm::vec3 {} };
        glm::vec3   m_lastPosition {};
        std::size_t m_positionCounter = 0;
    };
} // namespace udk

#endif // UDOCK_SYSTEM_HPP
