#ifndef UDOCK_TRANSFORM_HPP
#define UDOCK_TRANSFORM_HPP

#include "Udock/Core/Math.hpp"
#include <glm/ext/matrix_transform.hpp>
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <set>

namespace udk
{
    class Transform
    {
      public:
        static constexpr bool in_place_delete = true;

        Transform( glm::vec3 position = {}, glm::dquat rotation = glm::dquat( 1.0, { 0.0, 0.0, 0.0 } ) );

        Transform( const Transform & )             = delete;
        Transform & operator=( const Transform & ) = delete;

        Transform( Transform && ) noexcept             = default;
        Transform & operator=( Transform && ) noexcept = default;

        ~Transform();

        void setParent( Transform * parent );
        void addChild( Transform * child );
        void removeChild( Transform * child );

        void      set( const glm::mat4 & transform );
        glm::mat4 get() const;

        void lookAt( glm::vec3 target );

        glm::vec3  getPosition() const;
        glm::vec3  getRelativePosition() const;
        glm::dquat getRotation() const;
        glm::dquat getRelativeRotation() const;

        void setRotation( const glm::dquat & newRotation );
        void setPosition( const glm::vec3 & newPosition );
        void move( const glm::vec3 & delta );
        void rotate( const glm::vec3 & angles );
        void rotatePitch( const double pitch );
        void rotateYaw( const double yaw );
        void rotateRoll( const double roll );

        void rotateAround( const glm::dquat & rotation, const glm::dvec3 & target, const double distance );

        glm::vec3 getDirection() const;
        glm::vec3 getLeft() const;
        glm::vec3 getUp() const;

        void reset();

      private:
        Transform *           m_parent = nullptr;
        std::set<Transform *> m_children {};

        glm::vec3  m_position;
        glm::dquat m_rotation = glm::dquat( 1.0, { 0.0, 0.0, 0.0 } );
    };

} // namespace udk

#endif // UDOCK_TRANSFORM_HPP
