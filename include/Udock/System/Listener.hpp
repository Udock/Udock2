#ifndef UDOCK_LISTENER_HPP
#define UDOCK_LISTENER_HPP

#include "Udock/System/System.hpp"
#include <entt/entt.hpp>
#include <functional>

namespace udk
{
    enum class Event
    {
        Construct,
        Update,
        Destroy
    };

    class System;
    template<class ComponentType>
    class Listener
    {
      public:
        using ListenerCallback = std::function<void( Entity entity, Event eventType )>;

        Listener() = default;
        Listener( System * system, ListenerCallback callback );
        ~Listener();

        void subscribe( Event eventType );
        void unsubscribe( Event eventType );

      private:
        void onConstruct( entt::registry & registry, entt::entity entity );
        void onUpdate( entt::registry & registry, entt::entity entity );
        void onDestroy( entt::registry & registry, entt::entity entity );

        System *         m_system = nullptr;
        ListenerCallback m_callback;
        std::set<Event>  m_registeredEvent;
    };
} // namespace udk

#include "Udock/System/Listener.inl"

#endif // UDOCK_LISTENER_HPP
