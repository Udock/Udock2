#ifndef UDOCK_CAMERA_HPP
#define UDOCK_CAMERA_HPP

#include "Udock/Core/Math.hpp"
#include "Udock/System/Transform.hpp"

namespace udk
{
    struct Target
    {
        static constexpr bool in_place_delete = true;

        glm::vec3 position;
    };

    class Camera
    {
      public:
        static constexpr bool in_place_delete = true;

        Camera( Transform * transform, const glm::vec3 & target, const float aspectRatio = 16.f / 9.f );

        void setAspectRatio( const float aspectRatio ) { m_aspectRatio = aspectRatio; }

        void      lookAt( const Aabb & target );
        glm::mat4 getViewMatrix() const;
        glm::mat4 getProjectionMatrix() const;

        glm::vec3 screenToWorld( const glm::ivec2 & screenCoordinates,
                                 const glm::ivec2 & screenDimension,
                                 bool               invY = true ) const;
        float     getNear() const { return m_near; }
        float     getFar() const { return m_far; }
        float     getFov() const { return glm::radians( m_fov ); }

      private:
        Transform * m_transform;
        glm::vec3   m_target;

        float m_fov         = 60.f;
        float m_aspectRatio = 16.f / 9.f;
        float m_near        = 1e-2f;
        float m_far         = 1000.f;
    };
} // namespace udk

#endif // UDOCK_CAMERA_HPP
