#include "Udock/System/Listener.hpp"
#include "Udock/System/System.hpp"

namespace udk
{
    template<class ComponentType>
    Listener<ComponentType>::Listener( System * system, ListenerCallback callback ) :
        m_system( system ), m_callback( std::move( callback ) )
    {
    }

    template<class ComponentType>
    Listener<ComponentType>::~Listener()
    {
        while ( !m_registeredEvent.empty() )
        {
            unsubscribe( *m_registeredEvent.begin() );
        }
    }

    template<class ComponentType>
    void Listener<ComponentType>::subscribe( Event event )
    {
        entt::registry & registry = m_system->getRegistry();
        switch ( event )
        {
        case Event::Construct:
            registry.on_construct<ComponentType>().template connect<&Listener<ComponentType>::onConstruct>( this );
            break;
        case Event::Update:
            registry.on_update<ComponentType>().template connect<&Listener<ComponentType>::onUpdate>( this );
            break;
        case Event::Destroy:
            registry.on_destroy<ComponentType>().template connect<&Listener<ComponentType>::onDestroy>( this );
            break;
        }
        m_registeredEvent.emplace( event );
    }

    template<class ComponentType>
    void Listener<ComponentType>::unsubscribe( Event event )
    {
        entt::registry & registry = m_system->getRegistry();
        switch ( event )
        {
        case Event::Construct:
            registry.on_construct<ComponentType>().template disconnect<&Listener<ComponentType>::onConstruct>( this );
            break;
        case Event::Update:
            registry.on_update<ComponentType>().template disconnect<&Listener<ComponentType>::onUpdate>( this );
            break;
        case Event::Destroy:
            registry.on_destroy<ComponentType>().template disconnect<&Listener<ComponentType>::onDestroy>( this );
            break;
        }
        m_registeredEvent.erase( event );
    }

    template<class ComponentType>
    void Listener<ComponentType>::onConstruct( entt::registry & registry, entt::entity entity )
    {
        m_callback( { registry, entity }, Event::Construct );
    }

    template<class ComponentType>
    void Listener<ComponentType>::onUpdate( entt::registry & registry, entt::entity entity )
    {
        m_callback( { registry, entity }, Event::Update );
    }

    template<class ComponentType>
    void Listener<ComponentType>::onDestroy( entt::registry & registry, entt::entity entity )
    {
        m_callback( { registry, entity }, Event::Destroy );
    }
} // namespace udk
