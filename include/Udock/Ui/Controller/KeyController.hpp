#ifndef UDOCK_KEYBOARDCONTROLLER_HPP
#define UDOCK_KEYBOARDCONTROLLER_HPP

#include "Udock/Ui/UiInput.hpp"
#include <SDL.h>
#include <functional>
#include <vector>

namespace udk
{
    using KeyCallback = std::function<void( const UiInput & )>;
    class KeyboardController
    {
      public:
        KeyboardController()  = default;
        ~KeyboardController() = default;

        using KeyConfiguration = std::pair<SDL_Scancode, KeyAction>;
        void registerCallback( std::vector<KeyConfiguration> key, KeyCallback callback );
        void update( const UiInput & uiInput ) const;

      private:
        std::vector<std::pair<std::vector<KeyConfiguration>, KeyCallback>> m_callbacks;
    };

    class GameControllerController
    {
      public:
        GameControllerController()  = default;
        ~GameControllerController() = default;

        using KeyConfiguration = std::pair<SDL_GameControllerButton, KeyAction>;
        void registerCallback( std::vector<KeyConfiguration> key, KeyCallback callback );
        void update( const UiInput & uiInput ) const;

      private:
        std::vector<std::pair<std::vector<KeyConfiguration>, KeyCallback>> m_callbacks;
    };
} // namespace udk

#endif // UDOCK_KEYBOARDCONTROLLER_HPP
