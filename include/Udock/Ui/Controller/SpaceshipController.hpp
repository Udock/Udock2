#ifndef UDOCK_SPACESHIPCONTROLLER_HPP
#define UDOCK_SPACESHIPCONTROLLER_HPP

#include "Udock/System/System.hpp"
#include "Udock/Ui/UiInput.hpp"
#include <entt/entt.hpp>

namespace udk
{
    class RigidBody;
    class SpaceshipController
    {
      public:
        SpaceshipController() = default;
        SpaceshipController( Entity spaceShip );
        ~SpaceshipController() = default;

        void update( const UiInput & lastInput, float cameraStrength, float controllerStrength, float torqueReducer );

      private:
        static glm::vec3 getForce( const UiInput & lastInput );
        static glm::vec2 getTorque( const UiInput & lastInput );
        static bool      getFrontInput( const UiInput & lastInput );
        static bool      getBackInput( const UiInput & lastInput );
        static bool      getLeftInput( const UiInput & lastInput );
        static bool      getRightInput( const UiInput & lastInput );

        Entity m_spaceShip;
    };
} // namespace udk

#endif // UDOCK_SPACESHIPCONTROLLER_HPP
