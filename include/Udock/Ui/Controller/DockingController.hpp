#ifndef UDOCK_DOCKINGCONTROLLER_HPP
#define UDOCK_DOCKINGCONTROLLER_HPP

#include "Udock/Physics/PhysicalWorld.hpp"
#include "Udock/Ui/UiInput.hpp"
#include <optional>

namespace udk
{
    struct DockingControllerSettings
    {
        static const float EntityAngularVelocityDefault;
        float              entityAngularVelocity = EntityAngularVelocityDefault;
        static const float UserInputAngularDecaySpeedDefault;
        float              userInputAngularDecaySpeed = UserInputAngularDecaySpeedDefault;
    };

    class DockingController
    {
      public:
        DockingController( const DockingControllerSettings * settings,
                           System *                          system,
                           PhysicalWorld *                   world,
                           Entity                            cameraEntity );
        ~DockingController();

        void update( const UiInput & uiInput );

      private:
        const DockingControllerSettings * m_settings;

        Listener<Molecule>    m_moleculeListener;
        System *              m_system;
        PhysicalWorld *       m_world;
        Entity                m_cameraEntity;
        std::optional<Entity> m_firstAnchor;
    };

} // namespace udk

#endif // UDOCK_DOCKINGCONTROLLER_HPP
