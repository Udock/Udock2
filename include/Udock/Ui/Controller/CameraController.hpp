#ifndef UDOCK_CAMERACONTROLLER_HPP
#define UDOCK_CAMERACONTROLLER_HPP

#include "Udock/Physics/PhysicalWorld.hpp"
#include "Udock/System/Camera.hpp"
#include "Udock/Ui/UiInput.hpp"

namespace udk
{
    struct CameraControllerSettings
    {
        static const float TranslationSpeedDefault;
        float              translationSpeed = TranslationSpeedDefault;
        static const float TranslationFactorSpeedDefault;
        float              translationFactorSpeed = TranslationFactorSpeedDefault;
        static const float RotationSpeedDefault;
        float              rotationSpeed = RotationSpeedDefault;
        static const float ElasticityFactorDefault;
        float              elasticityFactor = ElasticityFactorDefault;
        static const float ElasticityThresholdDefault;
        float              elasticityThreshold = ElasticityThresholdDefault;
    };

    class FreeFly
    {
      public:
        FreeFly() = default;
        FreeFly( const CameraControllerSettings * settings, Entity cameraEntity );
        ~FreeFly() = default;

        void update( const UiInput & uiInput );

      private:
        const CameraControllerSettings * m_settings;
        Entity                           m_cameraEntity;
    };

    class TrackBall
    {
      public:
        TrackBall() = default;
        TrackBall( const CameraControllerSettings * settings, PhysicalWorld * world, Entity cameraEntity );
        ~TrackBall() = default;

        bool update( const UiInput & uiInput, bool updateTarget = true );

      private:
        const CameraControllerSettings * m_settings;
        PhysicalWorld *                  m_world;
        Entity                           m_cameraEntity;
        glm::vec3                        m_target;
    };

} // namespace udk

#endif // UDOCK_CAMERACONTROLLER_HPP
