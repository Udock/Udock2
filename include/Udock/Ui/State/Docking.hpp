#ifndef UDOCK_DOCKING_HPP
#define UDOCK_DOCKING_HPP

#include "Udock/Ui/Controller/CameraController.hpp"
#include "Udock/Ui/Controller/DockingController.hpp"
#include "Udock/Ui/Controller/KeyController.hpp"
#include "Udock/Ui/State/State.hpp"

namespace udk
{
    class DockingState : public State
    {
      public:
        DockingState( UiManager *     uiManager,
                      UdockSettings * settings,
                      System *        system,
                      PhysicalWorld * world,
                      Entity          camera );
        ~DockingState() override = default;

        void update( const UiInput & lastInput ) override;

        void                   resetCamera() override;
        std::unique_ptr<State> next() const override;

      private:
        KeyboardController m_keyboardController {};
        DockingController  m_entityController;
        TrackBall          m_trackBall;
        FreeFly            m_freeFly;

        Listener<Molecule> m_moleculeListener;
    };
} // namespace udk

#endif // UDOCK_DOCKING_HPP
