#ifndef UDOCK_STATE_HPP
#define UDOCK_STATE_HPP

#include "Udock/System/Listener.hpp"
#include "Udock/Ui/UiInput.hpp"
#include <entt/entt.hpp>

namespace udk
{
    class UiManager;
    struct UdockSettings;
    class System;
    class PhysicalWorld;

    using Entity = entt::handle;

    class State
    {
      public:
        State( UiManager * uiManager, UdockSettings * settings, System * system, PhysicalWorld * world, Entity camera );
        virtual ~State() = default;

        virtual void update( const UiInput & lastInput ) = 0;
        virtual bool done() const { return m_done; }
        virtual void resetCamera() = 0;

        virtual std::unique_ptr<State> next() const = 0;

      protected:
        UiManager *        m_uiManager;
        UdockSettings *    m_settings;
        System *           m_system;
        PhysicalWorld *    m_world;
        Entity             m_cameraEntity;
        Listener<Molecule> m_moleculeListener;

        bool m_done = false;
    };
} // namespace udk

#endif // UDOCK_STATE_HPP
