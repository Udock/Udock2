#ifndef UDOCK_SPACESHIP_HPP
#define UDOCK_SPACESHIP_HPP

#include "Udock/Renderer/Texture.hpp"
#include "Udock/System/Listener.hpp"
#include "Udock/Ui/Controller/KeyController.hpp"
#include "Udock/Ui/Controller/SpaceshipController.hpp"
#include "Udock/Ui/State/State.hpp"
#include "Udock/Utils/Chrono.hpp"

namespace udk
{
    struct SpaceshipSettings
    {
        static const float BulletRadiusDefault;
        float              bulletRadius = BulletRadiusDefault;
        static const float ToolAnchorBulletStrengthDefault;
        float              toolAnchorBulletStrength = ToolAnchorBulletStrengthDefault;
        static const float ToolCollisionBulletStrengthDefault;
        float              toolCollisionBulletStrength = ToolAnchorBulletStrengthDefault;

        static const float CameraInputStrengthDefault;
        float              cameraInputStrength = CameraInputStrengthDefault;
        static const float GameControllerCameraInputStrengthDefault;
        float              gameControllerCameraInputStrength = GameControllerCameraInputStrengthDefault;
        static const float TorqueInputStrengthReducerDefault;
        float              torqueInputStrengthReducer = TorqueInputStrengthReducerDefault;
    };

    struct TemporaryAnchor
    {
        static constexpr bool in_place_delete = true;

        glm::vec3 relativePosition {};
    };

    class SpaceshipState : public State
    {
      public:
        SpaceshipState( UiManager *     uiManager,
                        UdockSettings * settings,
                        System *        system,
                        PhysicalWorld * world,
                        Entity          cameraEntity );
        ~SpaceshipState() override;

        void update( const UiInput & lastInput ) override;

        void                   resetCamera() override;
        std::unique_ptr<State> next() const override;

      private:
        void fire();

        Entity                                m_ship;
        SpaceshipController                   m_spaceshipController;
        KeyboardController                    m_keyboardController;
        GameControllerController              m_gameController;
        ChronoBase<std::chrono::milliseconds> m_chrono;

        Texture m_sightTexture;

        bool m_isAnchorTool = true;
    };
} // namespace udk

#endif // UDOCK_SPACESHIP_HPP
