#ifndef UDOCK_UIINPUT_HPP
#define UDOCK_UIINPUT_HPP

#include "Udock/Core/Types.hpp"
#include <SDL.h>
#include <glm/glm.hpp>
#include <optional>
#include <set>

namespace udk
{
    enum class KeyAction
    {
        // Can be hold
        Pressed,

        // Only a single time
        Down,
        Up
    };

    struct GameController
    {
        glm::vec2 left;
        glm::vec2 right;

        float leftShoulder;
        float rightShoulder;

        std::set<SDL_GameControllerButton> keysPressed;
        std::set<SDL_GameControllerButton> keysDown;
        std::set<SDL_GameControllerButton> keysUp;

        bool isKeyPressed( const SDL_GameControllerButton key ) const
        {
            return keysPressed.find( key ) != keysPressed.end();
        }
        bool isKeyDown( const SDL_GameControllerButton key ) const { return keysDown.find( key ) != keysDown.end(); }
        bool isKeyUp( const SDL_GameControllerButton key ) const { return keysUp.find( key ) != keysUp.end(); }

        bool isKeyActivated( const SDL_GameControllerButton key, const KeyAction action ) const
        {
            switch ( action )
            {
            case KeyAction::Down: return isKeyDown( key );
            case KeyAction::Up: return isKeyUp( key );
            case KeyAction::Pressed: return isKeyPressed( key );
            }
            return false;
        }
    };

    struct UiInput
    {
        glm::ivec2 windowSize;
        bool       windowResized = false;

        float deltaTime;

        glm::ivec2 mousePosition;
        glm::ivec2 deltaMousePosition;

        int32_t deltaMouseWheel;

        bool doubleLeftClick = false;

        bool mouseLeftClicked = false;
        bool mouseLeftPressed = false;

        bool mouseRightClicked = false;
        bool mouseRightPressed = false;

        bool mouseMiddleClicked = false;
        bool mouseMiddlePressed = false;

        std::optional<Path> droppedFile;

        std::set<SDL_Scancode> keysPressed;
        std::set<SDL_Scancode> keysDown;
        std::set<SDL_Scancode> keysUp;

        std::optional<GameController> controller;

        bool isKeyPressed( const SDL_Scancode key ) const { return keysPressed.find( key ) != keysPressed.end(); }
        bool isKeyDown( const SDL_Scancode key ) const { return keysDown.find( key ) != keysDown.end(); }
        bool isKeyUp( const SDL_Scancode key ) const { return keysUp.find( key ) != keysUp.end(); }

        bool isKeyActivated( const SDL_Scancode key, const KeyAction action ) const
        {
            switch ( action )
            {
            case KeyAction::Down: return isKeyDown( key );
            case KeyAction::Up: return isKeyUp( key );
            case KeyAction::Pressed: return isKeyPressed( key );
            }
            return false;
        }

        void reset()
        {
            droppedFile     = {};
            doubleLeftClick = false;

            mouseLeftClicked   = false;
            mouseRightClicked  = false;
            mouseMiddleClicked = false;

            windowResized      = false;
            deltaMouseWheel    = 0;
            deltaMousePosition = {};
            keysDown           = {};
            keysUp             = {};

            if ( controller )
            {
                controller->keysDown = {};
                controller->keysUp   = {};
            }
        }
    };
} // namespace udk

#endif // UDOCK_UIINPUT_HPP
