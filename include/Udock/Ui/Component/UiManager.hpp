#ifndef UDOCK_UIMANAGER_HPP
#define UDOCK_UIMANAGER_HPP

#include "Udock/Ui/Component/FloatingWindow.hpp"
#include "Udock/Ui/Component/Menu.hpp"
#include <memory>
#include <optional>
#include <unordered_map>

namespace udk
{
    class UiManager
    {
      public:
        UiManager()  = default;
        ~UiManager() = default;

        UiManager( const UiManager & )             = delete;
        UiManager & operator=( const UiManager & ) = delete;

        UiManager( UiManager && )             = default;
        UiManager & operator=( UiManager && ) = default;

        void setMenuBar( const MenuBar & menuBar ) { m_menuBar = menuBar; }

        bool hasWindow( const std::string & name ) { return m_windows.find( name ) != m_windows.end(); }
        void addWindow( std::string name, std::unique_ptr<FloatingWindow> window );
        void removeWindow( const std::string & name );

        void draw();

      private:
        using WindowHashMap = std::unordered_map<std::string, std::unique_ptr<FloatingWindow>>;
        WindowHashMap          m_windows;
        std::optional<MenuBar> m_menuBar;
    };
} // namespace udk

#endif // UDOCK_UIMANAGER_HPP
