#include "Udock/Ui/Component/WindowComponent.hpp"
#include <imgui.h>

namespace udk
{
    template<class Type>
    ValueComponent<Type>::ValueComponent( std::string                name,
                                          Type                       value,
                                          ValueChangedCallback<Type> valueChangedCallback ) :
        Component( std::move( name ) ),
        m_value( std::move( value ) ), m_valueChangedCallback( std::move( valueChangedCallback ) )
    {
    }

    template<class Type>
    void ValueComponent<Type>::notify() const
    {
        if ( m_valueChangedCallback )
            m_valueChangedCallback( m_value );
    }

    template<class Type>
    Slider<Type>::Slider( std::string                name,
                          Type                       value,
                          std::pair<Type, Type>      range,
                          ValueChangedCallback<Type> valueChangedCallback ) :
        ValueComponent<Type>( std::move( name ), std::move( value ), std::move( valueChangedCallback ) ),
        m_range( std::move( range ) )
    {
    }

    template<class Type>
    void Slider<Type>::operator()()
    {
        if constexpr ( std::is_floating_point_v<Type> )
        {
            float value = static_cast<float>( this->m_value );
            if ( ImGui::SliderFloat( this->m_name.c_str(),
                                     &value,
                                     static_cast<float>( m_range.first ),
                                     static_cast<float>( m_range.second ),
                                     "%.1f" ) )
            {
                this->m_value = static_cast<Type>( value );
                this->notify();
            }
        }
        else if constexpr ( std::is_integral_v<Type> )
        {
            int value = static_cast<int>( this->m_value );
            if ( ImGui::SliderInt( this->m_name.c_str(),
                                   reinterpret_cast<int *>( &value ),
                                   static_cast<int>( m_range.first ),
                                   static_cast<int>( m_range.second ) ) )
            {
                this->m_value = static_cast<Type>( value );
                this->notify();
            }
        }
    }
} // namespace udk
