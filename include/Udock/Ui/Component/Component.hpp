#ifndef UDOCK_COMPONENT_HPP
#define UDOCK_COMPONENT_HPP

#include <functional>
#include <glm/vec4.hpp>
#include <string>

namespace udk
{
    class Component
    {
      public:
        Component( std::string name );
        virtual ~Component() = default;

        std::string getName() const { return m_name; };

        virtual void operator()() = 0;

      protected:
        std::string m_name;
    };

    class Text : public Component
    {
      public:
        using TextCallback = std::function<void( std::string & )>;

      public:
        Text( std::string name, TextCallback callback = {}, bool wrapped = true );
        ~Text() override = default;

        void operator()() override;

      private:
        TextCallback m_callback;
        bool         m_wrapped;
    };

    class ColoredText : public Component
    {
      public:
        using ColoredTextCallback = std::function<void( std::string &, glm::vec4 & )>;

      public:
        ColoredText( std::string name, ColoredTextCallback callback = {}, bool wrapped = true );
        ~ColoredText() override = default;

        void operator()() override;

      private:
        ColoredTextCallback m_callback;
        glm::vec4           m_color {};
        bool                m_wrapped;
    };

    using UiAction = std::function<void()>;
    class ActionComponent : public Component
    {
      public:
        ActionComponent(
            std::string name,
            UiAction    action = []() {} );

        virtual void operator()() = 0;

      protected:
        void execute() const;

      private:
        UiAction m_action;
    };

    class Button : public ActionComponent
    {
      public:
        Button(
            std::string name,
            UiAction    action = []() {} );
        ~Button() override = default;

        void operator()() override;
    };
} // namespace udk

#endif // UDOCK_COMPONENT_HPP
