#ifndef UDOCK_FLOATINGWINDOW_HPP
#define UDOCK_FLOATINGWINDOW_HPP

#include "Udock/Ui/Component/WindowComponent.hpp"
#include <functional>
#include <glm/glm.hpp>

namespace udk
{
    class FloatingWindow : public Component
    {
      public:
        FloatingWindow() : Component( "" ) {}
        FloatingWindow( std::string      title,
                        glm::vec2        position        = {},
                        glm::ivec2       pixelShift      = {},
                        glm::vec2        size            = {},
                        glm::vec4        backgroundColor = {},
                        ImGuiWindowFlags flags           = ImGuiWindowFlags_None,
                        bool             closable        = true,
                        bool             movable         = false );
        FloatingWindow( std::string      title,
                        glm::vec2        position        = {},
                        glm::vec2        size            = {},
                        glm::vec4        backgroundColor = {},
                        ImGuiWindowFlags flags           = ImGuiWindowFlags_None,
                        bool             closable        = true,
                        bool             movable         = false );
        ~FloatingWindow() = default;

        void setPosition( glm::vec2 position ) { m_position = std::move( position ); }
        void setSize( glm::vec2 size ) { m_position = std::move( size ); }
        void setBackgroundColor( glm::vec4 backgroundColor ) { m_backgroundColor = std::move( backgroundColor ); }

        void addElement( WindowComponent windowElement )
        {
            m_windowElements.emplace_back( std::move( windowElement ) );
        }

        bool isOpen() const { return m_open; }

        void operator()() override;

      private:
        bool             m_open = true;
        glm::vec2        m_position {};
        glm::vec2        m_size {};
        glm::ivec2       m_pixelShift {};
        glm::vec4        m_backgroundColor {};
        ImGuiWindowFlags m_flags {};
        bool             m_closable = true;
        bool             m_movable  = false;

        std::vector<WindowComponent> m_windowElements;
    };
} // namespace udk

#endif // UDOCK_FLOATINGWINDOW_HPP
