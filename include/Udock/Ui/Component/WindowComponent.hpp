#ifndef UDOCK_WINDOWCOMPONENT_HPP
#define UDOCK_WINDOWCOMPONENT_HPP

#include "Udock/Ui/Component/Component.hpp"
#include <atomic>
#include <functional>
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include <optional>
#include <queue>
#include <string>

namespace udk
{
    using WindowComponent = std::function<void()>;
    template<class Type>
    using ValueChangedCallback = std::function<Type( Type )>;

    template<class Type>
    class ValueComponent : public Component
    {
      public:
        ValueComponent( std::string name, Type value, ValueChangedCallback<Type> valueChangedCallback );
        ~ValueComponent() override = default;

        void setValue( Type newValue ) { m_value = newValue; }
        Type getValue() const { return m_value; }
        void setCallback( ValueChangedCallback<Type> valueChangedCallback )
        {
            m_valueChangedCallback = std::move( valueChangedCallback );
        };

      protected:
        void notify() const;

      protected:
        Type                       m_value {};
        ValueChangedCallback<Type> m_valueChangedCallback;
    };

    class Checkbox : public ValueComponent<bool>
    {
      public:
        Checkbox( std::string name, bool value = true, ValueChangedCallback<bool> valueChangedCallback = {} );
        ~Checkbox() override = default;

        void operator()() override;
    };

    template<class Type>
    class Slider : public ValueComponent<Type>
    {
        static_assert( std::is_floating_point_v<Type> || std::is_integral_v<Type>,
                       "Slider type is only implemented for integral and floating point types" );

      public:
        Slider(
            std::string                name,
            Type                       value                = {},
            std::pair<Type, Type>      range                = { 0, 1 },
            ValueChangedCallback<Type> valueChangedCallback = []( Type ) {} );
        ~Slider() override = default;

        void operator()() override;

      private:
        std::pair<Type, Type> m_range;
    };

    class Padding : public Component
    {
      public:
        Padding( glm::vec2 padding, WindowComponent element );
        ~Padding() override = default;

        void operator()() override;

      private:
        glm::vec2       m_padding;
        WindowComponent m_element;
    };

    class WindowComponentList : public Component
    {
      public:
        WindowComponentList( std::string name, std::vector<WindowComponent> elements = {}, bool separator = true );
        ~WindowComponentList() override = default;

        void addComponent( WindowComponent element ) { m_elements.emplace_back( std::move( element ) ); }

        void operator()() override;

      protected:
        std::vector<WindowComponent> m_elements;
        bool                         m_separator;
    };

    class ListBox : public Component
    {
      public:
        using SelectedCallback = std::function<void( std::size_t /* selectedIdx */ )>;
        using DisplayCallback  = std::function<std::optional<std::string_view>( std::size_t /* currentIdx */ )>;

      public:
        ListBox( std::string name, DisplayCallback displayCallback, SelectedCallback selectedCallback = {} );
        ~ListBox() override = default;

        void operator()() override;

      private:
        DisplayCallback  m_displayCallback;
        SelectedCallback m_selectedCallback;

        std::size_t m_selectedIdx = 0;
    };

    class FifoLinePlotter : public Component
    {
      public:
        using ValueProducer = std::function<float()>;
        using ValueResetter = std::function<bool()>;

      public:
        FifoLinePlotter( std::string             name,
                         ValueProducer           valueProducer,
                         std::size_t             displayMaxSize = 64,
                         glm::vec2               size           = {},
                         std::pair<float, float> boundaries     = { 0.f, std::numeric_limits<float>::max() },
                         ValueResetter           resetter       = {} );
        ~FifoLinePlotter() override = default;

        void operator()() override;

      private:
        std::vector<float>      m_values;
        ValueProducer           m_producer;
        std::size_t             m_displayMaxSize;
        std::pair<float, float> m_boundaries;
        glm::vec2               m_size;
        ValueResetter           m_resetter;
    };

    class RgbaColorPicker : public Component
    {
      public:
        using OnColorChange = std::function<void( const glm::vec4 & )>;

      public:
        RgbaColorPicker( std::string name, OnColorChange onColorChange, glm::vec4 defaultValue = {} );
        ~RgbaColorPicker() override = default;

        void operator()() override;

      private:
        OnColorChange m_onColorChange;
        glm::vec4     m_value;
    };

    class Horizontal : public WindowComponentList
    {
      public:
        Horizontal( std::string name, std::vector<WindowComponent> elements = {} );
        ~Horizontal() override = default;
        void operator()() override;
    };

    class ProgressBar : public Component
    {
      public:
        using UpdateProgression = std::function<float()>;

      public:
        ProgressBar( std::string name, UpdateProgression updateProgress );
        ~ProgressBar() override = default;
        void operator()() override;

      private:
        UpdateProgression m_updateProgress;
    };

    class Conditional : public Component
    {
      public:
        Conditional( std::atomic<bool> & condition, WindowComponent component );
        ~Conditional() override = default;

        void operator()() override;

      private:
        std::atomic<bool> & m_condition;
        WindowComponent     m_component;
    };

    class ComboBox : public Component
    {
      public:
        using SelectedCallback = std::function<void( std::size_t /* selectedIdx */ )>;
        using DisplayCallback  = std::function<std::optional<std::string_view>( std::size_t /* currentIdx */ )>;

      public:
        ComboBox( std::string      name,
                  DisplayCallback  displayCallback,
                  SelectedCallback selectedCallback = {},
                  std::size_t      selectedIdx      = 0 );
        ~ComboBox() override = default;

        void operator()() override;

      private:
        DisplayCallback  m_displayCallback;
        SelectedCallback m_selectedCallback;

        std::size_t m_selectedIdx = 0;
    };

    class Texture;
    class Image : public Component
    {
      public:
        Image( std::string name, Texture * texture );
        ~Image() override = default;

        void operator()() override;

      private:
        Texture * m_texture;
    };
} // namespace udk

#include "Udock/Ui/Component/WindowComponent.inl"

#endif // UDOCK_WINDOWCOMPONENT_HPP
