#ifndef UDOCK_MENU_HPP
#define UDOCK_MENU_HPP

#include "Udock/Ui/Component/Component.hpp"
#include <glm/glm.hpp>
#include <vector>

namespace udk
{
    class MenuItem : public ActionComponent
    {
      public:
        MenuItem(
            std::string name,
            UiAction    action = []() {} );

        void operator()() override;

      private:
        UiAction m_action;
    };

    class MenuField : public Component
    {
      public:
        MenuField( std::string name );

        void addItem( MenuItem item );
        void operator()() override;

      private:
        std::vector<MenuItem> m_items;
    };

    class MenuBar : public Component
    {
      public:
        MenuBar( glm::ivec2 padding = glm::ivec2 { 8 } );
        virtual ~MenuBar() = default;

        void addField( MenuField field ) { m_fields.emplace_back( std::move( field ) ); }
        void addField( Button field ) { m_fields.emplace_back( std::move( field ) ); }
        void addField( Text field ) { m_fields.emplace_back( std::move( field ) ); }
        void operator()() override;

      private:
        glm::ivec2                         m_padding;
        std::vector<std::function<void()>> m_fields;
    };
} // namespace udk

#endif // UDOCK_MENU_HPP
