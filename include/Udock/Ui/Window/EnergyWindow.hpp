#ifndef UDOCK_ENERGYWINDOW_HPP
#define UDOCK_ENERGYWINDOW_HPP

#include "Udock/Physics/Energy.hpp"
#include "Udock/Settings.hpp"
#include "Udock/Ui/Component/FloatingWindow.hpp"
#include <atomic>
#include <thread>

namespace udk
{
    class EnergyWindow : public FloatingWindow
    {
      public:
        EnergyWindow( UdockSettings * settings,
                      System *        system,
                      PhysicalWorld * world,
                      glm::vec2       padding = glm::vec2 { 0.01f, 0.05f } );
        ~EnergyWindow() override;

      private:
        float getLastEnergyScore();
        void  updateMoleculeData();

        UdockSettings * m_settings;
        System *        m_system;
        PhysicalWorld * m_world;

        std::atomic<bool> m_mustComputeEnergy = true;

        float              m_minScore    = std::numeric_limits<float>::max();
        std::atomic<float> m_energyScore = 0.f;
        std::thread        m_energyThread;

        std::atomic<bool>     m_energyUpdate = false;
        std::atomic<bool>     m_fullUpdate   = false;
        EnergyComputationData m_energyData;

        bool               m_needsUpdate = false;
        Listener<Molecule> m_moleculeListener;

        std::size_t                     m_improveIndicatorLife = 0;
        const static inline std::size_t ImproveIndicatorLife   = 60 * 2;
    };
} // namespace udk

#endif // UDOCK_ENERGYWINDOW_HPP
