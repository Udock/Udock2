#ifndef UDOCK_INFORMATIONWINDOW_HPP
#define UDOCK_INFORMATIONWINDOW_HPP

#include "Udock/System/Listener.hpp"
#include "Udock/Udock.hpp"
#include "Udock/Ui/Component/FloatingWindow.hpp"
#include <thread>

namespace udk
{
    class System;
    class PhysicalWorld;
    class InformationWindow : public FloatingWindow
    {
      public:
        using RemoveItemCallback = std::function<void( entt::entity /* toRemove*/ )>;
        using ClearItemsCallback = std::function<void()>;

        InformationWindow( UdockSettings *    settings,
                           System *           system,
                           PhysicalWorld *    physicalWorld,
                           RemoveItemCallback removeItemCallback,
                           ClearItemsCallback clearItemsCallback,
                           glm::vec2          padding = glm::vec2 { 0.01f, 0.05f } );
        ~InformationWindow() override;

      private:
        void optimize();
        void onNewMolecule( entt::entity entity, const Molecule & molecule );
        void onMoleculeDestroyed( entt::entity entity );
        void saveScene() const;
        void saveAs() const;

        UdockSettings * m_settings;
        System *        m_system;
        PhysicalWorld * m_physicalWorld;

        RemoveItemCallback m_removeItemCallback;
        ClearItemsCallback m_clearItemsCallback;

        using DisplayedEntity = std::pair<entt::entity, std::string>;
        std::vector<DisplayedEntity> m_displayedMolecule {};

        std::atomic<std::size_t>          m_selectedMolecule      = 0;
        std::atomic<std::size_t>          m_optimizingSteps       = 2000;
        std::atomic<bool>                 m_optimizing            = false;
        std::atomic<std::size_t>          m_currentOptimizingStep = 0;
        std::atomic<std::optional<float>> m_optimizedValue {};

        std::thread m_optimizationThread;

        Listener<Molecule> m_moleculeListener;
    };
} // namespace udk

#endif // UDOCK_INFORMATIONWINDOW_HPP
