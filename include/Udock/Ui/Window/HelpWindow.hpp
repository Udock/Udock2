#ifndef UDOCK_HELPWINDOW_HPP
#define UDOCK_HELPWINDOW_HPP

#include "Udock/Ui/Component/FloatingWindow.hpp"

namespace udk
{
    class HelpWindow : public FloatingWindow
    {
      public:
        HelpWindow( const glm::vec2 & padding = glm::vec2 { 0.01f, 0.05f } );
        ~HelpWindow() = default;
    };
} // namespace udk

#endif // UDOCK_HELPWINDOW_HPP
