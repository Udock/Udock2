#ifndef UDOCK_SETTINGSWINDOW_HPP
#define UDOCK_SETTINGSWINDOW_HPP

#include "Udock/Settings.hpp"
#include "Udock/Ui/Component/FloatingWindow.hpp"

namespace udk
{
    class SettingsWindow : public FloatingWindow
    {
      public:
        SettingsWindow( UdockSettings * settings, const glm::vec2 & padding = glm::vec2 { 0.01f, 0.05f } );
        ~SettingsWindow() = default;

      private:
        UdockSettings *   m_settings;
        std::atomic<bool> m_isRealisticSky   = true;
        std::atomic<bool> m_isFlatBackground = true;
    };
} // namespace udk

#endif // UDOCK_SETTINGSWINDOW_HPP
