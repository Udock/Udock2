#include "Udock/Renderer/UniformManager.hpp"
#include <cassert>
#include <type_traits>
#include <cstring>
#include <glm/glm.hpp>

namespace udk
{
    template<class Type>
    std::size_t UniformManager::computeOffset()
    {
        // std140 requires explicit alignment rules
        // https://www.khronos.org/registry/OpenGL/extensions/ARB/ARB_uniform_buffer_object.txt
        std::size_t minAlignment = 0;
        if constexpr ( std::is_arithmetic_v<std::remove_reference_t<Type>> )
        {
            minAlignment = 4;
        }
        else if constexpr ( std::is_same_v<glm::vec2, Type> || std::is_same_v<glm::ivec2, Type> )
        {
            minAlignment = 2 * 4;
        }
        else if constexpr (
            std::is_same_v<
                glm::vec3,
                Type> || std::is_same_v<glm::ivec3, Type> || std::is_same_v<glm::vec4, Type> || std::is_same_v<glm::ivec4, Type> || std::is_same_v<glm::mat4, Type> || std::is_same_v<glm::dmat4, Type> || std::is_class_v<Type> )
        {
            minAlignment = 4 * 4;
        }
        else
        {
            minAlignment = 4 * 4;
        }

        const std::size_t remaining = m_bufferUsedSize % minAlignment;
        return remaining == 0 ? remaining : minAlignment - remaining;
    }

    template<class Type>
    void UniformManager::addValue( const std::string & name, const Type & value )
    {
        if ( m_writingSave.find( name ) != m_writingSave.end() )
        {
            updateValue( name, value );
            return;
        }

        m_bufferUsedSize += computeOffset<Type>();

        constexpr std::size_t writeSize = sizeof( Type );
        if ( m_bufferCurrentMaxSize <= m_bufferUsedSize + writeSize )
        {
            m_bufferCurrentMaxSize <<= 1;
            m_uniformBuffer.resize( m_bufferCurrentMaxSize );
        }

        uint8_t * data = m_uniformBuffer.map<uint8_t>( m_bufferUsedSize, writeSize, BufferAuthorization::Write );
        std::memcpy( data, reinterpret_cast<const uint8_t *>( &value ), sizeof( Type ) );
        m_uniformBuffer.unmap();

        m_writingSave[ name ] = m_lastAdded = { m_bufferUsedSize, writeSize };
        m_bufferUsedSize += writeSize;
    }

    template<class Type>
    void UniformManager::addValue( const std::string & name, const Span<Type> values )
    {
        if ( m_writingSave.find( name ) != m_writingSave.end() )
        {
            updateValue( name, values );
            return;
        }

        const std::size_t offset = computeOffset<Type>();
        m_bufferUsedSize += offset;

        const std::size_t writeSize = ( sizeof( Type ) + offset ) * ( values.size() - 1 ) + sizeof( Type );
        if ( m_bufferCurrentMaxSize <= m_bufferUsedSize + writeSize )
        {
            m_bufferCurrentMaxSize <<= 1;
            m_uniformBuffer.resize( m_bufferCurrentMaxSize );
        }

        uint8_t * data = m_uniformBuffer.map<uint8_t>( m_bufferUsedSize, writeSize, BufferAuthorization::Write );
        for ( std::size_t i = 0; i < values.size(); i++ )
        {
            std::memcpy( data + i * ( sizeof( Type ) + offset ),
                         reinterpret_cast<const uint8_t *>( &values[ i ] ),
                         sizeof( Type ) );
        }
        m_uniformBuffer.unmap();

        m_writingSave[ name ] = m_lastAdded = { m_bufferUsedSize, writeSize };
        m_bufferUsedSize += writeSize;
    }

    template<class Type>
    void UniformManager::updateValue( const std::string & name, const Type & value )
    {
        if ( m_writingSave.find( name ) == m_writingSave.end() )
        {
            addValue( name, value );
            return;
        }

        assert( sizeof( Type ) == m_writingSave[ name ].second
                && "Used Type size of Type  does not match with recorded Type size" );

        uint8_t * data = m_uniformBuffer.map<uint8_t>(
            m_writingSave[ name ].first, m_writingSave[ name ].second, BufferAuthorization::Write );
        std::memcpy( data, reinterpret_cast<const uint8_t *>( &value ), m_writingSave[ name ].second );
        m_uniformBuffer.unmap();
    }

    template<class Type>
    void UniformManager::updateValue( const std::string & name, const Span<Type> values )
    {
        if ( m_writingSave.find( name ) == m_writingSave.end() )
        {
            addValue( name, values );
            return;
        }

        const std::size_t offset = computeOffset<Type>();
        assert( ( sizeof( Type ) + offset ) * ( values.size() - 1 ) + sizeof( Type ) == m_writingSave[ name ].second
                && "Used Type size of Type  does not match with recorded Type size" );

        uint8_t * data = m_uniformBuffer.map<uint8_t>(
            m_writingSave[ name ].first, m_writingSave[ name ].second, BufferAuthorization::Write );
        for ( std::size_t i = 0; i < values.size(); i++ )
        {
            std::memcpy( data + i * ( sizeof( Type ) + offset ),
                         reinterpret_cast<const uint8_t *>( &values[ i ] ),
                         sizeof( Type ) );
        }
        m_uniformBuffer.unmap();
    }
} // namespace udk
