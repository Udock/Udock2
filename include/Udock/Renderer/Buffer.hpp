#ifndef UDOCK_BUFFER_HPP
#define UDOCK_BUFFER_HPP

#include "Udock/Core/Macro.hpp"
#include <cstddef>
#include <vector>

#ifdef UDOCK_RENDERER_OPENGL
#include <GL/gl3w.h>
#endif // UDOCK_RENDERER_OPENGL

namespace udk
{
    // This interface was aiming to handle SSBO, the regression to GL 4.1 made this slightly less interesting
    enum class BufferType
    {
        Uniform
    };

    enum class BufferAuthorization : uint32_t
    {
        None  = 0,
        Read  = 1,
        Write = 2,
    };
    UDOCK_DECLARE_BITWISE_FUNCTIONS( BufferAuthorization )

    class Buffer
    {
      public:
        Buffer() = default;
        Buffer( std::size_t         allocationSize,
                BufferAuthorization authorizations = BufferAuthorization::None,
                bool                zeroInit       = true );

        Buffer( const uint8_t *     data,
                std::size_t         size,
                BufferAuthorization authorizations = BufferAuthorization::None );

        template<class DataType>
        Buffer( const std::vector<DataType> & data, BufferAuthorization authorizations = BufferAuthorization::None );

        ~Buffer();

        Buffer( const Buffer & );
        Buffer & operator=( const Buffer & );

        Buffer( Buffer && other ) noexcept;
        Buffer & operator=( Buffer && other ) noexcept;

        void bind( std::size_t index, BufferType bufferType = BufferType::Uniform ) const;
        void bind( std::size_t index,
                   std::size_t offset,
                   std::size_t range,
                   BufferType  bufferType = BufferType::Uniform ) const;

        void        resize( std::size_t newSize, bool zeroInit = false );
        std::size_t size() const { return m_currentSize; }

        template<class MappingType>
        MappingType * map( std::size_t startingPoint, std::size_t mappingSize, BufferAuthorization mappingType );

        void unmap() const;

      private:
#ifdef UDOCK_RENDERER_OPENGL
        GLuint m_handle = GL_INVALID_VALUE;
#endif // UDOCK_RENDERER_OPENGL

        std::size_t         m_currentSize = 0;
        BufferAuthorization m_authorizations;
    };

} // namespace udk

#ifdef UDOCK_RENDERER_OPENGL
#include "Udock/Renderer/GL/GLBuffer.inl"
#endif // UDOCK_RENDERER_OPENGL

#endif // UDOCK_BUFFER_HPP
