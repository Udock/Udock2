#ifndef UDOCK_TEXTURE_HPP
#define UDOCK_TEXTURE_HPP

#include "Udock/Core/Types.hpp"
#include <glm/vec2.hpp>

#ifdef UDOCK_RENDERER_OPENGL
#include <GL/gl3w.h>
#endif // UDOCK_RENDERER_OPENGL

namespace udk
{
    class Texture
    {
      public:
        Texture() = default;
        Texture( const Path & textureFile );

        Texture( const Texture & )             = delete;
        Texture & operator=( const Texture & ) = delete;

        Texture( Texture && );
        Texture & operator=( Texture && );

        ~Texture();

        const glm::ivec2 & getDimensions() const { return m_dimensions; }

#ifdef UDOCK_RENDERER_OPENGL
        GLuint getHandle() const { return m_handle; }
#endif // UDOCK_RENDERER_OPENGL

      private:
#ifdef UDOCK_RENDERER_OPENGL
        GLuint m_handle = GL_INVALID_VALUE;
#endif // UDOCK_RENDERER_OPENGL

        glm::ivec2 m_dimensions;
    };
} // namespace udk

#endif // UDOCK_TEXTURE_HPP
