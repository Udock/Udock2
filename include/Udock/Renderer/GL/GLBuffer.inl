#include "Udock/Renderer/Buffer.hpp"
#include <cassert>

namespace udk
{
    static GLenum toGL( BufferAuthorization authorizations )
    {
        if ( ( authorizations & BufferAuthorization::Read ) == BufferAuthorization::Read
             || ( authorizations & BufferAuthorization::Write ) == BufferAuthorization::Write )
        {
            return GL_DYNAMIC_DRAW;
        }

        return GL_STATIC_DRAW;
    }

    static GLuint toGL( BufferType type )
    {
        if ( type == BufferType::Uniform )
            return GL_UNIFORM_BUFFER;

        throw std::runtime_error( "Unrecognized buffer type" );
    }

    template<class DataType>
    Buffer::Buffer( const std::vector<DataType> & data, const BufferAuthorization authorizations ) :
        Buffer( reinterpret_cast<const uint8_t *>( data.data() ), data.size() * sizeof( DataType ), authorizations )
    {
    }

    template<class MappingType>
    MappingType * Buffer::map( std::size_t startingPoint, std::size_t mappingSize, BufferAuthorization mappingType )
    {
        assert( startingPoint <= m_currentSize && "This mapping try to access out of ot the bound of the buffer." );
        assert( mappingSize <= m_currentSize && "This mapping try to access out of ot the bound of the buffer." );

        glBindBuffer( GL_UNIFORM_BUFFER, m_handle );
        return static_cast<MappingType *>(
            glMapBufferRange( GL_UNIFORM_BUFFER, startingPoint, mappingSize, GL_MAP_READ_BIT | GL_MAP_WRITE_BIT ) );
    }

} // namespace udk
