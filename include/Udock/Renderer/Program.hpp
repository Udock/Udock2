#ifndef UDOCK_PROGRAM_HPP
#define UDOCK_PROGRAM_HPP

#include "Udock/Core/Types.hpp"
#include <string>
#include <vector>

#ifdef UDOCK_RENDERER_OPENGL
#include <GL/gl3w.h>
#endif // UDOCK_RENDERER_OPENGL

namespace udk
{
    class Program
    {
      public:
        Program( std::string name, std::vector<Path> shaderPaths );
        ~Program();

        Program( const Program & )             = delete;
        Program & operator=( const Program & ) = delete;

        Program( Program && );
        Program & operator=( Program && );

#ifdef UDOCK_RENDERER_OPENGL
        inline GLuint getId() const { return m_id; }
        inline void   setId( const GLuint p_id ) { m_id = p_id; }
        inline void   use() const { glUseProgram( m_id ); }
#endif // UDOCK_RENDERER_OPENGL

        inline const std::vector<Path> & getShaderPaths() const { return m_shaderPaths; }

      private:
        void create( const std::string & );
        void attachShader( const GLuint ) const;
        void link();
        void detachShaders();

#ifdef UDOCK_RENDERER_OPENGL
        GLuint              m_id = GL_INVALID_VALUE;
        std::vector<GLuint> m_shaders;
#endif // UDOCK_RENDERER_OPENGL

        std::string       m_name        = "";
        std::vector<Path> m_shaderPaths = {};
    };

} // namespace udk

#endif // UDOCK_PROGRAM_HPP
