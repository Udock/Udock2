#ifndef UDOCK_FRAMEBUFFER_HPP
#define UDOCK_FRAMEBUFFER_HPP

#include "Udock/Core/Types.hpp"
#include "Udock/Renderer/Program.hpp"
#include <glm/vec2.hpp>
#include <optional>
#include <vector>

#ifdef UDOCK_RENDERER_OPENGL
#include <GL/gl3w.h>
#endif // UDOCK_RENDERER_OPENGL

namespace udk
{
    enum class Filter
    {
        Nearest,
        Linear,
    };

    enum class WrappingMode
    {
        ClampToEdge,
        ClampToBorder,
        MirroredRepeat,
        Repeat
    };

    enum class Format
    {
        DepthComponent32F,
        R16F,
        RGBA16F,
        RGBA32UI,
    };

    enum class DataType
    {
        UnsignedInt,
        Int,
        Float
    };

    class FrameBuffer;
    class Attachment
    {
      public:
        Attachment( const glm::ivec2 & size,
                    Format             format,
                    DataType           dataType,

                    Filter min = Filter::Linear,
                    Filter mag = Filter::Linear,

                    WrappingMode wrapS          = WrappingMode::ClampToEdge,
                    WrappingMode wrapT          = WrappingMode::ClampToEdge,
                    std::size_t  levelOfDetails = 0,
                    bool         zeroInit       = false );

        Attachment( const Attachment & )             = delete;
        Attachment & operator=( const Attachment & ) = delete;

        Attachment( Attachment && other ) noexcept;
        Attachment & operator=( Attachment && other ) noexcept;

        ~Attachment();

        void resize( const glm::ivec2 & size );
        void bindAsTexture( const std::string & name, const Program & program, std::size_t bindingIndex ) const;

        friend FrameBuffer;

      private:
#ifdef UDOCK_RENDERER_OPENGL
        GLuint m_handle = GL_INVALID_VALUE;
#endif // UDOCK_RENDERER_OPENGL

        Format   m_format;
        DataType m_dataType;

        Filter m_minFilter;
        Filter m_magFilter;

        WrappingMode m_wrapS;
        WrappingMode m_wrapT;
        std::size_t  m_levelOfDetails;
    };

    class FrameBuffer
    {
      public:
        FrameBuffer() = default;
        FrameBuffer( glm::ivec2 viewportSize );

        FrameBuffer( const FrameBuffer & )             = delete;
        FrameBuffer & operator=( const FrameBuffer & ) = delete;

        FrameBuffer( FrameBuffer && other ) noexcept;
        FrameBuffer & operator=( FrameBuffer && other ) noexcept;

        ~FrameBuffer();

        void synchronize() const;

        const std::optional<Attachment> & getDepthBuffer() const { return m_depthBuffer; }
        void                              setDepthBuffer( Attachment && depthBuffer );
        void                              addAttachment( Attachment && attachment );
        const Attachment &                getAttachment( std::size_t index ) const;
        std::size_t                       getAttachmentNb() const { return m_attachments.size(); }

        void setDrawBuffers( const std::vector<std::size_t> & buffersId );

        void resize( glm::ivec2 newSize );
        void bind() const;
        void unbind() const;

#ifdef UDOCK_RENDERER_OPENGL
        GLuint getHandle() const { return m_handle; }
#endif // UDOCK_RENDERER_OPENGL

      private:
        glm::ivec2 m_viewportSize;

        std::vector<Attachment>   m_attachments;
        std::optional<Attachment> m_depthBuffer;

#ifdef UDOCK_RENDERER_OPENGL
        GLuint m_handle = GL_INVALID_VALUE;
#endif // UDOCK_RENDERER_OPENGL
    };

} // namespace udk

#endif // UDOCK_FRAMEBUFFER_HPP
