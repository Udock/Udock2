#ifndef UDOCK_UNIFORMMANAGER_HPP
#define UDOCK_UNIFORMMANAGER_HPP

#include "Udock/Core/Types.hpp"
#include "Udock/Renderer/Buffer.hpp"
#include "Udock/Renderer/Program.hpp"
#include <unordered_map>

namespace udk
{
    class UniformManager
    {
      public:
        UniformManager() = default;
        UniformManager( std::string name, std::size_t bufferInitializationSize );
        ~UniformManager() = default;

        UniformManager( const UniformManager & )             = delete;
        UniformManager & operator=( const UniformManager & ) = delete;

        UniformManager( UniformManager && )             = default;
        UniformManager & operator=( UniformManager && ) = default;

        template<class Type>
        void addValue( const std::string & name, const Type & value = Type {} );
        template<class Type>
        void addValue( const std::string & name, const Span<Type> values );
        template<class Type>
        void updateValue( const std::string & name, const Type & value );
        template<class Type>
        void updateValue( const std::string & name, const Span<Type> value );

        void bind( const Program & program, std::size_t bindingPoint ) const;

      private:
        template<class Type>
        std::size_t computeOffset();

        std::string m_name;
        std::size_t m_bufferCurrentMaxSize;
        std::size_t m_bufferUsedSize = 0;

        Buffer m_uniformBuffer;

        using AttributeInformation = std::pair<std::size_t /*offset*/, std::size_t /*padding*/>;
        AttributeInformation                                  m_lastAdded = { 0, 0 };
        std::unordered_map<std::string, AttributeInformation> m_writingSave;
    };

} // namespace udk

#include "Udock/Renderer/UniformManager.inl"

#endif // UDOCK_UNIFORMMANAGER_HPP
