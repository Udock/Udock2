#ifndef UDOCK_UI_HPP
#define UDOCK_UI_HPP

#include "Udock/Ui/Component/UiManager.hpp"

namespace udk
{
    class Renderer;
    class Window;

    class Ui
    {
      public:
        Ui() = default;
        Ui( const Window * window, const Renderer * renderer );
        ~Ui();

        Ui( const Ui & ui )             = delete;
        Ui & operator=( const Ui & ui ) = delete;

        Ui( Ui && ui ) noexcept;
        Ui & operator=( Ui && ui ) noexcept;

        UiManager & getUiManager() { return m_manager; };

        void render();

      private:
        const Window *   m_window   = nullptr;
        const Renderer * m_renderer = nullptr;
        UiManager        m_manager {};

        bool m_isInitialized = false;
        bool m_isVisible     = true;
    };
} // namespace udk

#endif // UDOCK_UI_HPP
