#ifndef UDOCK_BAKEDSKYBOX_HPP
#define UDOCK_BAKEDSKYBOX_HPP

#include "Udock/Renderer/Pass/Skybox.hpp"

namespace udk
{
    // Strongly based on the CubeMap chapter of LearnOpengl by Joey de Vries (https://twitter.com/JoeyDeVriez)
    // licensed under the terms of the CC BY-NC 4.0 license as published by Creative Commons
    // https://creativecommons.org/licenses/by-nc/4.0/
    // Reference: https://learnopengl.com/Advanced-OpenGL/Cubemaps
    class BakedSkybox : public Skybox
    {
      public:
        BakedSkybox( const Path & skyboxPath = "./resources/Skybox/MilkyWay" );

        BakedSkybox( BakedSkybox && );
        BakedSkybox & operator=( BakedSkybox && );

        virtual ~BakedSkybox() override = default;

      private:
        void load( const std::vector<Path> & files );
    };
} // namespace udk

#endif // UDOCK_BAKEDSKYBOX_HPP
