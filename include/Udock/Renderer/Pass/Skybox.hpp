#ifndef UDOCK_SKYBOX_HPP
#define UDOCK_SKYBOX_HPP

#include "Udock/Core/Types.hpp"
#include "Udock/Renderer/Program.hpp"
#include "Udock/Renderer/UniformManager.hpp"
#include "Udock/System/Camera.hpp"

namespace udk
{
    enum class SkyboxType
    {
        Realistic,
        Space,
        Flat
    };

    // Strongly based on the CubeMap chapter of LearnOpengl by Joey de Vries (https://twitter.com/JoeyDeVriez)
    // licensed under the terms of the CC BY-NC 4.0 license as published by Creative Commons
    // https://creativecommons.org/licenses/by-nc/4.0/
    // Reference: https://learnopengl.com/Advanced-OpenGL/Cubemaps
    class Skybox
    {
      public:
        Skybox();

        Skybox( const Skybox & )             = delete;
        Skybox & operator=( const Skybox & ) = delete;

        Skybox( Skybox && );
        Skybox & operator=( Skybox && );

        virtual ~Skybox();

        void bind( const std::string & name, const Program & program, std::size_t bindingIndex );
        void render();

      protected:
#ifdef UDOCK_RENDERER_OPENGL
        GLuint m_vao     = GL_INVALID_VALUE;
        GLuint m_vbo     = GL_INVALID_VALUE;
        GLuint m_texture = GL_INVALID_VALUE;
#endif // UDOCK_RENDERER_OPENGL

        static constexpr std::array<float, 108> vertices
            = { -1.0f, 1.0f,  -1.0f, -1.0f, -1.0f, -1.0f, 1.0f,  -1.0f, -1.0f,
                1.0f,  -1.0f, -1.0f, 1.0f,  1.0f,  -1.0f, -1.0f, 1.0f,  -1.0f,

                -1.0f, -1.0f, 1.0f,  -1.0f, -1.0f, -1.0f, -1.0f, 1.0f,  -1.0f,
                -1.0f, 1.0f,  -1.0f, -1.0f, 1.0f,  1.0f,  -1.0f, -1.0f, 1.0f,

                1.0f,  -1.0f, -1.0f, 1.0f,  -1.0f, 1.0f,  1.0f,  1.0f,  1.0f,
                1.0f,  1.0f,  1.0f,  1.0f,  1.0f,  -1.0f, 1.0f,  -1.0f, -1.0f,

                -1.0f, -1.0f, 1.0f,  -1.0f, 1.0f,  1.0f,  1.0f,  1.0f,  1.0f,
                1.0f,  1.0f,  1.0f,  1.0f,  -1.0f, 1.0f,  -1.0f, -1.0f, 1.0f,

                -1.0f, 1.0f,  -1.0f, 1.0f,  1.0f,  -1.0f, 1.0f,  1.0f,  1.0f,
                1.0f,  1.0f,  1.0f,  -1.0f, 1.0f,  1.0f,  -1.0f, 1.0f,  -1.0f,

                -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f,  1.0f,  -1.0f, -1.0f,
                1.0f,  -1.0f, -1.0f, -1.0f, -1.0f, 1.0f,  1.0f,  -1.0f, 1.0f };
    };
} // namespace udk

#endif // UDOCK_SKYBOX_HPP
