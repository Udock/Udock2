#ifndef UDOCK_SHADING_HPP
#define UDOCK_SHADING_HPP

#include "Udock/Renderer/FrameBuffer.hpp"
#include "Udock/Renderer/Program.hpp"
#include "Udock/Renderer/UniformManager.hpp"
#include <functional>

namespace udk
{
    struct GBuffer
    {
        const Attachment * viewPositionNormals;
        const Attachment * colors;
        const Attachment * depth;
    };

    using PostProcess = std::function<void( const Program & /* program */,
                                            const FrameBuffer & /* finalFrameBuffer */,
                                            const GBuffer & /* gBuffer */,
                                            std::optional<UniformManager> & /* uniforms */ )>;
    struct PostProcessSubpass
    {
        Program     program;
        PostProcess renderCallback;

        std::optional<UniformManager> uniforms {};
    };

    class GeometryPass;
    class ShadingPass
    {
      public:
        ShadingPass( const glm::ivec2 & size );

        ShadingPass( const ShadingPass & )             = delete;
        ShadingPass & operator=( const ShadingPass & ) = delete;

        ShadingPass( ShadingPass && );
        ShadingPass & operator=( ShadingPass && );

        ~ShadingPass();

        const FrameBuffer & getFrameBuffer() const { return m_frameBuffer; }

        void addSubpass( PostProcessSubpass && subpass );
        void clear();
        void resize( const glm::ivec2 size );
        void render( const UniformManager & rendererLevelUniforms, const GeometryPass * const geometryPass );

      private:
        FrameBuffer m_frameBuffer;
#ifdef UDOCK_RENDERER_OPENGL
        GLuint m_quadVAO = GL_INVALID_VALUE;
        GLuint m_quadVBO = GL_INVALID_VALUE;
#endif // UDOCK_RENDERER_OPENGL

        std::vector<PostProcessSubpass> m_postProcessSubpasses;
    };
} // namespace udk

#endif // UDOCK_SHADING_HPP
