#ifndef UDOCK_GEOMETRYHANDLER_HPP
#define UDOCK_GEOMETRYHANDLER_HPP

#include "Udock/Data/MolecularSurface.hpp"
#include "Udock/Renderer/UniformManager.hpp"
#include <functional>

#ifdef UDOCK_RENDERER_OPENGL
#include <GL/gl3w.h>
#endif // UDOCK_RENDERER_OPENGL

namespace udk
{
    class Program;
    using GeometryHandler = std::function<void( const Program & )>;

    class MeshHandler
    {
      public:
        MeshHandler( const Mesh & mesh );

        MeshHandler( const MeshHandler & )             = delete;
        MeshHandler & operator=( const MeshHandler & ) = delete;

        MeshHandler( MeshHandler && other ) noexcept;
        MeshHandler & operator=( MeshHandler && other ) noexcept;

        ~MeshHandler();

        void render( const Program & program, const glm::mat4 & transform );

      private:
        MeshView       m_surface;
        UniformManager m_uniforms {};

        constexpr static std::size_t VertexId = 0;
        constexpr static std::size_t NormalId = 1;
        constexpr static std::size_t ColorId  = 2;

#ifdef UDOCK_RENDERER_OPENGL
        GLuint m_meshVao     = GL_INVALID_VALUE;
        GLuint m_verticesVbo = GL_INVALID_VALUE;
        GLuint m_normalsVbo  = GL_INVALID_VALUE;
        GLuint m_colorsVbo   = GL_INVALID_VALUE;
        GLuint m_ibo         = GL_INVALID_VALUE;
#endif // UDOCK_RENDERER_OPENGL
    };

    struct MolecularSurfaceSettings
    {
        glm::vec4 positiveCharge;
        glm::vec4 neutralCharge;
        glm::vec4 negativeCharge;
        float     contrast;
    };

    class MolecularSurfaceHandler
    {
      public:
        MolecularSurfaceHandler( const Mesh & mesh, const MolecularSurfaceSettings & settings );

        MolecularSurfaceHandler( const MolecularSurfaceHandler & )             = delete;
        MolecularSurfaceHandler & operator=( const MolecularSurfaceHandler & ) = delete;

        MolecularSurfaceHandler( MolecularSurfaceHandler && other ) noexcept;
        MolecularSurfaceHandler & operator=( MolecularSurfaceHandler && other ) noexcept;

        ~MolecularSurfaceHandler();

        void render( const Program & program, const glm::mat4 & transform, const MolecularSurfaceSettings & settings );

      private:
        MeshView       m_surface;
        UniformManager m_uniforms {};

        constexpr static std::size_t VertexId = 0;
        constexpr static std::size_t NormalId = 1;
        constexpr static std::size_t ColorId  = 2;

#ifdef UDOCK_RENDERER_OPENGL
        GLuint m_meshVao     = GL_INVALID_VALUE;
        GLuint m_verticesVbo = GL_INVALID_VALUE;
        GLuint m_normalsVbo  = GL_INVALID_VALUE;
        GLuint m_chargesVbo  = GL_INVALID_VALUE;
        GLuint m_ibo         = GL_INVALID_VALUE;
#endif // UDOCK_RENDERER_OPENGL
    };

    struct PointHandler
    {
        PointHandler( std::size_t allocationSize = 64 );

        PointHandler( const PointHandler & )             = delete;
        PointHandler & operator=( const PointHandler & ) = delete;

        PointHandler( PointHandler && other ) noexcept;
        PointHandler & operator=( PointHandler && other ) noexcept;

        ~PointHandler();

        void render( Span<glm::vec4> points, Span<glm::vec4> colors );

      private:
        constexpr static std::size_t SphereId = 0;
        constexpr static std::size_t ColorId  = 1;

        std::size_t m_currentSize;

#ifdef UDOCK_RENDERER_OPENGL
        GLuint m_pointsVao  = GL_INVALID_VALUE;
        GLuint m_spheresVbo = GL_INVALID_VALUE;
        GLuint m_colorsVbo  = GL_INVALID_VALUE;
#endif // UDOCK_RENDERER_OPENGL
    };

    struct LineHandler
    {
        LineHandler( glm::vec4 lineColor, std::size_t allocationSize = 64 );

        LineHandler( const LineHandler & )             = delete;
        LineHandler & operator=( const LineHandler & ) = delete;

        LineHandler( LineHandler && other ) noexcept;
        LineHandler & operator=( LineHandler && other ) noexcept;

        ~LineHandler();

        void render( const Program & program, Span<glm::vec4> lines, glm::vec4 lineColor );

      private:
        constexpr static std::size_t PointId = 0;

        UniformManager m_uniforms {};
        std::size_t    m_currentSize;

#ifdef UDOCK_RENDERER_OPENGL
        GLuint m_linesVao  = GL_INVALID_VALUE;
        GLuint m_pointsVbo = GL_INVALID_VALUE;
#endif // UDOCK_RENDERER_OPENGL
    };
} // namespace udk

#endif // UDOCK_GEOMETRYHANDLER_HPP
