#ifndef UDOCK_GEOMETRY_HPP
#define UDOCK_GEOMETRY_HPP

#include "Udock/Renderer/FrameBuffer.hpp"
#include "Udock/Renderer/Pass/GeometryHandler.hpp"
#include "Udock/System/Camera.hpp"

namespace udk
{
    struct GeometrySubpass
    {
        Program         program;
        GeometryHandler renderCallback;
    };

    class GeometryPass
    {
      public:
        GeometryPass( const glm::ivec2 & size );

        GeometryPass( const GeometryPass & )             = delete;
        GeometryPass & operator=( const GeometryPass & ) = delete;
        GeometryPass( GeometryPass && ) noexcept;
        GeometryPass & operator=( GeometryPass && ) noexcept;

        ~GeometryPass();

        const Attachment & getViewPosition() const;
        const Attachment & getColors() const;
        const Attachment & getDepth() const;

        void resize( const glm::ivec2 & newSize );
        void addSubpass( GeometrySubpass && pass );
        void clear();
        void render( const UniformManager & rendererLevelUniforms );

      private:
        FrameBuffer                  m_frameBuffer;
        std::vector<GeometrySubpass> m_geometrySubpasses;
    };
} // namespace udk

#endif // UDOCK_GEOMETRY_HPP
