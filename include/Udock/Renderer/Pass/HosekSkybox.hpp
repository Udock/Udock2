#ifndef UDOCK_HOSEKSKYBOX_HPP
#define UDOCK_HOSEKSKYBOX_HPP

#include "Udock/Renderer/Pass/Skybox.hpp"

// Implementation of Lukas Hosek, & Alexander Wilkie (2012). An analytic model for full spectral sky-dome radiance. ACM
// Transactions on Graphics, 31(4), 1�9.
// Article and authors' implementation:
// https://cgg.mff.cuni.cz/projects/SkylightModelling/HosekWilkie_SkylightModel_SIGGRAPH2012_Preprint_lowres.pdf
// Shadertoy implementation by pajunen: https://www.shadertoy.com/view/wslfD7
// XYZ to RGB: https://www.itp.uni-hannover.de/fileadmin/itp/emeritus/zawischa/static_html/render.html
// Cube map baking based on the IBL chapter of LearnOpengl by Joey de Vries (https://twitter.com/JoeyDeVriez)
// licensed under the terms of the CC BY-NC 4.0 license as published by Creative Commons
// https://creativecommons.org/licenses/by-nc/4.0/
// Reference: https://learnopengl.com/PBR/IBL/Diffuse-irradiance
namespace udk
{
    class HosekSkybox : public Skybox
    {
      public:
        HosekSkybox( float     turbidity      = 4.f,
                     float     groundAlbedo   = 0.4f,
                     glm::vec3 solarDirection = glm::normalize( glm::vec3 { 1.f, 1.f, 1.f } ) );

        HosekSkybox( HosekSkybox && );
        HosekSkybox & operator=( HosekSkybox && );

        virtual ~HosekSkybox() override = default;

        void setSolarDirection( glm::vec3 solarDirection );
        void setTurbidity( float turbidity );
        void setGroundAlbedo( float albedo );

      private:
        void bake();

        std::array<glm::vec3, 9> getConfiguration( float turbidity, float albedo, float solarElevation );
        glm::vec3                getExpectedRadiance( float turbidity, float albedo, float solarElevation );
        glm::vec3                quinticBezierInterpolation( const float       t,
                                                             const glm::vec3 & A,
                                                             const glm::vec3 & B,
                                                             const glm::vec3 & C,
                                                             const glm::vec3 & D,
                                                             const glm::vec3 & E,
                                                             const glm::vec3 & F );
        glm::vec3                getSample( Span<const glm::vec3> dataset,
                                            std::size_t           stride,
                                            std::size_t           shift,
                                            float                 turbidity,
                                            float                 albedo,
                                            float                 solar_elevation );

        static const std::array<glm::vec3, 1080> DatasetXYZ;
        static const std::array<glm::vec3, 120>  RadiancesXYZ;

        glm::vec3 m_solarDirection;
        float     m_turbidity;
        float     m_groundAlbedo;
    };

} // namespace udk

#endif // UDOCK_HOSEKSKYBOX_HPP
