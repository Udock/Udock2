#ifndef UDOCK_RENDERER_HPP
#define UDOCK_RENDERER_HPP

#include "Udock/Renderer/Pass/GeometryPass.hpp"
#include "Udock/Renderer/Pass/ShadingPass.hpp"
#include "Udock/Renderer/Ui.hpp"
#include "Udock/Settings.hpp"
#include "Udock/System/Camera.hpp"
#include "Udock/System/Listener.hpp"
#include <SDL.h>
#include <map>

namespace udk
{
    class Window;
    class Renderer
    {
      public:
        Renderer( const Window *  window,
                  UdockSettings * settings,
                  System *        system,
                  PhysicalWorld * world,
                  Entity          camera );

        Renderer( const Renderer & other )             = delete;
        Renderer & operator=( const Renderer & other ) = delete;

        Renderer( Renderer && other ) noexcept             = default;
        Renderer & operator=( Renderer && other ) noexcept = default;

        ~Renderer();

#ifdef UDOCK_RENDERER_OPENGL
        SDL_GLContext getHandle() const { return m_context; }
#endif // UDOCK_RENDERER_OPENGL

        void     setSkybox( std::unique_ptr<Skybox> && skybox );
        Skybox * getSkybox() { return m_skybox.get(); }

        void addGeometrySubpass( GeometrySubpass && subpass );
        void addPostProcessSubpass( PostProcessSubpass && subpass );
        void clear();

        UiManager & getUiManager() { return m_ui.getUiManager(); }

        glm::ivec2 getSize() const { return m_viewportSize; }
        void       resize( glm::ivec2 newSize );
        void       render();

      private:
        void createRenderingPipeline();
        void onNewMesh( Entity newEntity, Event event );

        const Window *  m_window;
        UdockSettings * m_settings;
        System *        m_system;
        PhysicalWorld * m_world;
        Entity          m_cameraEntity;

        Listener<Mesh> m_meshListener;

        glm::ivec2 m_viewportSize;

        std::unique_ptr<PointHandler> m_pointHandler;
        std::unique_ptr<LineHandler>  m_lineHandler;

        // Should be enough since the application targets only a small number of element.
        // Potential upgrade: https://github.com/Tessil/robin-map
        using SurfaceHandlerHashMap = std::map<entt::entity, MolecularSurfaceHandler>;
        SurfaceHandlerHashMap m_surfaceHandlers {};
        using MeshHashMap = std::map<entt::entity, MeshHandler>;
        MeshHashMap m_meshHandlers {};

        FrameBuffer m_linearizedDepthFrameBuffer;
        FrameBuffer m_trailsFrameBuffer;
        glm::vec3   m_lastCameraPosition {};

        std::unique_ptr<Skybox>       m_skybox;
        std::unique_ptr<GeometryPass> m_geometryPass;
        std::unique_ptr<ShadingPass>  m_shadingPass;
        UniformManager                m_rendererLevelUniforms;

        Ui   m_ui;
        bool m_initalized = false;

#ifdef UDOCK_RENDERER_OPENGL
        SDL_GLContext m_context;
#endif // UDOCK_RENDERER_OPENGL
    };
} // namespace udk

#endif // UDOCK_RENDERER_HPP
