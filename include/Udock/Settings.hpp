#ifndef UDOCK_SETTINGS_HPP
#define UDOCK_SETTINGS_HPP

#include "Udock/Renderer/Pass/GeometryHandler.hpp"
#include "Udock/Renderer/Pass/Skybox.hpp"
#include "Udock/Ui/Controller/CameraController.hpp"
#include "Udock/Ui/Controller/DockingController.hpp"
#include "Udock/Ui/State/Spaceship.hpp"
#include <cstdint>
#include <glm/glm.hpp>

namespace udk
{
    struct UdockSettings
    {
        CameraControllerSettings  cameraControllerSettings {};
        DockingControllerSettings entityControllerSettings {};
        SpaceshipSettings         spaceshipSettings {};

        static const bool      DisplayTargetDefault;
        bool                   displayTarget = DisplayTargetDefault;
        static const float     TargetSizeDefault;
        float                  targetSize = TargetSizeDefault;
        static const glm::vec4 TargetColorDefault;
        glm::vec4              targetColor = TargetColorDefault;

        static const uint8_t ShaderUniformsRendererIndexDefault;
        static const uint8_t ShaderUniformsModelIndexDefault;

        static const bool EnableTrailsDefault;
        bool              enableTrails = EnableTrailsDefault;
        static const bool EnableShadingWithEnvironmentDefault;
        bool              enableShadingWithEnvironment = EnableShadingWithEnvironmentDefault;

        static const glm::vec4 PositiveChargeColorDefault;
        static const glm::vec4 NeutralChargeColorDefault;
        static const glm::vec4 NegativeChargeColorDefault;
        static const float     ChargeContrastDefault;

        MolecularSurfaceSettings moleculeSurfaceColors = { PositiveChargeColorDefault,
                                                           NeutralChargeColorDefault,
                                                           NegativeChargeColorDefault,
                                                           ChargeContrastDefault };

        static const glm::vec4 ConstraintColorDefault;
        glm::vec4              constraintColor = ConstraintColorDefault;
        static const float     DockingSpeedDefault;
        float                  dockingSpeed = DockingSpeedDefault;

        static const float EnergyMaxRadiusDefault;
        std::atomic<float> energyMaxRadius = EnergyMaxRadiusDefault;
        static const float OptimizationStepSizeDefault;
        std::atomic<float> optimizationStepSize = OptimizationStepSizeDefault;

        static const SkyboxType SkyboxTypeDefault;
        SkyboxType              skyboxType = SkyboxTypeDefault;

        static const glm::vec4 BackgroundColorDefault;
        glm::vec4              backgroundColor = BackgroundColorDefault;

        static const float TurbidityDefault;
        float              turbidity = TurbidityDefault;
        static const float GroundAlbedoDefault;
        float              groundAlbedo = GroundAlbedoDefault;
    };
} // namespace udk

#endif // UDOCK_SETTINGS_HPP
