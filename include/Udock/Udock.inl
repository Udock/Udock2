#include "Udock/Udock.hpp"

namespace udk
{
    template<typename Window, typename... Args>
    void Udock::toggleWindow( const std::string & windowName, Args &&... args )
    {
        UiManager & uiManager = m_renderer->getUiManager();
        if ( uiManager.hasWindow( windowName ) )
            uiManager.removeWindow( windowName );
        else
            m_renderer->getUiManager().addWindow( windowName,
                                                  std::make_unique<Window>( std::forward<Args>( args )... ) );
    }
} // namespace udk
