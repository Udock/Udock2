#ifndef UDOCK_MOLECULESURFACE_HPP
#define UDOCK_MOLECULESURFACE_HPP

#include "Udock/Data/Mesh.hpp"
#include "Udock/Data/Molecule.hpp"

namespace udk
{
    Mesh getSolventAccessibleSurface( const Molecule & molecule, float cubeSize = 0.5f, float probeRadius = 1.4f );
    static std::vector<float> sdSas( const Molecule &   molecule,
                                     const glm::vec3 &  origin,
                                     const glm::ivec3 & gridSize,
                                     const float        cubeSize,
                                     const float        probeRadius );

    Mesh getSolventExcludedSurface( const Molecule & molecule, float cubeSize = 0.4f, float probeRadius = 1.4f );
    static std::vector<float> sdSes( const Molecule &     molecule,
                                     const glm::vec3 &    origin,
                                     const glm::ivec3 &   gridSize,
                                     const float          cubeSize,
                                     const float          probeRadius,
                                     std::vector<float> & charges );
} // namespace udk

#endif // UDOCK_MOLECULESURFACE_HPP
