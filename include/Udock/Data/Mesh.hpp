#ifndef UDOCK_MESH_HPP
#define UDOCK_MESH_HPP

#include "Udock/Core/Types.hpp"
#include <glm/vec3.hpp>
#include <vector>

namespace udk
{
    struct Mesh
    {
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;
        std::vector<uint32_t>  indices;
        std::vector<glm::vec3> colors;
    };

    struct MeshView
    {
        MeshView() = default;
        MeshView( const Mesh & mesh );

        Span<const glm::vec3> vertices;
        Span<const glm::vec3> normals;
        Span<const uint32_t>  indices;
        Span<const glm::vec3> colors;
    };
} // namespace udk

#endif // UDOCK_MESH_HPP
