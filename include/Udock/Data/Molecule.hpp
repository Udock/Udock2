#ifndef UDOCK_MOLECULE_HPP
#define UDOCK_MOLECULE_HPP

#include "Udock/Core/Math.hpp"
#include "Udock/Core/Types.hpp"
#include "Udock/Data/Atom.hpp"
#include "Udock/Utils/AccelerationStructure.hpp"
#include <vector>

namespace udk
{
    struct Chain
    {
        std::string id;
        std::string name;
        std::size_t startResidueId = 0;
        std::size_t residueNb      = 0;
    };

    struct Residue
    {
        int64_t     id;
        std::string name;
        std::size_t startAtomId = 0;
        std::size_t atomNb      = 0;
        std::size_t startBondId = 0;
        std::size_t bondNb      = 0;

        std::vector<std::size_t> extraBounds;
    };

    using Bond = std::pair<std::size_t /* first atom */, std::size_t /* second atom */>;

    class Molecule
    {
      public:
        Molecule( std::string          name,
                  std::vector<Atom>    atoms,
                  std::vector<Chain>   chains   = {},
                  std::vector<Residue> residues = {},
                  std::vector<Bond>    bonds    = {} );
        ~Molecule() = default;

        std::string_view           getName() const { return m_name; }
        float                      getCharge( const glm::vec3 & position ) const;
        const Aabb &               getDimensions() const { return m_aabb; }
        const std::vector<Atom> &  getAtoms() const { return m_atoms; }
        const std::vector<Chain> & getChains() const { return m_chains; }
        const std::vector<Residue> getResidues() const { return m_residues; }
        const std::vector<Bond>    getBonds() const { return m_bonds; }

        const AccelerationStructure & getAccelerationStructure() const { return m_accelerationStructure; }

      private:
        Aabb getBoundingGeometries();
        void updateBarycenter();
        void moveCenterToOrigin();

        std::string m_name;
        glm::vec3   m_barycenter = glm::vec3( 0.f );
        float       m_radius     = 0.f;

        std::vector<Atom>     m_atoms {};
        std::vector<Chain>    m_chains {};
        std::vector<Residue>  m_residues {};
        std::vector<Bond>     m_bonds {};
        Aabb                  m_aabb {};
        AccelerationStructure m_accelerationStructure;
    };
} // namespace udk

#endif // UDOCK_MOLECULE_HPP
