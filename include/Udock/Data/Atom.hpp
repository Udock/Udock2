#ifndef UDOCK_ATOM_HPP
#define UDOCK_ATOM_HPP

#include <glm/glm.hpp>
#include <string_view>

namespace udk
{
    enum class Sybyl
    {
        CarbonSp3,                         // (C.3)
        CarbonSp2,                         // (C.2)
        CarbonAromatic,                    // (C.ar)
        Carbocation,                       // (guanadinium) (C.cat)
        NitrogenSp3,                       // (N.3)
        NitrogenSp2,                       // (N.2)
        NitrogenSp3PositivelyCharged,      // (N.4)
        NitrogenAromatic,                  // (N.ar)
        NitrogenAmide,                     // (N.am)
        NitrogenTrigonalPlanar,            // (N.pl3)
        OxygenSp3,                         // (O.3)
        OxygenSp2,                         // (O.2)
        OxygenInCarboxylatesAndPhosphates, // (O.co2)
        SulphurSp3,                        // (S.3)
        PhosphorusSp3,                     // (P.3)
        Fluorine,                          // (F)
        Hydrogen,                          // (H)
        Lithium                            // (Li)
    };

    constexpr std::string_view toString( Sybyl type );
    constexpr Sybyl            fromString( std::string_view type );
    constexpr float            getRadius( Sybyl type );
    constexpr float            getEpsilon( Sybyl type );

    struct Atom
    {
        std::string name;
        Sybyl       sybylType;
        glm::vec3   pos;
        float       radius;
        float       epsilon;
        float       charge;
        std::size_t residueId;
    };

} // namespace udk

#include "Udock/Data/Atom.inl"

#endif // UDOCK_ATOM_HPP
