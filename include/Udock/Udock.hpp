#ifndef UDOCK_UDOCK_HPP
#define UDOCK_UDOCK_HPP

#include "Udock/Physics/PhysicalWorld.hpp"
#include "Udock/Renderer/Pass/Skybox.hpp"
#include "Udock/Renderer/Renderer.hpp"
#include "Udock/Settings.hpp"
#include "Udock/System/Camera.hpp"
#include "Udock/System/System.hpp"
#include "Udock/Ui/State/State.hpp"
#include "Udock/Window.hpp"

namespace udk
{
    class Udock
    {
      public:
        Udock();
        ~Udock();

        void run();

      private:
        void createUi();

        template<typename Window, typename... Args>
        void toggleWindow( const std::string & windowName, Args &&... args );

        Window        m_window;
        System        m_system {};
        PhysicalWorld m_physicalWorld;
        UdockSettings m_settings;

        Camera * m_camera;

        std::unique_ptr<Renderer> m_renderer;
        SkyboxType                m_currentSkyboxType;
        std::unique_ptr<State>    m_state;
        Listener<Molecule>        m_moleculeListener;
    };
} // namespace udk

#include "Udock/Udock.inl"

#endif // UDOCK_APPLICATION_HPP
