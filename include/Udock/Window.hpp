#ifndef UDOCK_WINDOW_HPP
#define UDOCK_WINDOW_HPP

#include "Udock/Ui/UiInput.hpp"
#include <SDL.h>
#include <cstddef>
#include <string>

namespace udk
{
    class Window
    {
      public:
        Window( glm::ivec2 size );

        Window( const Window & ) = delete;
        Window & operator=( const Window & ) = delete;

        Window( Window && other ) noexcept = default;
        Window & operator=( Window && other ) noexcept = default;
        ~Window()                                      = default;

        glm::ivec2       getSize() const { return m_size; }
        std::size_t      getWidth() const { return m_size.x; }
        std::size_t      getHeight() const { return m_size.y; }
        std::string_view getTitle() const { return WindowTitle; }
        SDL_Window *     getHandle() const { return m_window.get(); }
        const UiInput &  getLastUiInput() const { return m_lastInput; }

        bool update();

      private:
        void        updateControllers();
        static void closeWindow( SDL_Window * handle );

        const static std::string WindowTitle;

        glm::ivec2 m_size;
        using WindowHandle    = std::unique_ptr<SDL_Window, decltype( &closeWindow )>;
        WindowHandle m_window = { nullptr, []( SDL_Window * handle ) {} };

        uint64_t m_lastTimeStep;
        UiInput  m_lastInput {};

        struct GameControllerHandle
        {
            SDL_GameController * handle    = nullptr;
            SDL_Haptic *         haptic    = nullptr;
            bool                 hasHaptic = false;

            GameControllerHandle() = default;
            GameControllerHandle( SDL_GameController * controller );

            GameControllerHandle( const GameControllerHandle & ) = delete;
            GameControllerHandle & operator=( const GameControllerHandle & ) = delete;

            GameControllerHandle( GameControllerHandle && );
            GameControllerHandle & operator=( GameControllerHandle && );

            ~GameControllerHandle();
        };

        GameControllerHandle m_controller;
    };
} // namespace udk

#endif // UDOCK_WINDOW_HPP
