#ifndef UDOCK_CHRONO_HPP
#define UDOCK_CHRONO_HPP

#include <chrono>
#include <fmt/chrono.h>
#include <string>

namespace udk
{
    inline std::time_t getNow() { return std::chrono::system_clock::to_time_t( std::chrono::system_clock::now() ); }

    static std::string getNowString() { return fmt::format( "{:%d-%m-%Y %H:%M:%OS}", fmt::localtime( getNow() ) ); }

    template<class Duration>
    class ChronoBase
    {
        using Clock = std::chrono::high_resolution_clock;

      public:
        void start()
        {
            m_begin   = Clock::now();
            m_started = true;
        }

        void stop()
        {
            m_end     = Clock::now();
            m_started = false;
        }

        float elapsedTime() const
        {
            return ( static_cast<float>( std::chrono::duration_cast<Duration>( m_end - m_begin ).count() ) );
        }

        float currentTime() const
        {
            return ( static_cast<float>( std::chrono::duration_cast<Duration>( Clock::now() - m_begin ).count() ) );
        }

        bool isStarted() const { return m_started; }

      private:
        bool              m_started = false;
        Clock::time_point m_begin;
        Clock::time_point m_end;
    };

    using Chrono = ChronoBase<std::chrono::seconds>;
} // namespace udk

#endif // UDOCK_CHRONO_HPP
