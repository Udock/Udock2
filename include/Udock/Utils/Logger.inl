#include "Udock/Utils/Logger.hpp"
#include <glm/glm.hpp>

template<>
struct fmt::formatter<glm::vec2>
{
    constexpr auto parse( fmt::format_parse_context & ctx ) -> decltype( ctx.begin() ) { return ctx.begin(); }

    template<typename FormatContext>
    auto format( const glm::vec2 & p, FormatContext & ctx ) -> decltype( ctx.out() )
    {
        return format_to( ctx.out(), "({:.4}, {:.4f})", p.x, p.y );
    }
};

template<>
struct fmt::formatter<glm::vec3>
{
    constexpr auto parse( fmt::format_parse_context & ctx ) -> decltype( ctx.begin() ) { return ctx.begin(); }

    template<typename FormatContext>
    auto format( const glm::vec3 & p, FormatContext & ctx ) -> decltype( ctx.out() )
    {
        return format_to( ctx.out(), "({:.4}, {:.4f}, {:.4f})", p.x, p.y, p.z );
    }
};

template<>
struct fmt::formatter<glm::vec4>
{
    constexpr auto parse( fmt::format_parse_context & ctx ) -> decltype( ctx.begin() ) { return ctx.begin(); }

    template<typename FormatContext>
    auto format( const glm::vec4 & p, FormatContext & ctx ) -> decltype( ctx.out() )
    {
        return format_to( ctx.out(), "({:.4f}, {:.4f}, {:.4f}, {:.4f})", p.x, p.y, p.z, p.w );
    }
};

template<>
struct fmt::formatter<glm::mat4>
{
    constexpr auto parse( fmt::format_parse_context & ctx ) -> decltype( ctx.begin() ) { return ctx.begin(); }

    template<typename FormatContext>
    auto format( const glm::mat4 & m, FormatContext & ctx ) -> decltype( ctx.out() )
    {
        return format_to( ctx.out(),
                          "\n({:.4f}, {:.4f}, {:.4f}, {:.4f})\n({:.4f}, {:.4f}, {:.4f}, {:.4f})\n({:.4f}, {:.4f}, "
                          "{:.4f}, {:.4f})\n({:.4f}, {:.4f}, {:.4f}, {:.4f})\n",
                          m[ 0 ][ 0 ],
                          m[ 1 ][ 0 ],
                          m[ 2 ][ 0 ],
                          m[ 3 ][ 0 ],
                          m[ 0 ][ 1 ],
                          m[ 1 ][ 1 ],
                          m[ 2 ][ 1 ],
                          m[ 3 ][ 1 ],
                          m[ 0 ][ 2 ],
                          m[ 1 ][ 2 ],
                          m[ 2 ][ 2 ],
                          m[ 3 ][ 2 ],
                          m[ 0 ][ 3 ],
                          m[ 1 ][ 3 ],
                          m[ 2 ][ 3 ],
                          m[ 3 ][ 3 ] );
    }
};
