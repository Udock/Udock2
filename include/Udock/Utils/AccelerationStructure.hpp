#ifndef UDOCK_ACCELERATION_STRUCTURE_HPP
#define UDOCK_ACCELERATION_STRUCTURE_HPP

#include <functional>
#include <glm/glm.hpp>
#include <vector>

namespace udk
{
    class Molecule;
    class AccelerationStructure
    {
      public:
        AccelerationStructure( const Molecule * const molecule );
        ~AccelerationStructure() = default;

        glm::ivec3  getGridPosition( const glm::vec3 & position ) const;
        std::size_t getGridHash( glm::ivec3 gridPosition ) const;

        void getNearElements( const glm::vec3 &                     worldPos,
                              float                                 radius,
                              std::function<void( std::size_t id )> callback ) const;

      private:
        void build( const Molecule * const molecule );

        glm::vec3  m_cellSize;
        glm::vec3  m_origin;
        glm::ivec3 m_gridSize;

        std::vector<std::pair<std::size_t /* atom id */, std::size_t /* hash */>>                m_hashes;
        std::vector<std::pair<std::size_t /* start hash idx */, std::size_t /* end hash idx */>> m_grid;
    };

} // namespace udk

#endif // UDOCK_ACCELERATION_STRUCTURE_HPP
