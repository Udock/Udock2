#ifndef UDOCK_MOLECULEPARSER_HPP
#define UDOCK_MOLECULEPARSER_HPP

#include "Udock/Data/Mesh.hpp"
#include "Udock/Data/Molecule.hpp"

namespace udk
{
    Mesh     readObj( const Path & path );
    Molecule readMolecule( const Path & path );

    void writeMolecule( const Path & path, const Molecule & molecule, const glm::mat4 & transform = glm::mat4 { 1.f } );
} // namespace udk

#endif // UDOCK_MOLECULEPARSER_HPP
