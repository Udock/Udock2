#ifndef UDOCK_LOGGER_HPP
#define UDOCK_LOGGER_HPP

#include <fmt/format.h>
#include <iostream>

namespace udk
{
    class Logger
    {
      public:
        inline static Logger & get()
        {
            static Logger instance;
            return instance;
        }

        inline void logDebug( std::string_view debug ) const
        {
#ifndef _DEBUG
            return;
#endif
            log( "DEBUG", debug );
        }
        inline void logInfo( std::string_view info ) const { log( "INFO", info ); }
        inline void logWarning( std::string_view warning ) const { log( "WARNING", warning ); }
        inline void logError( std::string_view error ) const { log( "ERROR", error ); }

      private:
        Logger()                             = default;
        Logger( const Logger & )             = delete;
        Logger & operator=( const Logger & ) = delete;
        ~Logger()                            = default;

        void log( std::string_view, std::string_view ) const;
    };

    template<typename... T>
    inline void UDK_DEBUG( std::string_view str, T &&... args )
    {
        Logger::get().logDebug( fmt::format( str, args... ) );
    }
    template<typename... T>
    inline void UDK_INFO( std::string_view str, T &&... args )
    {
        Logger::get().logInfo( fmt::format( str, args... ) );
    }
    template<typename... T>
    inline void UDK_WARNING( std::string_view str, T &&... args )
    {
        Logger::get().logWarning( fmt::format( str, args... ) );
    }
    template<typename... T>
    inline void UDK_ERROR( std::string_view str, T &&... args )
    {
        Logger::get().logError( fmt::format( str, args... ) );
    }
    template<typename... T>
    inline void UDK_CONSOLE( std::string_view str, T &&... args )
    {
        std::cout << fmt::format( str, args... ) << std::endl;
    }

} // namespace udk

#include "Udock/Utils/Logger.inl"

#endif // UDOCK_LOGGER_HPP
