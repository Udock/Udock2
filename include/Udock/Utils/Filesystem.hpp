#ifndef UDOCK_FILESYSTEM_HPP
#define UDOCK_FILESYSTEM_HPP

#include "Udock/Core/Types.hpp"
#include <fstream>
#include <string>

namespace udk
{
    inline std::string readPath( const Path & p_path )
    {
        std::ifstream file;
        file.open( p_path, std::ios::in );

        if ( !file.is_open() )
        {
            throw std::runtime_error( "Cannot open file " + p_path.string() );
        }

        const uintmax_t size = std::filesystem::file_size( p_path );
        std::string     result( size, '\0' );
        file.read( result.data(), size );
        file.close();

        return result;
    }

} // namespace udk

#endif // UDOCK_FILESYSTEM_HPP
